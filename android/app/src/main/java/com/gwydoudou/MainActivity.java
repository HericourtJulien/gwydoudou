package com.gwydoudou;

import com.facebook.react.ReactActivity;
import com.brentvatne.react.ReactVideoPackage;
import com.sensors.RNSensorsPackage;
import com.sensormanager.SensorManagerPackage;
import com.github.yamill.orientation.OrientationPackage;
import com.lwansbrough.RCTCamera.RCTCameraPackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.github.yamill.orientation.OrientationPackage;
import android.content.Intent;
import android.content.res.Configuration;


public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "gwydoudou";
    }

}
