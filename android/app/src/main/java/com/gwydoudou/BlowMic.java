/*
package ddi.gobelin.benjamin.blowmic;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.IOException;
import java.util.Map;

public class MainActivity extends ReactContextBaseJavaModule {

    private AudioRecord audioRecorder = null;
    private static final int RECORDER_SAMPLERATE = 40000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private boolean isRecording = false;

    private static final String LOG_TAG = "AudioRecordTest";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;

    final Handler mHandler = new Handler();//handler for communicate between threads

    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {Manifest.permission.RECORD_AUDIO};

    //Thread for recording with mic leaving main UI Thread available for user
    private Thread recordingThread = null;

    private int read = 0;
    private int amplitude = 0;

    */
/**
     * Ask permission to use microphone to user
     * @param requestCode
     * @param permissions
     * @param grantResults
     *//*

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted ) finish();

    }

    */
/**
     * used by React-Native
     * The purpose of this method is to return the string name of the NativeModule which represents this class in JavaScript.
     * @return ModuleName
     * We will be able to access it via React.NativeModules.BlowMic in JavaScript.
     *//*

    @Override
    public String getName() {
        return "BlowMic";
    }

    */
/**
     * used by React-Native
     * returns the constant values exposed to JavaScript.
     * Its implementation is not required but is very useful to key pre-defined values that need to be communicated from JavaScript to Java in sync.
     * @return
     *//*

    @Override
    public Map < String, Object > getConstants() {
        final Map < String,
                Object > constants = new HashMap <  > ();
        constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
        constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
        return constants;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Context context = getApplicationContext();
        final int duration = Toast.LENGTH_SHORT;

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        final Button startBtn = (Button) findViewById(R.id.recordBtn);
        final Button stopBtn = (Button) findViewById(R.id.stopBtn);

        startBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(!isRecording){
                    //laucnh recording

                    Toast toast = Toast.makeText(context, "start recording ...", duration);
                    toast.show();

                    startRecording();
                }else{
                    Toast toast = Toast.makeText(context, "already recording : " + isRecording, duration);
                    toast.show();
                }
            }
        });

        stopBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                if (isRecording){
                    //stop recording
                    Toast toast = Toast.makeText(context, "stop recording", duration);
                    toast.show();

                    mHandler.post(stopRecording);
                }else{
                    Toast toast = Toast.makeText(context, "not recording ... : "+ isRecording, duration);
                    toast.show();
                }
            }
        });
    }

    */
/**
     * React-Native annotation to launch function with JavaScript
     * The return type of bridge methods is always VOID
     *//*

    @ReactMethod
    private void startRecording(){

        // Get the minimum buffer size required for the successful creation of an AudioRecord object.
        int bufferSizeInBytes = AudioRecord.getMinBufferSize( RECORDER_SAMPLERATE,
                RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING
        );
        // Initialize Audio Recorder.
        audioRecorder = new AudioRecord( MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE,
                RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING,
                bufferSizeInBytes
        );

        // Start Recording using micrphone.
        audioRecorder.startRecording();

        isRecording = true;
        final byte[] buffer = new byte[bufferSizeInBytes];

        recordingThread = new Thread(new Runnable() {
            public void run() {
                while (isRecording) {

                    // read the data into the buffer
                    read = audioRecorder.read(buffer, 0, buffer.length);
                    amplitude = (buffer[0] & 0xff) << 8 | buffer[1];

                    // Determine amplitude
                    double amplitudeDb = 20 * Math.log10((double)Math.abs(amplitude) / 32768);
                    String dbString = String.valueOf(amplitudeDb);
                    Log.e(LOG_TAG, "dB : " + dbString);

                    if(amplitudeDb >= 5.0){
                        mHandler.post(stopRecording);
                    }
                }
            }
        }, "recordingThread");

        recordingThread.start();
    }

    */
/**
     * stop recording thread and release
     *//*

    @ReactMethod
    final Runnable stopRecording = new Runnable(){

        public void run(){
            isRecording = false;
            audioRecorder.stop();
            audioRecorder.release();
            audioRecorder = null;
            read = 0;
            amplitude = 0;
            Log.e(LOG_TAG, "STOP RECORDING THREAD");
            recordingThread.interrupt();
        }
    };




}
*/
