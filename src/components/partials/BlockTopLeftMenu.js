/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Image
} from 'react-native';
import * as Animatable from 'react-native-animatable';

export default class BlockTopLeftMenu extends Component {

    constructor(props) {
        super(props);

    }

    onButtonPressed(action) {
        switch (action) {
            case 'map':
                this.props.playSound(13);
                this.props.navigator.push({
                    id: 'Map'
                });

                this.props.stopAll();

                return;
            case 'bundle':
                this.props.playSound(13);
                this.props.navigator.push({
                    id: 'Bundle'
                });
                return;
        }
    }

    render() {
        return (
            <Animatable.View
                animation="bounceIn"
                style={styles.blockMenu}>
                <TouchableHighlight onPress={() => this.onButtonPressed(this.props.type)} style={styles.blockMenuIcon} underlayColor="0">
                    {this.props.type === 'bundle' ? (
                        <Image source={require('../../assets/img/icons/bundle.png')} style={styles.blockMenuImage}/>
                    ) : (
                        <Image source={require('../../assets/img/icons/home.png')} style={styles.blockMenuImage}/>
                    )}
                </TouchableHighlight>
            </Animatable.View>
        );
    }
}

const styles = StyleSheet.create({
    blockMenu: {
        flex: 1,
        height: 50,
        flexDirection: 'row',
        position: 'absolute',
        left: 40,
        top: 40,
        zIndex: 100
    },
    blockMenuIcon: {
        marginLeft: 10,
        height: 80,
        width: 80,
        justifyContent: 'center',
        alignItems: 'center'
    },
    blockMenuImage: {
        height: 75,
        width: 75
    }
});

