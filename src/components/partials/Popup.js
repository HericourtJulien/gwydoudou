/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableHighlight,
    Image
} from 'react-native';
import IntAnim from '../../datas/InterfaceAnimations';
import * as Animatable from 'react-native-animatable';

let winW = Dimensions.get('window').width;
let winH = Dimensions.get('window').height;

export default class Popup extends Component {

    constructor(props) {
        super(props);

        this.state = {
            popup: []
        }
    }

    componentWillMount() {
        this.createPopup();
    }

    createPopup() {
        let content = [];
        let {children, ...props} = this.props;
        let mask = (
            <Animatable.View key={0} delay={100} animation={IntAnim.ANIMATIONS.MASK.START} style={styles.mask}/>
        );
        let popup = (
            <Animatable.View key={1} delay={100} animation={IntAnim.ANIMATIONS.PREVIEW.START}>
                <View style={styles.popup}>
                    <TouchableHighlight onPress={() => this.closePopup()} style={styles.close}
                                        underlayColor="0">
                        <Image source={require('../../assets/img/icons/close.png')} style={styles.closeIcon}/>
                    </TouchableHighlight>
                    {children}
                </View>
            </Animatable.View>
        );

        typeof this.refs.fullBlock != 'undefined' ? this.refs.fullBlock.transitionTo({zIndex: 3}, 600) : null;

        content.push(mask);
        content.push(popup);

        this.setState({
            popup: content
        });
    }

    closePopup() {
        let mask = this.state.popup[0];
        let popup = this.state.popup[1];
        let clonedElements = [];
        let clonedElement;

        this.props.playSound(12);

        clonedElement = React.cloneElement(mask, {
            animation: IntAnim.ANIMATIONS.MASK.END,
            duration: 600
        });

        clonedElements.push(clonedElement);

        clonedElement = React.cloneElement(popup, {
            animation: IntAnim.ANIMATIONS.PREVIEW.END,
            duration: 600
        });

        clonedElements.push(clonedElement);

        this.refs.fullBlock.transitionTo({zIndex: -1, transform:[{scale: 0}]}, 600);

        this.setState({
            popup: clonedElements
        });
    }

    render() {
        return (
            <Animatable.View ref={'fullBlock'} style={styles.fullBlock}>
                {this.state.popup.length > 0 ? this.state.popup : null}
            </Animatable.View>
        );
    }
}

const styles = StyleSheet.create({
    fullBlock: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        zIndex: 3,
    },
    popup: {
        position: 'absolute',
        width: winW * 0.74,
        height: winH * 0.69,
        top: winH / 2 - winH * 0.69 / 2,
        left: winW / 2 - winW * 0.74 / 2,
        backgroundColor: '#FFFFFF',
        zIndex: 3,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    mask: {
        position: 'absolute',
        top: -winH,
        left: 0,
        width: winW,
        height: winH,
        backgroundColor: '#ffffff',
        opacity: 0,
        zIndex: 2,
    },
    close: {
        position: 'absolute',
        top: 20,
        right: 20
    },
    closeIcon: {
        width: 25,
        height: 25
    },
});

