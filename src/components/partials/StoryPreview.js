/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableHighlight,
    Image
} from 'react-native';

let winW = Dimensions.get('window').width;
let winH = Dimensions.get('window').height;

export default class StoryPreview extends Component {
    render() {
        let {data, ...props} = this.props;

        return (
            <View style={styles.previewBlock} {...props}>
                <TouchableHighlight onPress={() => this.props.onCrossClicked()} style={styles.previewClose} underlayColor="0">
                    <Image source={require('../../assets/img/icons/close.png')} style={styles.previewCloseIcon}/>
                </TouchableHighlight>
                <View style={styles.blockSplit}>
                    <Image source={data.IMG} style={styles.previewImg}/>
                    <View style={styles.blockSplitContent}>
                        <View style={styles.blockTitles}>
                            <Text style={styles.previewSubtitle}>
                                {data.SUBTITLE.toUpperCase()}
                            </Text>
                            <Text style={styles.previewTitle}>
                                {data.TITLE.toUpperCase()}
                            </Text>
                            <Image source={require('../../assets/img/others/small_separation.png')} style={styles.previewSeparation}/>
                        </View>
                        <Text numberOfLines={6} style={styles.previewText}>
                            {data.TEXT}
                        </Text>
                        <TouchableHighlight onPress={() => this.props.onLaunchStory()} underlayColor="0">
                            <Image source={require('../../assets/img/icons/send.png')} style={styles.buttonSend}/>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    previewBlock: {
        position: 'absolute',
        width: winW * 0.74,
        height: winH * 0.69,
        top: winH / 2 - winH * 0.69 / 2,
        left: winW / 2 - winW * 0.74 / 2,
        backgroundColor: '#FFFFFF',
        zIndex: 3,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 50,
        paddingRight: 50,
        paddingTop: 80,
        paddingBottom: 80,
    },
    previewSubtitle: {
        fontSize: 36,
        fontFamily: 'GoodDog',
        color: '#ff6f6f'
    },
    previewTitle: {
        fontFamily: 'GoodDog',
        fontSize: 60,
        color: '#ff6f6f'
    },
    previewSeparation: {
        width: 88,
        height: 7,
        marginTop: 25
    },
    previewText: {
        color: '#28cece',
        fontSize: 15,
        textAlign: 'center'
    },
    previewImg: {
        width: 306,
        height: 350
    },
    previewClose: {
        position: 'absolute',
        top: 20,
        right: 20
    },
    previewCloseIcon: {
        width: 25,
        height: 25
    },
    blockSplit: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'row',
        width: '100%',
        height: '100%'
    },
    blockSplitContent: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: '100%',
        width: '50%'
    },
    blockTitles: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonSend: {
        width: 100,
        height: 74
    }
});

