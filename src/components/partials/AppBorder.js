/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions
} from 'react-native';

var winW = Dimensions.get('window').width;
var winH = Dimensions.get('window').height;

export default class AppBorder extends Component {
    render() {
        return (
            <Image source={require('../../assets/img/others/app_border.png')} style={styles.border}/>
        );
    }
}

const styles = StyleSheet.create({
    border: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: winW,
        height: winH,
        zIndex: 0
    },
});

