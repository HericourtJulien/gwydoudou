/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Image,
    Animated,
    Easing
} from 'react-native';
import * as Animatable from 'react-native-animatable';

export default class Pin extends Component {

    constructor() {
        super();


    }

    render() {
        let {item, id, ...props} = this.props;
        return (
            <View style={{
                position: 'absolute',
                top: item.PIN.POSITION.Y,
                left: item.PIN.POSITION.X,
                zIndex: 2
            }} key={id}>
                <TouchableHighlight
                    underlayColor="0"
                    onPress={() => this.props.onClick()}>
                    <Animatable.View
                        animation="bounceIn"
                        delay={1000}>
                        <Image
                            source={item.PIN.IMG}
                            style={{width: item.PIN.DIMENSIONS.WIDTH, height: item.PIN.DIMENSIONS.HEIGHT}}
                            />
                    </Animatable.View>
                </TouchableHighlight>
            </View>
        );
    }
}

