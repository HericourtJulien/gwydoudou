/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Image,
    AsyncStorage
} from 'react-native';
import * as Animatable from 'react-native-animatable';

export default class BlockTopRightMenu extends Component {

    constructor(props) {
        super(props);

        this.state = {
            sound: true
        }
    }

    onButtonPressed(action) {
        switch (action) {
            case 'settings':
                this.props.playSound(13);
                this.props.onParameterPress();
                return;
            case 'unlock':
                this.props.playSound(13);
                this.props.navigator.push({
                    id: 'UnlockNewStory'
                });
                return;
        }
    }

    toggleSound() {

        if (!this.state.sound) {
            this.props.playSound(13);
            this.props.muteAll(false);
        } else {
            this.props.muteAll(true);
        }

        this.setState({
            sound: !this.state.sound
        });
    }

    render() {
        return (
            <Animatable.View
                animation="bounceIn"
                style={styles.blockMenu}>
                <TouchableHighlight onPress={() => this.toggleSound()} style={styles.blockMenuIcon} underlayColor="0">
                    {this.state.sound ? (
                        <Image source={require('../../assets/img/icons/sound.png')} style={styles.blockMenuImage}/>
                    ) : (
                        <Image source={require('../../assets/img/icons/sound_off.png')} style={styles.blockMenuImage}/>
                    )}
                </TouchableHighlight>
                {this.props.settings !== false ? (
                    <TouchableHighlight onPress={() => this.onButtonPressed('settings')} style={styles.blockMenuIcon} underlayColor="0">
                        <Image source={require('../../assets/img/icons/settings.png')} style={styles.blockMenuImage}/>
                    </TouchableHighlight>
                ) : null}
            </Animatable.View>
        );
    }
}

const styles = StyleSheet.create({
    blockMenu: {
        flex: 1,
        height: 50,
        flexDirection: 'row',
        position: 'absolute',
        right: 40,
        top: 40,
        zIndex: 2
    },
    blockMenuIcon: {
        marginLeft: 10,
        height: 80,
        width: 80,
        justifyContent: 'center',
        alignItems: 'center'
    },
    blockMenuImage: {
        height: 75,
        width: 75
    }
});

