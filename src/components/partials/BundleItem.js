/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    AsyncStorage
} from 'react-native';
import Animator from '../interactions/Animator';
import * as Animatable from 'react-native-animatable';

let cpt = 0;

export default class BundleBlock extends Component {

    constructor(props) {
        super(props);

        this.state = {
            content: [],
            appear: false
        };
    }

    componentDidMount() {
        // AsyncStorage.setItem('trophy_1', JSON.stringify(true));
        // AsyncStorage.setItem('trophy_2', JSON.stringify(true));
        // AsyncStorage.setItem('trophy_3', JSON.stringify(true));

        let trophy = this.props.data.ID_NAME;
        let that = this;
        let maxTrophy = 3;

        if (trophy != null) {
            AsyncStorage.getItem(trophy, (error, result) => {
                result == 'true' ? that.setState({appear: true}) : that.setState({appear: false});
            });
        }
    }

    render() {
        return (
            <View style={styles.block}>
                <Animatable.Image source={this.props.data.BG} style={styles.img} animation={'bounceIn'}>
                    {typeof this.props.data.IMG == 'object' && this.state.appear ? (
                        <Animator
                            source={this.props.data.IMG}
                            duration={2500}
                            repeat={false}
                            style={styles.icon}
                        />
                    ) : null}
                </Animatable.Image>
                <Text style={styles.text}>
                    {this.state.appear ? this.props.data.TEXT.toUpperCase() : '???'}
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    block: {
        height: 306,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    img: {
        marginBottom: 30,
        width: 111,
        height: 168,
        position: 'relative'
    },
    text: {
        textAlign: 'center',
        color: '#0e7480'
    },
    icon: {
        position: 'absolute',
        width: 70,
        height: 168,
        marginLeft: 8
    }
});

