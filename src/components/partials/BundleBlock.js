/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
import BundleItem from './BundleItem';

export default class BundleBlock extends Component {

    constructor(props) {
        super(props);

        this.state = {
            content: []
        };
    }

    componentWillMount() {
        let elements = [];
        let element;
        this.props.data.ELEMENTS.map((item, index) => {
            element = (
                <BundleItem key={index} data={item} style={styles.block}/>
            );
            elements.push(element);
        });

        this.setState({
            content: elements
        });
    }

    render() {
        return (
            <View>
                <Text style={styles.title}>{this.props.data.TITLE.toUpperCase()}</Text>
                <View style={styles.block}>
                    {this.state.content}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    block: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: 472,
        height: 306,
        marginLeft: 79
    },
    title: {
        fontSize: 36,
        fontFamily: 'GoodDog',
        color: '#0e7480',
        textAlign: 'center'
    }
});

