/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image
} from 'react-native';


/** Define pages **********/
import Page_0 from '../../datas/stories/amazonia/pages/id_0/ID_0';
import Page_1 from '../../datas/stories/amazonia/pages/id_1/ID_1';
import Page_2 from '../../datas/stories/amazonia/pages/id_2/ID_2';
import Page_3 from '../../datas/stories/amazonia/pages/id_3/ID_3';
import Page_4 from '../../datas/stories/amazonia/pages/id_4/ID_4';
import Page_5 from '../../datas/stories/amazonia/pages/id_5/ID_5';
import Page_6 from '../../datas/stories/amazonia/pages/id_6/ID_6';
import Page_7 from '../../datas/stories/amazonia/pages/id_7/ID_7';
import Page_8 from '../../datas/stories/amazonia/pages/id_8/ID_8';
import Page_9 from '../../datas/stories/amazonia/pages/id_9/ID_9';
import Page_10 from '../../datas/stories/amazonia/pages/id_10/ID_10';
import Page_11 from '../../datas/stories/amazonia/pages/id_11/ID_11';
import Page_12 from '../../datas/stories/amazonia/pages/id_12/ID_12';

var winW = Dimensions.get('window').width;
var winH = Dimensions.get('window').height;

export default class Page extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: props.data,
            pageWanted: null,
            pages: {
                0: <Page_0 onNextPage={() => this.nextPage()} playSound={(id, repeat) => this.props.playSound(id, repeat)} muteAll={(bool) => this.props.muteAll(bool)} stopSound={(id) => this.props.stopSound(id)} adjustVolume={(id, value) => this.props.adjustVolume(id, value)} stopAll={() => this.props.stopAll()}/>,
                1: <Page_1 onNextPage={() => this.nextPage()} playSound={(id, repeat) => this.props.playSound(id, repeat)} muteAll={(bool) => this.props.muteAll(bool)} stopSound={(id) => this.props.stopSound(id)} adjustVolume={(id, value) => this.props.adjustVolume(id, value)} stopAll={() => this.props.stopAll()}/>,
                2: <Page_2 onNextPage={() => this.nextPage()} playSound={(id, repeat) => this.props.playSound(id, repeat)} muteAll={(bool) => this.props.muteAll(bool)} stopSound={(id) => this.props.stopSound(id)} adjustVolume={(id, value) => this.props.adjustVolume(id, value)} stopAll={() => this.props.stopAll()}/>,
                3: <Page_3 onNextPage={() => this.nextPage()} playSound={(id, repeat) => this.props.playSound(id, repeat)} muteAll={(bool) => this.props.muteAll(bool)} stopSound={(id) => this.props.stopSound(id)} adjustVolume={(id, value) => this.props.adjustVolume(id, value)} stopAll={() => this.props.stopAll()}/>,
                4: <Page_5 onNextPage={() => this.nextPage()} playSound={(id, repeat) => this.props.playSound(id, repeat)} muteAll={(bool) => this.props.muteAll(bool)} stopSound={(id) => this.props.stopSound(id)} adjustVolume={(id, value) => this.props.adjustVolume(id, value)} stopAll={() => this.props.stopAll()}/>,
                5: <Page_6 onNextPage={() => this.nextPage()} playSound={(id, repeat) => this.props.playSound(id, repeat)} muteAll={(bool) => this.props.muteAll(bool)} stopSound={(id) => this.props.stopSound(id)} adjustVolume={(id, value) => this.props.adjustVolume(id, value)} stopAll={() => this.props.stopAll()}/>,
                6: <Page_9 onNextPage={() => this.nextPage()} playSound={(id, repeat) => this.props.playSound(id, repeat)} muteAll={(bool) => this.props.muteAll(bool)} stopSound={(id) => this.props.stopSound(id)} adjustVolume={(id, value) => this.props.adjustVolume(id, value)} stopAll={() => this.props.stopAll()}/>,
                7: <Page_10 onNextPage={() => this.nextPage()} playSound={(id, repeat) => this.props.playSound(id, repeat)} muteAll={(bool) => this.props.muteAll(bool)} stopSound={(id) => this.props.stopSound(id)} adjustVolume={(id, value) => this.props.adjustVolume(id, value)} stopAll={() => this.props.stopAll()}/>,
                8: <Page_11 onNextPage={() => this.nextPage()} playSound={(id, repeat) => this.props.playSound(id, repeat)} muteAll={(bool) => this.props.muteAll(bool)} stopSound={(id) => this.props.stopSound(id)} adjustVolume={(id, value) => this.props.adjustVolume(id, value)} stopAll={() => this.props.stopAll()}/>,
                9: <Page_12 onNextPage={(home) => this.nextPage(home)} playSound={(id, repeat) => this.props.playSound(id, repeat)} muteAll={(bool) => this.props.muteAll(bool)} stopSound={(id) => this.props.stopSound(id)} adjustVolume={(id, value) => this.props.adjustVolume(id, value)} stopAll={() => this.props.stopAll()}/>,
            }
        }
    }

    componentWillMount() {
        this.checkPageWanted();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            data: nextProps.data
        }, () => this.checkPageWanted());
    }

    checkPageWanted() {
        var pageWanted = this.state.pages[this.state.data.ID];

        this.setState({
            pageWanted: pageWanted
        });
    }

    nextPage(home) {
        this.props.onNextPage(home);
    }

    render() {
        let {...props} = this.props;
        return (
            <View style={styles.container} {...props}>
                {this.state.pageWanted}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        height: winH - 116,
        width: winW
    }
});

