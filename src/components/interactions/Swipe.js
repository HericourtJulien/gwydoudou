/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

import Swiper from 'react-native-swiper';

export default class Swipe extends Component {

    componentDidMount() {
        console.log(this);
    }

    render() {
        let {children, showButtons, ...props} = this.props;

        return (
            <Swiper {...props}>
                {children}
            </Swiper>
        );
    }
}

const styles = StyleSheet.create({

});

