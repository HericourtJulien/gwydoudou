/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    AsyncStorage
} from 'react-native';

var Sound = require('react-native-sound');

export default class CloneComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            allSounds: [
                {
                    id: 0,
                    path: 'son-fond.wav',
                    obj: null
                },
                {
                    id: 1,
                    path: 'casacade-boucle.wav',
                    obj: null
                },
                {
                    id: 2,
                    path: 'croco-pleure-touch.wav',
                    obj: null
                },
                {
                    id: 3,
                    path: 'son-fond2.wav',
                    obj: null
                },
                {
                    id: 4,
                    path: 'croco-content-touch.wav',
                    obj: null
                },
                {
                    id: 5,
                    path: 'ding-victoire.wav',
                    obj: null
                },
                {
                    id: 6,
                    path: 'feuille-touch.wav',
                    obj: null
                },
                {
                    id: 7,
                    path: 'paresseux-ronchon-touch.wav',
                    obj: null
                },
                {
                    id: 8,
                    path: 'paresseux-touch.wav',
                    obj: null
                },
                {
                    id: 9,
                    path: 'river-boucle.wav',
                    obj: null
                },
                {
                    id: 10,
                    path: 'son-fond3.mp3',
                    obj: null
                },
                {
                    id: 11,
                    path: 'toucan-touch.wav',
                    obj: null
                },
                {
                    id: 12,
                    path: 'croix-touch.wav',
                    obj: null
                },
                {
                    id: 13,
                    path: 'pop-bouton.wav',
                    obj: null
                },
                {
                    id: 14,
                    path: 'mouches-boucle.wav',
                    obj: null
                },
            ]
        }
    }

    componentWillMount() {
        this.preloadSounds();
    }

    preloadSounds() {
        let i = 0;
        let maxSound = this.state.allSounds.length;
        let currentSound;

        for (i; i < maxSound; i++) {
            currentSound = this.state.allSounds[i];
            currentSound.obj = new Sound(currentSound.path, Sound.MAIN_BUNDLE, (error) => {
                if (error) {
                    console.log('Failed to load the sound', error);
                    return;
                }
                // loaded successfully
                console.log('Sound Loaded');
            });
        }

        this.forceUpdate();
    };

    playSound(id, repeat) {
        console.log("play");
        let soundNb = parseInt(id);
        this.state.allSounds[soundNb].obj.play();

        if(repeat){
            this.state.allSounds[soundNb].obj.setNumberOfLoops(-1);
        }
    }

    stopSound(id){
        let soundNb = parseInt(id);
        this.state.allSounds[soundNb].obj.stop();
    }

    adjustVolume(id, value){
        console.log("volume", value);
        let soundNb = parseInt(id);
        this.state.allSounds[soundNb].obj.setVolume(value);
    }

    muteAll(bool) {
        let i = 0;
        let maxSound = this.state.allSounds.length;
        let currentSound;

        for (i; i < maxSound; i++) {
            currentSound = this.state.allSounds[i];
            bool ? currentSound.obj.setVolume(0) : currentSound.obj.setVolume(1);
        }

        this.forceUpdate();
    }

    stopAll() {
        console.log("stop all");
        let i = 0;
        let maxSound = this.state.allSounds.length;
        let currentSound;

        for (i; i < maxSound; i++) {
            currentSound = this.state.allSounds[i];
            currentSound.obj.stop();
        }

        this.forceUpdate();
    }

    render() {
        return (
            <View/>
        );
    }
}

