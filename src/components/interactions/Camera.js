/**
 * Created by benja on 24/04/2017.
 */

'use strict';
import React, {Component} from 'react';
import {
    Dimensions,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    Image
} from 'react-native';
import NativeCamera from 'react-native-camera';
import Animator from '../interactions/Animator';
import DATAS from '../../datas/Datas';
import * as Animatable from 'react-native-animatable';
import IntAnim from '../../datas/InterfaceAnimations';

let winSize = Dimensions.get('window');
let winW = winSize.width;
let winH = winSize.height;
let imageWidth = 875;
let imageHeight = 621;


//var for AR cards
let stories = [];
let assets = [];

let storyNumber = DATAS.STORIES.length;
let i = 0;
let characterStyle;
let characterAnimation;

export default class Camera extends Component {

    /**
     * constructor holding state params linked and used in the readQR function,
     * following what camera is seeing
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            bounds: {
                origin: {
                    x: 0,
                    y: 0
                },
                size: {
                    height: 0,
                    width: 0
                }
            },
            opacity: 1,
            stringToUnlock: props.stringToUnlock,
            callbackAction: props.action,
            isNewStory: props.isNewStory,
            positionReceiver: props.positionReceiver,
            cardVisible:0,
            cardShown:false
        };
        this.callToActionShown = false;
        this.storyToPlay;
        this.passed = false;

        this.cardAssetsLoaded = {
            background : false,
            character : false,
            foreground : false
        };
    }

    componentWillUnmount(){
        console.log("camera unmount");
    }

    launchStory(datas) {
        console.log(datas);

        this.props.navigator.push({
            id: 'Story',
            data: datas
        });
    }

    readQR(e) {


        this.props.onDetect();


        if (e.data === this.state.stringToUnlock) {
            this.state.callbackAction !== undefined ? this.state.callbackAction() : console.log("no action callback");
        }else if(this.state.isNewStory === true){
            this.state.callbackAction !== undefined ? this.state.callbackAction(e.data) : console.log("no action callback");
        }

        //Android does not support bounds on qr code read system
        if (!e.bounds) {
            this.setState({
                bounds: {
                    origin: {
                        x: winW / 2 - imageWidth / 2,
                        y: winH / 2 - imageHeight / 2
                    }, size: {
                        width: imageWidth,
                        height: imageHeight
                    }
                },
                data: e.data
            });
        } else {
            this.setState({
                bounds: e.bounds,
                data: e.data
            });

        }

        if(!this.state.cardShown && this.state.isNewStory){

            this.setState({
                cardShown: true
            });

            console.log("card shown", this.state.cardShown);

            for(i; i<storyNumber; i++){
                let story = DATAS.STORIES[i];
                stories.push(story);

                if(story.CARD.UNLOCK === e.data){
                    for(let j=0; j<story.CARD.ASSETS.length; j++){
                        assets.push(story.CARD.ASSETS[j].IMG);
                    }
                    characterStyle = story.CARD.STYLES.CHARACTER;
                    characterAnimation = story.CARD.STYLES.ANIMATION;

                    this.storyToPlay = story;
                }
            }
        }

        //check if assets are loaded
       if(this.cardAssetsLoaded.background /*&& this.cardAssetsLoaded.character*/ && this.cardAssetsLoaded.foreground && !this.passed){

            this.passed = true;

            this.setState({
                cardVisible : 1
            });

            this.callToActionShown = true;
       }

    }

    render() {

        //le "..." -> va checker les propriétés liés au code dans les balises du render, qui sont définsi das les props
        let {children, ...props} = this.props;

        return (
            <View style={styles.container}>

                <NativeCamera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    style={styles.preview}
                    aspect={NativeCamera.constants.Aspect.fill}
                    onBarCodeRead={this.readQR.bind(this)}>

                    {this.state.cardShown === true ?
                        <View
                            style={{
                                width: 875,
                                height: 621,
                                position: 'absolute',
                                top:parseInt(this.state.bounds.origin.y - imageHeight + 150),
                                left:parseInt(this.state.bounds.origin.x - imageWidth + 150),

                            }}
                        >
                            <Image
                                style={cardStyleSheet.cardBackground}
                                opacity={this.state.cardVisible}
                                source={assets[0]}
                                onLoad={() => {this.cardAssetsLoaded.background = true ; console.log("background loaded")}}
                            />
                            {/*<Animatable.View animation={characterAnimation} delay={0} duration={5000} style={{position:'absolute', bottom:0, right:0, height:250, width:200}}>
                                <Image
                                     style={cardStyleSheet.cardChar}
                                     opacity={this.state.cardVisible}
                                     source={(assets[1])}
                                     onLoad={() => {this.cardAssetsLoaded.character = true ; console.log("character loaded")}}
                                 />
                            </Animatable.View>*/}

                            <Image
                               style={cardStyleSheet.cardPlants}
                               opacity={this.state.cardVisible}
                               source={(assets[2])}
                               onLoad={() => {this.cardAssetsLoaded.foreground = true ; console.log("foreground loaded")}}
                            />

                        </View>
                    : null }

                    {children}

                    {this.callToActionShown === true ?

                        <View style={styles.callView}>
                            <Animatable.Image
                                animation={IntAnim.ANIMATIONS.CTA}
                                iterationCount={'infinite'}
                                duration={5000}
                                style={styles.callText}
                                source={require('../../assets/img/icons/CTA.png')}
                            />

                            <TouchableHighlight
                                onPress={() => {
                                    this.launchStory(this.storyToPlay);
                                }}
                                underlayColor="0"
                            >
                                <Image
                                    style={styles.callImage}
                                    source={require('../../assets/img/icons/go.png')}
                                />
                            </TouchableHighlight>

                        </View>

                    : null}

                </NativeCamera>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        position: 'relative',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        //width: 120,
        //height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 30
    },
    scan: {
        width: 100,
        height: 74,
        position: 'absolute',
        bottom: 40
    },
    callView:{
        flex:1,
        justifyContent:'flex-end',
        alignItems:'center',
        marginBottom:50
    },
    callText:{
        width:340,
        height:100
    },
    callImage:{
        width:110,
        height:80
    }
});

const cardStyleSheet = StyleSheet.create({
    cardBackground: {
        position:'absolute',
        width:"100%",
        height:"100%",
        top:0,
        left:0,
    },
    cardPlants: {
        position:'absolute',
        right:0,
        bottom:0,
    },
    cardChar: {
        position:'absolute',
        bottom:0,
        right:0,
    }
});