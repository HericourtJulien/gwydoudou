/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableHighlight
} from 'react-native';
import * as Animatable from 'react-native-animatable';

TouchAnimatable = Animatable.createAnimatableComponent(TouchableHighlight);


export default class Touch extends Component {
    render() {
        let {children, ...props} = this.props;

        return (
            <TouchAnimatable {...props}>
                <View>
                    {children}
                </View>
            </TouchAnimatable>
        );
    }
}

