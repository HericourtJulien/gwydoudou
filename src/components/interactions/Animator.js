/**
 * Created by benja on 01/05/2017.
 */

'use strict';

import React, {Component} from 'react';
import {
    Dimensions,
    StyleSheet,
    Animated,
    Easing,
    Text,
    View,
    Image
} from 'react-native';
import Animation from 'lottie-react-native';

let Window = Dimensions.get('window');

/**
 * Animation Component
 * React-native component using lottie-react-native
 * display an animation with custom param
 * @param width   : int
 * @param height   : int
 * @param repeat   : bool
 * @param duration   : int
 */
export default class Animator extends Component {

    constructor(props) {
        super(props);

        this.state = {
            progress: new Animated.Value(0),
            duration: props.duration,
            repeat: props.repeat,
            source: props.source,
            backgroundColor: props.backgroundColor,
            delay: props.delay,
            reverse: props.reverse
        };
    }

    componentDidMount() {
        this.animate();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            progress: new Animated.Value(0),
            duration: nextProps.duration,
            repeat: nextProps.repeat,
            source: nextProps.source,
            backgroundColor: nextProps.backgroundColor,
            delay: nextProps.delay,
            reverse: nextProps.reverse
        });
        this.animate();
    }

    animate = () => {
        if(this.state.reverse !== true || this.state.reverse === undefined){

            //set state to begin to a value of 0 for current animation
            this.state.progress.setValue(0);

            ///animating system
            Animated.timing(this.state.progress, {
                toValue: 1,
                duration: this.state.duration,
                easing: Easing.linear,
            }).start(() => {
                this.state.progress.setValue(0);
                if (this.state.delay > 0) {
                    setTimeout(() => {
                            this.animate();
                    }, this.state.delay);
                }else{
                    this.animate();
                }
                if(this.state.repeat === true){
                    this.animate();
                }
            });

        }else{

            //play animation in reverse mode
            this.state.progress.setValue(1);

            Animated.timing(this.state.progress, {
                toValue: 0,
                duration: this.state.duration,
                easing: Easing.linear,
            }).start(() => {
                this.state.progress.setValue(1);
                if (this.state.delay > 0) {
                    setTimeout(() => {
                        this.animate();
                    }, this.state.delay);
                }
                if(this.state.repeat === true){
                    this.animate();
                }
            });

        }
    };

    render() {
        let {...props} = this.props;

        return (
            <Animation
                source={this.state.source}
                progress={this.state.progress}
                delay={this.state.delay}
                {...props}
            />
        )
    }

}
