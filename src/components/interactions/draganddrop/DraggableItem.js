/**
 * Created by benja on 03/05/2017.
 */
'use strict';
import React, {Component} from 'react';
import{
    StyleSheet,
    View,
    Text,
    PanResponder,
    Animated,
    Dimensions
} from 'react-native';

export default class DraggableItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            width: props.width,
            height: props.height,
            x: props.x,
            y: props.y,
            color: props.color,
            source: props.source,
            lockX: props.lockX,
            lockY: props.lockY,
            maxX: props.maxX,
            maxY: props.maxY,
            minX: props.minX,
            minY: props.minY,
            toX: props.toX,
            toY: props.toY,
            gestureHandler: props.gestureHandler,
            animation: props.mainAnimation,
            callback: props.callback,
            hideAnimation: props.hideAnimation,
            pan: new Animated.ValueXY()
        };

        this.oldPosX = 0;
        this.oldPosY = 0;

        this.canMoveX = true;
        this.canMoveY = true;
    }

    componentWillMount() {
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => true, //allow movement capture

            onPanResponderMove: (e, gesture) => {

                //check if draggableItem has constraints
                let positions = this.moveElement(gesture);

                if (!this.state.lockX && !this.state.lockY) {
                    Animated.spring(
                        this.state.pan,
                        {
                            toValue: {x: positions.newX - this.state.x, y: positions.newY - this.state.y}
                        }
                    ).start();
                } else {
                    Animated.spring(
                        this.state.pan,
                        {
                            toValue: {x: positions.newX, y: positions.newY}
                        }
                    ).start();
                }

            },

            onPanResponderRelease: (e, gesture) => { //when release the element

                Animated.spring(
                    this.state.pan,
                    {toValue: {x: 0, y: 0}}
                ).start();

                if (this.canMoveX === false || this.canMoveY === false) {
                    return;
                }

                // Flatten the offset to avoid erratic behavior
                this.state.pan.flattenOffset();

                let currentDropZone = this.state.gestureHandler(gesture);

                if (currentDropZone !== false) {

                    if (currentDropZone !== undefined) {
                        Animated.spring(
                            this.state.pan,
                            {
                                toValue: {
                                    x: currentDropZone.props.toX,
                                    y: currentDropZone.props.toY
                                }
                            }
                        ).start();
                    }

                    this.state.callback !== undefined ? this.state.callback() : null;
                }
            }
        });
    }

    moveElement(gesture) {
        //check if it is possible to move or not the draggable item
        //cause it is possible that it has constraints on axis

        let newX, newY;

        if (this.state.width === undefined || this.state.height === undefined) {
            console.warn("draggable item width or height is not defined");
            console.error("it won't move !");
        }

        //check if there are constrains on axis
        if (this.state.maxX !== undefined && this.state.minX !== undefined || this.state.maxY !== undefined && this.state.minY !== undefined) {
            if (gesture.moveX <= this.state.maxX && gesture.moveX > this.state.minX) {
                newX = gesture.moveX - this.state.width / 2;
                this.canMoveX = true;
            } else {
                newX = this.oldPosX;
                this.canMoveX = false;
            }

            if (gesture.moveY <= this.state.maxY && gesture.moveY > this.state.minY) {
                newY = gesture.moveY - this.state.height / 2;
                this.canMoveY = true;
            } else {
                newY = this.oldPosY;
                this.canMoveY = false;
            }
        } else {
            console.log("max and min value for draggable item");
            console.log("one of them is not defined");

            console.log("min X ", this.state.minX);
            console.log("max X ", this.state.maxX);
            console.log("min Y ", this.state.minY);
            console.log("max Y ", this.state.maxY);

            newX = this.state.pan.x;
            newY = this.state.pan.y;
        }

        if (this.state.lockX) {
            newX = this.oldPosX;
            this.canMoveX = true;
        }

        if (this.state.lockY) {
            newY = this.oldPosY;
            this.canMoveY = true;
        }

        this.oldPosX = newX;
        this.oldPosY = newY;

        return {newX, newY};
    }

    componentDidMount() {

    }

    render() {

        let {children, ...props} = this.props;

        return (
            <View style={{
                top: this.state.y,
                left: this.state.x,
                position: "absolute",
                width: "100%",
                height: "100%",
                zIndex: 3
            }}
            >
                <Animated.View
                    {...this._panResponder.panHandlers} //we assign the handlers of the PanResponder to the Animated.View. We are using the spread syntax to avoid assigning one by one.
                    style={[this.state.pan.getLayout(), {
                        backgroundColor: this.state.color,
                        width: this.state.width,
                        height: this.state.height,
                    }]} //we set the styles to animate from the animation. The getLayout method returns the left and top properties with the correct values for each frame during the animation.
                >
                    {children}
                </Animated.View>
            </View>
        );
    }
}