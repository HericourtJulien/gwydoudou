   
 **DONT FORGET TO**
 add a reference for your dropzone
 
` ref={(dropzone) => { this.dropzone = dropzone; }} `
 
**AND**

add a gesture handler prop to your draggable item

`gestureHandler={this.gestureManager.bind(this)}`
 
 and the gesture manager method
 
 ` gestureManager(gesture){
      return(this.dropzone.isOverDropZone(gesture));
    }
      `

**USAGE FOR MULTIPLE DROPZONES**
1) in the constructor, add an array of dropzone :

` this.dropZones = {
             1: null,
             2: null,
             3: null,
             4: null
         };`
         
2) modify the gesture manager function to handle multiple dropzones as folow :

`
gestureManager(gesture) { 
     let dropzoneToUse = null;
     Object.keys(this.dropZones).map((key, index) => {
         let dropzone = this.dropZones[key].isOverDropZone(gesture);
         dropzone != false ? dropzoneToUse = dropzone : false;
     });
     if(dropzoneToUse != false) {
         return dropzoneToUse;
     }
}
`

3) be sure to add a ref to each dropzone in your render method

`ref={(dropzone1) => {
                             this.dropZones[1] = dropzone1;
                         }}`
                         
4) you should add a width and height props to the draggable item, outside of its stylesheet


///////////////////////////////////////////////////////
**Example for a draggable item in a render method**
` <DragItem
         gestureHandler={(gesture) => this.gestureManager(gesture)}
         callback={() => alert("over dropzone")}
         style={removableLeaf.containerBG}
         minX={0}
         maxX={winW/2}
         minY={winH/2}
         maxY={winH}
         width={500}
         height={750}
     >
         <Animator
             source={require("./assets/json/feuille2-basG.json")}
             duration={3500}
             repeat={true}
             style={removableLeaf.BG}
         />
     </DragItem>`