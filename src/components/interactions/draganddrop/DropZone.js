/**
 * Created by benja on 03/05/2017.
 */
'use strict';
import React, {Component} from 'react';
import{
    StyleSheet,
    View,
    Text,
    PanResponder,
    Animated,
    Dimensions
} from 'react-native';

export default class DropZone extends Component{
    constructor(props){
        super(props);

        this.state = {
            width: props.width,
            height: props.height,
            x: props.x,
            y: props.y,
            color: props.color,
            dropZoneValues: null,
            id: props.id,
            lockX:props.lockX,
            lockY:props.lockY
        }
    }

    componentDidMount(){

    }

    setDropZoneValues(event){
        this.setState({
            dropZoneValues: event.nativeEvent.layout
        });
    }

    isOverDropZone(gesture){

        let dzValues = this.state.dropZoneValues;

        let normalCalcul = (gesture.moveX > dzValues.x && gesture.moveX < dzValues.x + dzValues.width &&
        gesture.moveY > dzValues.y && gesture.moveY < dzValues.y + dzValues.height);

        let calculLockX = (gesture.moveX > dzValues.x && gesture.moveX && gesture.moveY > dzValues.y && gesture.moveY < dzValues.y + dzValues.height);
        let calculLockY = (gesture.moveX > dzValues.x && gesture.moveX < dzValues.x + dzValues.width && gesture.moveY > dzValues.y && gesture.moveY);

        if(this.state.lockX){
            if(calculLockX){
                console.log("over with locked X axis");
                return this;
            }
        }else if(this.state.lockY){
            if(calculLockY){
                console.log("over with locked Y axis");
                return this;
            }
        }else{
            if(normalCalcul){
                console.log("over");

                console.log(this);

                return this;
            }
        }

        return false;

        /*if(gesture.moveX > dzValues.x && gesture.moveX < dzValues.x + dzValues.width &&
            gesture.moveY > dzValues.y && gesture.moveY < dzValues.y + dzValues.height){
            console.log("over");
            return this;
        }*/
    }

    render(){
        return(
            <View style={{
                width:this.state.width,
                height:this.state.height,
                top:this.state.y,
                left:this.state.x,
                backgroundColor:this.state.color,
                position:'absolute',
                zIndex: 2
            }}
            onLayout={this.setDropZoneValues.bind(this)}
            >
            </View>
        );
    }

}


