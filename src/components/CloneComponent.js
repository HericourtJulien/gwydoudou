/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

export default class CloneComponent extends Component {
    render() {
        return (
            <View>
                <Text style={styles.title}>
                    Title component
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    title: {
        color: '#FF0000',
    },
});

