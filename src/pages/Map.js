/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    TouchableHighlight,
    Modal,
    Animated,
    Easing,
    AsyncStorage
} from 'react-native';
import Pin from '../components/partials/Pin';
import BlockTopRightMenu from '../components/partials/BlockTopRightMenu';
import BlockTopLeftMenu from '../components/partials/BlockTopLeftMenu';
import DATA from '../datas/Datas';
import StoryPreview from '../components/partials/StoryPreview';
import Popup from '../components/partials/Popup';
import Sounds from '../components/interactions/Sounds';
import * as Animatable from 'react-native-animatable';
import AppBorder from '../components/partials/AppBorder';
import IntAnim from '../datas/InterfaceAnimations';


let winW = Dimensions.get('window').width;
let winH = Dimensions.get('window').height;

const CustomTouchableHighlight = Animatable.createAnimatableComponent(TouchableHighlight);

class Map extends Component {

    constructor() {
        super();

        this.state = {
            pins: {},
            preview: {},
            settingsPopup: [],
            settingsPopupTitle: 'Espace réservé à Papa et Maman !',
            firstTimePopup: []
        };
    }

    onButtonPressed(action) {
        switch (action) {
            case 'unlock':
                this.refs.sounds.playSound(13);
                this.props.navigator.push({
                    id: 'UnlockNewStory'
                });
                return;
        }
    }

    componentWillMount() {
        // TEMP
        // AsyncStorage.removeItem('mapFirstTime');

        AsyncStorage.getItem('mapFirstTime', (error, result) => {
            if (result == null) {
                this.createFirstTimePopup();
                AsyncStorage.setItem('mapFirstTime', JSON.stringify(true));
            }
        });

    }

    createFirstTimePopup() {
        AsyncStorage.getItem('user', (error, result) => {
            let user = JSON.parse(result);
            if (user !== null) {
                let childName = user.child_name;
                let isGirl = user.girl;

                let popup = [(
                    <Popup playSound={(id) => this.refs.sounds.playSound(id)}>
                        <Image source={require('../assets/img/gifs/welcome.gif')}
                               style={settingsStyle.firstTime}/>

                        <Text style={settingsStyle.firstTimeSubtitle}>
                            {/*BIENVENUE {childName} !{'\n'}{isGirl ? 'PRÊTE' : 'PRÊT'} POUR L'AVENTURE ?*/}
                            BIENVENUE Zoé ! {'\n'}  PRÊTE POUR L'AVENTURE ?
                        </Text>
                    </Popup>
                )];

                this.setState({
                    firstTimePopup: popup
                });
            }
        });


    }

    componentDidMount() {
        this.positionPins();
    }

    positionPins() {
        let pinsComponent = [];
        let previewComponent = [];

        AsyncStorage.getItem("displayPin", (err, result) => {
            if(result === null){

                let item = DATA.STORIES[1];

                pinsComponent.push(
                    <Pin item={item} id={item.ID} source={item.PIN.source}
                         onClick={(id) => this.togglePreview(item.ID, 'open')}/>
                );
                previewComponent.push(
                    <Animatable.View id={item.ID} style={styles.blockResumeView}>
                        <StoryPreview
                            data={item.RESUME}
                            onCrossClicked={(id) => this.togglePreview(item.ID, 'close')}
                            onLaunchStory={(id) => this.launchStory(item)}/>
                    </Animatable.View>
                );
            }else{
                DATA.STORIES.map((item, index) => {
                    pinsComponent.push(
                        <Pin key={index} item={item} id={item.ID} source={item.PIN.source}
                             onClick={(id) => this.togglePreview(item.ID, 'open')}/>
                    );
                    previewComponent.push(
                        <Animatable.View key={index} id={item.ID} style={styles.blockResumeView}>
                            <StoryPreview
                                data={item.RESUME}
                                onCrossClicked={(id) => this.togglePreview(item.ID, 'close')}
                                onLaunchStory={(id) => this.launchStory(item)}/>
                        </Animatable.View>
                    );
                });
            }

            this.setState({
                pins: pinsComponent,
                preview: previewComponent
            });

        });


    }

    launchStory(data) {
        this.refs.sounds.playSound(13);

        this.props.navigator.push({
            id: 'Story',
            data: data
        });
    }

    togglePreview(id, type) {
        var clonedElement;
        var clonedElements = [];

        type == 'open' ? this.refs.sounds.playSound(13) : this.refs.sounds.playSound(12);;

        this.state.preview.map((item, index) => {
            if (item.props.id == id) {
                clonedElement = React.cloneElement(item, {
                    animation: type == 'open' ? IntAnim.ANIMATIONS.PREVIEW.START : IntAnim.ANIMATIONS.PREVIEW.END,
                    duration: 600
                });
                clonedElements.push(clonedElement);
            } else {
                clonedElements.push(item);
            }
            this.refs.mask.animate(type == 'open' ? IntAnim.ANIMATIONS.MASK.START : IntAnim.ANIMATIONS.MASK.END, 600);
        });

        this.setState({
            preview: clonedElements
        });
    }

    createPopup() {
        let content = [];
        let mask = (
            <Animatable.View key={0} delay={100} animation={IntAnim.ANIMATIONS.MASK.START} style={settingsStyle.mask}/>
        );
        let popup = (
            <Animatable.View key={1} delay={100} animation={IntAnim.ANIMATIONS.PREVIEW.START}
                             style={settingsStyle.fullBlock}>
                <View style={settingsStyle.popup}>
                    <TouchableHighlight onPress={() => this.closePopup()} style={settingsStyle.popupClose}
                                        underlayColor="0">
                        <Image source={require('../assets/img/icons/close.png')} style={settingsStyle.popupCloseIcon}/>
                    </TouchableHighlight>
                    <Image source={require('../assets/img/icons/lock.png')} style={settingsStyle.lock}/>
                    <Text style={settingsStyle.calcul}>
                        6 <Text style={settingsStyle.calculX}>x</Text> 7
                    </Text>

                    <View style={settingsStyle.flex}>
                        <TouchableHighlight onPress={() => this.closePopup()} underlayColor="0">
                            <Image source={require('../assets/img/settings/reponse1.png')} style={settingsStyle.answer}>
                                <Text style={settingsStyle.answerText}>36</Text>
                            </Image>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => this.closePopup()} underlayColor="0">
                            <Image source={require('../assets/img/settings/reponse2.png')} style={settingsStyle.answer}>
                                <Text style={settingsStyle.answerText}>39</Text>
                            </Image>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => {
                            this.refs.sounds.playSound(13);
                            this.props.navigator.push({
                                id: 'Settings'
                            });
                        }} underlayColor="0">
                            <Image source={require('../assets/img/settings/reponse3.png')} style={settingsStyle.answer}>
                                <Text style={settingsStyle.answerText}>42</Text>
                            </Image>
                        </TouchableHighlight>
                    </View>


                    <Text style={settingsStyle.title}>
                        {this.state.settingsPopupTitle}
                    </Text>

                    <Text style={settingsStyle.subtitle}>
                        Donne la bonne réponse pour accéder aux paramètres.
                    </Text>
                </View>
            </Animatable.View>
        );

        content.push(mask);
        content.push(popup);

        this.setState({
            settingsPopup: content
        });
    }

    closePopup() {
        let mask = this.state.settingsPopup[0];
        let popup = this.state.settingsPopup[1];
        let clonedElements = [];
        let clonedElement;

        this.refs.sounds.playSound(12);

        clonedElement = React.cloneElement(mask, {
            animation: IntAnim.ANIMATIONS.MASK.END,
            duration: 600
        });

        clonedElements.push(clonedElement);

        clonedElement = React.cloneElement(popup, {
            animation: IntAnim.ANIMATIONS.PREVIEW.END,
            duration: 600
        });

        clonedElements.push(clonedElement);

        this.setState({
            settingsPopup: clonedElements
        });

        /** Temp **********/
        // setTimeout(() => this.unlockApp(), 1000);
    }

    togglePin(){
        AsyncStorage.removeItem("displayPin");
    }

    render() {
        return (
            <Image source={require('../assets/img/others/bg_map.png')} style={styles.backgroundImageContainer}>
                <Sounds ref={'sounds'}/>
                <AppBorder/>
                <Animatable.View ref="mask" style={styles.mask}/>
                <BlockTopLeftMenu navigator={this.props.navigator} type={'bundle'} muteAll={(bool) => this.refs.sounds.muteAll(bool)} playSound={(id) => this.refs.sounds.playSound(id)}/>
                <BlockTopRightMenu navigator={this.props.navigator} onParameterPress={() => this.createPopup()} muteAll={(bool) => this.refs.sounds.muteAll(bool)} playSound={(id) => this.refs.sounds.playSound(id)}/>
                {this.state.pins.length > 0 ? this.state.pins : null}
                {this.state.preview.length > 0 ? this.state.preview : null}
                {this.state.settingsPopup.length > 0 ? this.state.settingsPopup : null}

                {this.state.firstTimePopup.length > 0 ? this.state.firstTimePopup : null}

                <Animatable.Image
                    animation={IntAnim.ANIMATIONS.CTA}
                    iterationCount={'infinite'}
                    duration={5000}
                    source={require('../assets/img/others/cta_map.png')}
                    style={styles.cta}/>
                <CustomTouchableHighlight animation="bounceIn" onPress={this.onButtonPressed.bind(this, 'unlock')}
                                          style={styles.blockUnlock} underlayColor="0">
                    <Image source={require('../assets/img/icons/new_card.png')} style={styles.unlockNewStoryImg}/>
                </CustomTouchableHighlight>

                {/*Cheat code*/}
                <TouchableHighlight onPress={() => this.togglePin()} style={styles.cheatCode} underlayColor="0">
                    <View/>
                </TouchableHighlight>
            </Image>
        );
    }
}

const styles = StyleSheet.create({
    backgroundImageContainer: {
        flex: 1,
        width: winW,
        height: winH,
        backgroundColor: 'transparent',
    },
    blockResumeView: {
        position: 'absolute',
        width: winW,
        height: winH,
        top: -winH,
        left: 0
    },
    blockUnlock: {
        height: 75,
        width: 100,
        position: 'absolute',
        left: winW / 2 - 50,
        bottom: 40,
        justifyContent: 'flex-end',
        alignItems: 'center',
        zIndex: 2,
    },
    unlockNewStoryImg: {
        width: 100,
        height: 75
    },
    mask: {
        position: 'absolute',
        top: -winH,
        left: 0,
        width: winW,
        height: winH,
        backgroundColor: '#28cece',
        opacity: 0,
        zIndex: 3
    },
    cta: {
        position: 'absolute',
        bottom: 120,
        width: 405,
        height: 98,
        left: winW / 2 - 202,
    },
    firstTime: {
        width: 314,
        height: 295
    },
    cheatCode: {
        position: 'absolute',
        width: 150,
        height: 80,
        right: 0,
        bottom: 0,
        //backgroundColor:"red"
    }
});

const settingsStyle = StyleSheet.create({
    mask: {
        position: 'absolute',
        top: -winH,
        left: 0,
        width: winW,
        height: winH,
        backgroundColor: '#ffffff',
        opacity: 0,
        zIndex: 3
    },
    popup: {
        position: 'absolute',
        width: winW * 0.74,
        height: winH * 0.69,
        top: winH / 2 - winH * 0.69 / 2,
        left: winW / 2 - winW * 0.74 / 2,
        backgroundColor: '#FFFFFF',
        zIndex: 3,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    popupClose: {
        position: 'absolute',
        top: 20,
        right: 20
    },
    popupCloseIcon: {
        width: 25,
        height: 25
    },
    gif: {
        width: '60%',
        height: '60%'
    },
    lock: {
        width: 78,
        height: 93,
        marginBottom: 16
    },
    answer: {
        width: 100,
        height: 74,
        marginBottom: 32
    },
    answerText: {
        width: 100,
        lineHeight: 74,
        textAlign: 'center',
        fontSize: 30,
        color: '#fff'
    },
    flex: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '60%'
    },
    calcul: {
        fontFamily: 'GoodDog',
        fontSize: 110,
        color: '#ff6f6f',
        marginBottom: 24
    },
    calculX: {
        fontSize: 50,
    },
    title: {
        fontFamily: 'GoodDog',
        fontSize: 36,
        color: '#ff6f6f',
        marginBottom: 12
    },
    subtitle: {
        color: '#28cece'
    },
    firstTime: {
        width: 314,
        height: 294
    },
    firstTimeSubtitle: {
        fontSize: 38,
        color: '#28cece',
        fontFamily: 'GoodDog',
        textAlign: 'center',
        marginTop: 32
    },
});

module.exports = Map;