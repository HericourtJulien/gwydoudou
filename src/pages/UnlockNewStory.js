/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableHighlight,
    Image,
    Dimensions,
    AsyncStorage
} from 'react-native';

import CameraSystem from '../components/interactions/CameraStory';
import AppBorder from '../components/partials/AppBorder';
import BlockMenu from '../components/partials/BlockTopRightMenu';
import BlockTopLeftMenu from '../components/partials/BlockTopLeftMenu';
import Popup from '../components/partials/Popup';
import Sounds from '../components/interactions/Sounds';
import * as Animatable from 'react-native-animatable';
import IntAnim from '../datas/InterfaceAnimations';

let winW = Dimensions.get('window').width;
let winH = Dimensions.get('window').height;


class UnlockNewStory extends Component {

    constructor(props) {
        super(props);

        this.state = {
            popup: [],
            card: null,
            willUnmount: false
        };
    }

    componentDidMount() {
        // TEMP
        AsyncStorage.removeItem('new_card');
        AsyncStorage.setItem('displayPin', JSON.stringify(true));

        AsyncStorage.getItem('new_card', (error, result) => {
            if (result == null) {
                setTimeout(() => {
                    this.createPopup();
                }, 300);
                AsyncStorage.setItem('new_card', JSON.stringify(true));
            }
        });

    }

    unmount() {
        console.log('unmount');
    }

    componentWillUnmount() {
        console.log('will unmount new story')
    }


    goToStory(datas){
        this.setState({
            willUnmount: true
        });

        console.log("Aaaaaaah");
        console.log(datas);

            this.props.navigator.push({
                id: 'Story',
                data: false
            });
    }


    createPopup() {
        let popup = [(
            <Popup playSound={(id) => this.refs.sounds.playSound(id)}>
                <Image source={require('../assets/img/gifs/tuto_carte_ra.gif')} style={styles.gif}/>
                <Text style={styles.subtitle}>
                    Survole ta carte postale pour{"\n"} débloquer l'histoire
                </Text>
            </Popup>
        )];

        this.setState({
            popup: popup
        });
    }

    render() {

        return (
            <View style={styles.container}>

                {this.state.willUnmount === false ? ( <CameraSystem
                        navigator={this.props.navigator}
                        isNewStory={true}
                        action={() => {}}
                        onDetect={() => {}}
                        goToStory={(datas) => this.goToStory(datas)}

                    >
                        <Sounds ref={'sounds'}/>


                        {/*<AppBorder/>*/}

                        {/*Border*/}
                        <Image source={require('../assets/img/others/app_left_border.png')} style={styles.leftBorder}/>
                        <Image source={require('../assets/img/others/app_right_border.png')}
                               style={styles.rightBorder}/>
                        <Image source={require('../assets/img/others/app_top_border.png')} style={styles.topBorder}/>
                        <Image source={require('../assets/img/others/app_bottom_border.png')}
                               style={styles.bottomBorder}/>

                        <BlockMenu
                            navigator={this.props.navigator}
                            settings={false}
                            muteAll={(bool) => this.refs.sounds.muteAll(bool)}
                            playSound={(id) => this.refs.sounds.playSound(id)}
                            stopAll={() => this.refs.sounds.stopAll()}
                        />
                        <BlockTopLeftMenu
                            navigator={this.props.navigator}
                            type={'map'}
                            muteAll={(bool) => this.refs.sounds.muteAll(bool)}
                            playSound={(id) => this.refs.sounds.playSound(id)}
                            stopAll={() => this.refs.sounds.stopAll()}
                        />

                        {this.state.popup}

                    </CameraSystem>
                ) : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    largeText: {
        fontFamily: "Helvetica"
    },
    blockHome: {
        flex: 0,
        position: 'absolute',
        marginRight: 'auto',
        top: 10,
        left: 10
    },
    blockHomeIcon: {
        height: 50,
        width: 50,
        //backgroundColor: '#ff00ff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    gif: {
        width: '60%',
        height: '60%'
    },
    subtitle: {
        fontSize: 38,
        color: '#28cece',
        fontFamily: 'GoodDog',
        textAlign: 'center',
        marginTop: 32
    },
    capture: {
        position: 'absolute',
        width: 320,
        height: 308,
        top: winH / 2 - 154,
        left: winW / 2 - 160,
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    },
    bottomBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        bottom: 0,
        left: 0
    }
});

module.exports = UnlockNewStory;