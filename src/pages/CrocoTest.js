/**
 * Created by benja on 12/05/2017.
 */

import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Image,
    Text,
    Dimensions,
    TouchableHighlight,
    Animated,
    DeviceEventEmitter
} from 'react-native';
import { SensorManager } from 'NativeModules';
import WebViewBridge from 'react-native-webview-bridge';

let crocoSystem = require("../datas/stories/amazonia/assets/croco/index.html");
let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;
let accelero = false;

export default class CrocoTest extends Component{

    constructor(props){
        super(props);
    }

    acceleroManager() {

        if(!accelero){
            accelero = true;
            this.startAccel();
        }else{
            this.stopAccel();
        }
    }

    startAccel(){
        let correctedValue;
        let right = false;
        let left = false;

        let direction = "";
        let previousDirection;

        //margin used to avoid croco to always going left or right and help user let the croco stay in place
        let margin = 2;

        let webview = this.webview;

        SensorManager.startAccelerometer(100); // To start the accelerometer with a minimum delay of 100ms between events.
        DeviceEventEmitter.addListener('Accelerometer', function (data) {

            //when data.x is negative, device lean to the right, else lean to the left
            correctedValue = Math.round(data.x);

            //need to stock previous datas to know if we should call a function in the webview or not, to avoid too many call or calling a function at each time
            //the sensorManager give us datas. That's a way of optimization
            previousDirection = direction;

            if(correctedValue < 0 - margin){
                right = true;
                left = false;
                direction = "right";
            }else if(correctedValue > margin){
                right = false;
                left = true;
                direction = "left";
            }else{
                right = false;
                left = false;
                direction = "";
            }

            //check if the direction has changed to call or not functions through the webview bridge
            if(direction == "left"){
                if(previousDirection != direction){
                    webview.sendToBridge(JSON.stringify("left"));
                }
            }else if(direction == "right"){
                if(previousDirection != direction){
                    webview.sendToBridge(JSON.stringify("right"));
                }
            }else{
                if(previousDirection != direction){
                    webview.sendToBridge(JSON.stringify("none"));
                }
            }

            console.log("left", left);
            console.log("right", right);

        });
    }

    stopAccel(){
        SensorManager.stopAccelerometer();
        console.log("STOP");
    }

    componentDidMount(){

        this.acceleroManager();

        //this.webview.sendToBridge(JSON.stringify(test));
    }

    onBridgeMessage(webViewData) {
        let jsonData = JSON.parse(webViewData);

        if(jsonData.animationDone){
            this.stopAccel();
        }
    }

    render() {
        return (
           <View style={styles.container}>



               <WebViewBridge
                    style={styles.webView}
                    ref={(webviewbridge) => {
                        this.webview = webviewbridge;
                    }}
                    onBridgeMessage={this.onBridgeMessage.bind(this)}
                    source={crocoSystem}
                />

            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        position:'absolute',
        backgroundColor:'#a6a8a6',
        overflow:'hidden',
        height:height,
        width:"100%"
    },
    webView: {
        overflow: 'hidden',
        position:'absolute',
        height:height,
        width:'100%'
    }
});

module.exports = CrocoTest;
