/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    TextInput,
    TouchableHighlight,
    Picker,
    AsyncStorage
} from 'react-native';
import AppBorder from '../components/partials/AppBorder';
import Selection from 'react-native-selection';
import Sounds from '../components/interactions/Sounds';
import Swiper from 'react-native-swiper';
import BlockTopLeftMenu from '../components/partials/BlockTopLeftMenu';


let placeholders = {
    gender: null,
    child_name: "Prénom de l'enfant",
    child_age: "Age de l'enfant",
    parent_names: "Nom, Prénom d'un parent",
    address: "Adresse de l'enfant",
    postcode: "Code postal",
    email: "E-mail des parents",
    frequence: null,
    stories: null
};

let frequences = [
    {
        name: '1 SEMAINE',
        value: 1
    }, {
        name: '2 SEMAINES',
        value: 2
    }, {
        name: '3 SEMAINES',
        value: 3
    }, {
        name: '1 MOIS',
        value: 4
    },
];

let stories = [
    {
        name: '1 CARTE',
        value: 1
    }, {
        name: '3 CARTES',
        value: 2
    }, {
        name: '4 CARTES + 1 GRATUITES',
        value: 3
    }, {
        name: '8 CARTES + 2 GRATUITES',
        value: 4
    },
];

let sliderWidth = 640;
let sliderHeight = 360;

class Settings extends Component {

    constructor(props) {
        super(props);

        this.state = {
            boy: false,
            girl: true,
            child_name: null,
            child_age: null,
            parent_names: null,
            address: null,
            postcode: null,
            email: null,
            frequence: null,
            user: true,
            card: false
        }
    }

    componentWillMount() {
        AsyncStorage.getItem('user', (error, result) => {
            let user = JSON.parse(result);
            this.setState(user);
        });
    }

    onValidation() {
        this.refs.sounds.playSound(13);
        AsyncStorage.setItem('user', JSON.stringify(this.state));
        this.props.navigator.push({
            id: 'Map'
        });
    }

    chooseGender(type) {
        if (type == 'girl') {
            this.setState({
                boy: false,
                girl: true
            });
        } else {
            this.setState({
                boy: true,
                girl: false
            });
        }
    }

    onSelect(choice, type) {
        if (type == 'frequence') {
            this.setState({
                frequence: choice.value
            });
        } else if (type == 'stories') {
            this.setState({
                stories: choice.value
            });
        }

    }

    userPress() {
        !this.state.user ? this.refs.swiper.scrollBy(-1) : null;
        this.refs.sounds.playSound(13);

        this.setState({
            user: true,
            card: false
        });
    }

    cardPress() {
        !this.state.card ? this.refs.swiper.scrollBy(1) : null;
        this.refs.sounds.playSound(13);

        this.setState({
            user: false,
            card: true
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Sounds ref={'sounds'}/>
                <AppBorder/>
                <BlockTopLeftMenu
                    navigator={this.props.navigator}
                    type={'map'}
                    muteAll={(bool) => this.refs.sounds.muteAll(bool)}
                    stopAll={() => this.refs.sounds.stopAll()}
                    playSound={(id) => this.refs.sounds.playSound(id)}/>


                <View style={styles.wrapperCentral}>
                    <Text style={styles.title}>PARAMÈTRES</Text>
                    <Text style={styles.id}>211352524</Text>

                    <View style={styles.wrapperIcon}>
                        <TouchableHighlight onPress={() => this.userPress()} underlayColor="0">
                            {this.state.user ? (
                                <Image source={require('../assets/img/icons/user_active.png')} style={styles.icon}/>
                            ) : (
                                <Image source={require('../assets/img/icons/user_inactive.png')} style={styles.icon}/>
                            )}
                        </TouchableHighlight>

                        <Image source={require('../assets/img/settings/separation_vertical.png')} style={styles.verticalSeparation}/>

                        <TouchableHighlight onPress={() => this.cardPress()} underlayColor="0">
                            {this.state.card ? (
                                <Image source={require('../assets/img/icons/card_active.png')} style={styles.icon}/>
                            ) : (
                                <Image source={require('../assets/img/icons/card_inactive.png')} style={styles.icon}/>
                            )}
                        </TouchableHighlight>
                    </View>


                    <Swiper
                        style={styles.wrapper}
                        width={sliderWidth}
                        height={sliderHeight}
                        showsButtons={false}
                        showsPagination={false}
                        ref={'swiper'}>
                        <View style={styles.wrapperSlider}>
                            <View style={styles.genderBlock}>
                                <TouchableHighlight onPress={() => this.chooseGender('girl')} underlayColor="0">
                                    {this.state.girl ? (
                                        <View>
                                            <Text style={[styles.genderText, styles.pink]}>MA PETITE</Text>
                                            <Image source={require('../assets/img/forms/separation.png')}
                                                   style={styles.separation}/>
                                        </View>
                                    ) : (
                                        <View>
                                            <Text style={styles.genderText}>MA PETITE</Text>
                                        </View>
                                    )}
                                </TouchableHighlight>

                                <TouchableHighlight onPress={() => this.chooseGender('boy')} underlayColor="0">
                                    {this.state.boy ? (
                                        <View>
                                            <Text style={[styles.genderText, styles.pink]}>MON PETIT</Text>
                                            <Image source={require('../assets/img/forms/separation.png')}
                                                   style={styles.separation}/>
                                        </View>
                                    ) : (
                                        <View>
                                            <Text style={styles.genderText}>MON PETIT</Text>
                                        </View>
                                    )}
                                </TouchableHighlight>
                            </View>

                            <Image source={require('../assets/img/forms/input_4.png')} style={styles.textInputImage}>
                                <TextInput onChangeText={(text) => this.setState({child_name: text.toUpperCase()})}
                                           placeholder={placeholders.child_name.toUpperCase()}
                                           placeholderTextColor={'#ff6f6f'}
                                           value={this.state.child_name}
                                           style={styles.textInput}/>
                            </Image>

                            <View style={styles.dualInput}>
                                <Image source={require('../assets/img/forms/input_2.png')} style={styles.textInputImageSmall}>
                                    <TextInput onChangeText={(text) => this.setState({parent_names: text.toUpperCase()})}
                                               placeholder={placeholders.parent_names.toUpperCase()}
                                               placeholderTextColor={'#ff6f6f'}
                                               value={this.state.parent_names}
                                               style={styles.textInputSmall}/>
                                </Image>
                                <Image source={require('../assets/img/forms/input_3.png')} style={styles.textInputImageSmall}>
                                    <TextInput onChangeText={(text) => this.setState({child_age: text.toUpperCase()})}
                                               placeholder={placeholders.child_age.toUpperCase()}
                                               placeholderTextColor={'#ff6f6f'}
                                               value={this.state.child_age}
                                               style={styles.textInputSmall}
                                               keyboardType="numeric"/>
                                </Image>
                            </View>

                            <Image source={require('../assets/img/forms/input_1.png')} style={styles.textInputImageBig}>
                                <TextInput onChangeText={(text) => this.setState({address: text.toUpperCase()})}
                                           placeholder={placeholders.address.toUpperCase()}
                                           placeholderTextColor={'#ff6f6f'}
                                           value={this.state.address}
                                           style={styles.textInputBig}/>
                            </Image>

                            <View style={styles.dualInput}>
                                <Image source={require('../assets/img/forms/input_3.png')} style={styles.textInputImageSmall}>
                                    <TextInput onChangeText={(text) => this.setState({postcode: text.toUpperCase()})}
                                               placeholder={placeholders.postcode.toUpperCase()}
                                               placeholderTextColor={'#ff6f6f'}
                                               value={this.state.postcode}
                                               style={styles.textInputSmall}
                                               keyboardType="numeric"/>
                                </Image>
                                <Image source={require('../assets/img/forms/input_2.png')} style={styles.textInputImageSmall}>
                                    <TextInput onChangeText={(text) => this.setState({email: text.toUpperCase()})}
                                               placeholder={placeholders.email.toUpperCase()}
                                               placeholderTextColor={'#ff6f6f'}
                                               value={this.state.email}
                                               style={styles.textInputSmall}
                                               keyboardType="email-address"/>
                                </Image>
                            </View>
                        </View>

                        <View style={styles.wrapperSlider}>
                            <Text style={styles.cardNumber}>3</Text>
                            <Text style={styles.cardNumberLabel}>CARTES RESTANTES</Text>

                            <View style={styles.wrapperSelect}>
                                <Image source={require('../assets/img/forms/select.png')} style={styles.select}>
                                    <Selection
                                        title="FRÉQUENCE D'ENVOI"
                                        options={frequences}
                                        onSelection={(choice)=>this.onSelect(choice, 'frequence')}
                                        style={{body: null, option: null}}
                                        textColot="#ff6f6f"
                                    />
                                </Image>

                                <Image source={require('../assets/img/forms/select.png')} style={styles.select}>
                                    <Selection
                                        title="PLUS D'HISTOIRES"
                                        options={stories}
                                        onSelection={(choice)=>this.onSelect(choice, 'stories')}
                                        style={{body: null, option: null}}
                                        textColot="#ff6f6f"
                                    />
                                </Image>
                            </View>

                            <Text style={styles.cardNumberLabel}>PRIX : 29,50 €</Text>
                        </View>
                    </Swiper>

                    {this.state.user ? (
                        <TouchableHighlight onPress={() => this.onValidation()} underlayColor="0">
                            <Image source={require('../assets/img/icons/edit.png')} style={styles.validate}/>
                        </TouchableHighlight>
                    ) : (
                        <TouchableHighlight onPress={() => this.refs.sounds.playSound(13)} underlayColor="0">
                            <Image source={require('../assets/img/icons/pay.png')} style={styles.validate}/>
                        </TouchableHighlight>
                    )}

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#28cece',
    },
    wrapperCentral: {
        marginTop: 60,
        width: 630,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    wrapperSlider: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    wrapperIcon: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 36
    },
    wrapperSelect: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 36
    },
    title: {
        fontSize: 60,
        fontFamily: 'GoodDog',
        color: '#ffffff'
    },
    id: {
        color: '#fff',
        marginTop: 0,
        marginBottom: 40
    },
    textInputSmall: {
        color: '#28cece',
        fontFamily: 'Volkswagen-DemiBold',
        fontSize: 15,
        width: 280,
        height: 39,
        textAlign: 'center'
    },
    textInput: {
        color: '#28cece',
        fontFamily: 'Volkswagen-DemiBold',
        fontSize: 15,
        width: 320,
        height: 39,
        textAlign: 'center'
    },
    textInputBig: {
        color: '#28cece',
        fontFamily: 'Volkswagen-DemiBold',
        fontSize: 15,
        width: 630,
        height: 39,
        textAlign: 'center'
    },
    textInputImageSmall: {
        width: 280,
        height: 39,
        marginBottom: 36
    },
    textInputImage: {
        width: 320,
        height: 39,
        marginBottom: 36
    },
    textInputImageBig: {
        width: 640,
        height: 39,
        marginBottom: 36
    },
    dualInput: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 630
    },
    validate: {
        width: 74,
        height: 74
    },
    separation: {
        width: 88,
        height: 7
    },
    genderBlock: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 200,
        marginBottom: 36
    },
    genderText: {
        textAlign: 'center',
        color: '#fff',
        marginBottom: 6
    },
    pink: {
        color: '#ff6f6f'
    },
    select: {
        width: 300,
        height: 39,
        marginBottom: 36,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        width: 100,
        height: 74
    },
    verticalSeparation: {
        width: 7,
        height: 62,
        marginLeft: 36,
        marginRight: 36
    },
    cardNumber: {
        fontSize: 60,
        fontFamily: 'GoodDog',
        color: '#ffffff',
        textAlign: 'center'
    },
    cardNumberLabel: {
        fontSize: 36,
        fontFamily: 'GoodDog',
        color: '#ffffff',
        textAlign: 'center',
        marginTop: 0
    }
});

module.exports = Settings;