/**
 * Created by benja on 09/05/2017.
 */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableHighlight,
    DeviceEventEmitter
} from 'react-native';
import { SensorManager } from 'NativeModules';

let accelero = false;

export default class Test extends Component {

    acceleroManager() {

        if(!accelero){
            accelero = true;
            this.startAccel();
        }else{
            this.stopAccel();
        }
    }

    startAccel(){
        let correctedValue;
        let right = false;
        let left = false;

        SensorManager.startAccelerometer(100); // To start the accelerometer with a minimum delay of 100ms between events.
        DeviceEventEmitter.addListener('Accelerometer', function (data) {

            //when data.x is negative, device lean to the right, else lean to the left
            correctedValue = Math.round(data.x);
            console.log("run");

            if(correctedValue < 0){
                right = true;
                left = false;
            }else if(correctedValue > 0){
                right = false;
                left = true;
            }else{
                right = false;
                left = false;
            }

            console.log("left", left);
            console.log("right", right);
        });
    }

    stopAccel(){
        SensorManager.stopAccelerometer();
        console.log("STOP");
    }

    render() {
        return (
            <View>
                <Text>
                    Test
                </Text>

                <TouchableHighlight onPress={this.acceleroManager.bind(this)} style={styles.button} underlayColor="0">
                    <Text>start/stop accelero logs</Text>
                </TouchableHighlight>

            </View>
        );
    }
}

const styles = StyleSheet.create({});

module.exports = Test;