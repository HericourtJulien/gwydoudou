/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions
} from 'react-native';
import AppBorder from '../components/partials/AppBorder';
import BlockTopLeftMenu from '../components/partials/BlockTopLeftMenu';
import BundleBlock from '../components/partials/BundleBlock';
import Swiper from 'react-native-swiper';
import DATA from '../datas/Datas';
import Sounds from '../components/interactions/Sounds';

let winW = Dimensions.get('window').width;
let winH = Dimensions.get('window').height;

let sliderWidth = 630;
let sliderHeight = 306;

class Bundle extends Component {

    constructor(props) {
        super(props);

        this.state = {
            slider: []
        };
    }

    componentWillMount() {
        let content = [];
        let element;

        DATA.STORIES.map((item, index) => {
            element = (
                <BundleBlock key={index} data={item.BUNDLE}/>
            );
            content.push(element);
        });

        this.setState({
            slider: content
        });
    }

    render() {
        let prevArrow = (<Image source={require('../assets/img/slides/nav_left.png')} style={styles.arrow_left}/>);
        let nextArrow = (<Image source={require('../assets/img/slides/nav_right.png')} style={styles.arrow_right}/>);

        return (
            <View style={styles.container}>
                <Sounds ref={'sounds'}/>
                <AppBorder/>
                <BlockTopLeftMenu
                    navigator={this.props.navigator}
                    muteAll={(bool) => this.refs.sounds.muteAll(bool)}
                    playSound={(id) => this.refs.sounds.playSound(id)}
                    stopAll={() => this.refs.sounds.stopAll()}
                    type={'map'}/>
                <Text style={styles.title}>
                    LES TROUVAILLES{"\n"} DE ZOÉ
                </Text>
                <Image source={require('../assets/img/slides/white_separation.png')} style={styles.separation}/>

                {this.state.slider.length > 0 ? (
                    <Swiper
                        style={styles.wrapper}
                        width={sliderWidth}
                        height={sliderHeight}
                        showsButtons={true}
                        showsPagination={false}
                        prevButton={prevArrow}
                        nextButton={nextArrow}>
                        {this.state.slider}
                    </Swiper>
                ) : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        height: winH,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#28cece',
        padding: 128
    },
    title: {
        fontSize: 60,
        fontFamily: 'GoodDog',
        color: '#ffffff',
        textAlign: 'center'
    },
    separation: {
        margin: 20,
        width: 88,
        height: 7
    },
    arrow_left: {
        position: 'absolute',
        width: 48,
        height: 76,
        top: -30,
        left: -80
    },
    arrow_right: {
        position: 'absolute',
        width: 48,
        height: 76,
        top: -30,
        right: -80
    }
});

module.exports = Bundle;