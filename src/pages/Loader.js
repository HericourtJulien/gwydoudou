/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    AsyncStorage
} from 'react-native';
import AnimateNumber from 'react-native-animate-number';
import * as Animatable from 'react-native-animatable';

class Loader extends Component {



    render() {
        let loadingSize = 256;

        return (
            <View style={styles.container}>
                <Image source={require('../assets/img/others/logo.png')} style={styles.logo}/>
                <View style={styles.blockLoader}>
                    <Image source={require('../assets/img/others/loader.png')} style={styles.loader}/>
                    <Animatable.View ref="test" style={styles.loading}/>
                    <View style={styles.whiteBg}/>
                        <AnimateNumber
                            value={100}
                            formatter={(val) => {
                                this.refs.test.transitionTo({width: loadingSize * val / 100})
                                return parseInt(val) + '%'
                            }}
                            timing={(interval, progress) => {
                                return interval * Math.random() * progress * 10
                            }}
                            onFinish={() => {
                                setTimeout(() => {
                                    this.props.navigator.push({id: 'UnlockApp'});
                                    // AsyncStorage.removeItem('trophy_2');
                                    // AsyncStorage.removeItem('trophy_3');
                                    // AsyncStorage.removeItem('unlock_app');
                                    AsyncStorage.removeItem('mapFirstTime');
                                    // AsyncStorage.removeItem('new_card');
                                    // console.log(AsyncStorage.getAllKeys());
                                }, 500);
                            }}
                            style={styles.percentage}
                        />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#28cece'
    },
    logo: {
        width: 224,
        height: 335,
        marginBottom: 80
    },
    loader: {
        width: 257,
        height: 26,
        marginBottom: 16,
        position: 'relative',
        zIndex: 3
    },
    percentage: {
        fontFamily: 'GoodDog',
        fontSize: 36,
        color: '#ff6f6f',
        textAlign: 'center'
    },
    loading: {
        width: 0,
        height: 26,
        backgroundColor: '#ff6f6f',
        position: 'absolute',
        zIndex: 2
    },
    whiteBg: {
        width: 256,
        height: 26,
        position: 'absolute',
        zIndex: 1,
        backgroundColor: 'white'
    }
});

module.exports = Loader;