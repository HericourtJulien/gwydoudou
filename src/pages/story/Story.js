/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    TouchableHighlight,
    WebView,
    AsyncStorage,
} from 'react-native';
import BlockTopLeftMenu from '../../components/partials/BlockTopLeftMenu';
import BlockMenu from '../../components/partials/BlockTopRightMenu';
import * as Animatable from 'react-native-animatable';
import AppBorder from '../../components/partials/AppBorder';
import Sounds from '../../components/interactions/Sounds';
import Page from '../../components/partials/Page';
import Animator from '../../components/interactions/Animator';
import DATA from '../../datas/Datas';

PageAnimatable = Animatable.createAnimatableComponent(Page);

var winW = Dimensions.get('window').width;
var winH = Dimensions.get('window').height;

class Story extends Component {

    constructor(props) {
        super(props);

        this.state = {
            pages: (props.data === false ? false : props.data.DATA.default.PAGES),
            prevPage: {},
            currentPage: {},
            nextPage: {},
            bundleLock: true,
            character: null,
            childName: 'Zoé'
        };
    }

    componentWillMount() {
        let initialId = 0;
        this.createPage(initialId, 'init');
        AsyncStorage.
        getItem('user', (error, result) => {
            let user = JSON.parse(result);
            if (user !== null) {
                this.setState({
                    childName: user.child_name
                });
            }
        });
    }

    checkTextCharacter(text) {
        let checkCharacter = text.indexOf('{char}');
        let isCharacter = checkCharacter != -1 ? true : false;

        if (isCharacter) {
            text = text.substr(0, checkCharacter);
        }

        return text;
    }

    checkTextName(text) {
        let childName = text.indexOf('{name}');
        let isName = childName != -1 ? true : false;

        if (isName) {
            text = text.replace('{name}', this.state.childName);
        }

        return text;
    }

    checkTextBold(text) {
        let checkBold = text.split('{b}');
        let isBold = checkBold.length > 1 ? true : false;

        if (isBold) {
            text = (
                <Text style={textStyles.paragraph}>
                    {checkBold[0]}
                    <Text style={textStyles.bold}>{checkBold[1]}</Text>
                    {checkBold[2]}
                </Text>
            );
        } else {
            text = (
                <Text style={textStyles.paragraph}>
                    {text}
                </Text>
            )
        }

        return text;
    }

    displayCurrentText(currentPage) {
        let currentText = currentPage.TEXT[currentPage.CURRENT_TEXT];
        let finalText;

        currentText = this.checkTextCharacter(currentText);
        currentText = this.checkTextName(currentText);
        finalText = this.checkTextBold(currentText);

        return finalText;
    }

    checkIconCharacter(currentText) {
        let checkCharacter = currentText.split('{char}');
        let isCharacter = checkCharacter.length > 1 ? true : false;
        let newCharacter = (
            <Image source={require('../../assets/img/characters/icon_toudou.png')} style={styles.character}/>);

        if (isCharacter) {
            switch (checkCharacter[1]) {
                case 'none':
                    newCharacter = null;
                    break;
                case 'croco':
                    newCharacter = (<Image source={require('../../assets/img/characters/icon_croco.png')}
                                           style={styles.character}/>);
                    break;
                case 'toucan':
                    newCharacter = (<Image source={require('../../assets/img/characters/icon_toucan.png')}
                                           style={styles.character}/>);
                    break;
                case 'paresseux':
                    newCharacter = (<Image source={require('../../assets/img/characters/icon_paresseux.png')}
                                           style={styles.character}/>);
                    break;
                default:
                    break;
            }
        }

        return newCharacter;
    }

    createPage(id, type, home) {
        let prevPage,
            currentPage,
            nextPage,
            currentText;
        let pages = this.state.pages;

        console.log(pages);


        if (pages === false) {
            pages = DATA.STORIES[0].DATA.default.PAGES ;
        }

        console.log(pages);

        let blockSize = winH / 0.3;

        home ? this.props.navigator.push({ id: 'Map'}) : null;

        /** Check type **********/
        if (type === 'init') {
            currentPage = pages[id];
        } else if (type === 'prev') {
            currentPage = this.state.currentPage;

            this.refs.currentText.animate(prevStart, 600).then(() => {
                if (currentPage.CURRENT_TEXT > 0) {
                    currentPage.CURRENT_TEXT = currentPage.CURRENT_TEXT - 1;
                    this.setState({
                        currentPage: currentPage
                    });

                    this.refs.currentText.animate(prevEnd, 300);
                } else {
                    currentPage = pages[id - 1];

                    this.refs.page.animate(fadeOut, 500).then(() => {
                        this.setState({
                            currentPage: currentPage
                        });

                        setTimeout(() => {
                            this.refs.currentText.animate(prevEnd, 300);
                            this.refs.page.animate(fadeIn, 1000);
                        }, 3000);
                    });
                }
            });

        } else if (type === 'next') {
            currentPage = this.state.currentPage;

            this.refs.currentText.animate(nextStart, 600).then(() => {
                if (currentPage.CURRENT_TEXT < currentPage.TEXT.length - 1) {
                    currentPage.CURRENT_TEXT = currentPage.CURRENT_TEXT + 1;
                    this.setState({
                        currentPage: currentPage
                    });

                    this.refs.currentText.animate(nextEnd, 300);
                } else {
                    currentPage = pages[id + 1];

                    this.refs.page.animate(fadeOut, 500).then(() => {
                        this.setState({
                            currentPage: currentPage
                        });

                        setTimeout(() => {
                            this.refs.currentText.animate(nextEnd, 300);
                            this.refs.page.animate(fadeIn, 1000);
                        }, 3000);
                    });
                }
            });

        }

        /** Check if the future current page can move **********/
        if (currentPage.CAN_MOVE.PREV) {
            prevPage = pages[currentPage.ID - 1];
        } else {
            prevPage = null;
        }

        /** Check if the future current page can move **********/
        if (currentPage.CAN_MOVE.NEXT) {
            nextPage = pages[currentPage.ID + 1];
        } else {
            nextPage = null;
        }

        /** Update State **********/
        this.setState({
            prevPage: prevPage,
            currentPage: currentPage,
            nextPage: nextPage
        });
    }

    render() {
        let prevPage = this.state.prevPage;
        let currentPage = this.state.currentPage;
        let nextPage = this.state.nextPage;
        let currentText = currentPage.TEXT[currentPage.CURRENT_TEXT];
        let character = this.checkIconCharacter(currentText);

        return (
            <View style={styles.fullBlock}>
                <View style={styles.pictureBlock}>
                    <Sounds ref={'sounds'}/>

                    <BlockTopLeftMenu navigator={this.props.navigator} type={'map'}  muteAll={(bool) => this.refs.sounds.muteAll(bool)} playSound={(id, repeat) => this.refs.sounds.playSound(id, repeat)} stopSound={(id) => this.refs.sounds.stopSound(id)}  adjustVolume={(id, value) => this.refs.sounds.adjustVolume(id, value)} stopAll={() => this.refs.sounds.stopAll()}/>
                    <BlockMenu navigator={this.props.navigator} settings={false}  muteAll={(bool) => this.refs.sounds.muteAll(bool)} playSound={(id, repeat) => this.refs.sounds.playSound(id, repeat)} stopSound={(id) => this.refs.sounds.stopSound(id)}  adjustVolume={(id, value) => this.refs.sounds.adjustVolume(id, value)} stopAll={() => this.refs.sounds.stopAll()}/>


                    <AppBorder/>

                    <Image source={this.props.data.TRANSITION_GIF} style={styles.transition}/>
                    <PageAnimatable
                        animation="fadeIn"
                        ref={'page'}
                        data={this.state.currentPage}
                        onNextPage={(home) => this.createPage(currentPage.ID, 'next', home)}
                        muteAll={(bool) => this.refs.sounds.muteAll(bool)}
                        playSound={(id, repeat) => this.refs.sounds.playSound(id, repeat)}
                        stopSound={(id) => this.refs.sounds.stopSound(id)}
                        adjustVolume={(id, value) => this.refs.sounds.adjustVolume(id, value)}
                        stopAll={() => this.refs.sounds.stopAll()}
                    />

                </View>
                <Image source={require('../../assets/img/story/text_bg.png')} style={styles.textBarBlock}>
                    {character}

                    <View style={styles.mask}>
                        <Animatable.Text
                            animation="slideInUp"
                            delay={1000}
                            ref="currentText"
                            style={styles.textBarContent}>
                            {currentPage !== null ? this.displayCurrentText(currentPage) : null}
                        </Animatable.Text>
                    </View>


                    {currentPage.CAN_MOVE.PREV ? (
                        <TouchableHighlight style={styles.arrow_up} underlayColor="0"
                                            onPress={() => this.createPage(currentPage.ID, 'prev')}>
                            <Image source={require('../../assets/img/slides/nav_right.png')}
                                   style={styles.arrow_img}/>
                        </TouchableHighlight>
                    ) : null}

                    {currentPage.CAN_MOVE.NEXT ? (
                        <TouchableHighlight style={styles.arrow_down} underlayColor="0"
                                            onPress={() => this.createPage(currentPage.ID, 'next')}>
                            <Image source={require('../../assets/img/slides/nav_left.png')}
                                   style={styles.arrow_img}/>
                        </TouchableHighlight>
                    ) : null}

                </Image>
            </View>
        );
    }
}

const fadeOut = {
    0: {
        opacity: 1,
    },
    1: {
        opacity: 0,
    },
};

const fadeIn = {
    0: {
        opacity: 0,
    },
    0.5: {
        opacity: 0,
    },
    1: {
        opacity: 1,
    },
};

const nextStart = {
    0: {
        marginTop: 0
    },
    1: {
        marginTop: -winH
    }
};

const nextEnd = {
    0: {
        marginTop: winH
    },
    1: {
        marginTop: 0
    }
};

const prevStart = {
    0: {
        marginTop: 0
    },
    1: {
        marginTop: winH
    }
};

const prevEnd = {
    0: {
        marginTop: -winH
    },
    1: {
        marginTop: 0
    }
};


const styles = StyleSheet.create({
    fullBlock: {
        flex: 1,
        width: winW,
        height: winH,
        backgroundColor: '#28cece',
        justifyContent: 'space-between'
    },
    pictureBlock: {
        height: winH - 106,
    },
    textBarBlock: {
        height: 128,
        width: winW,
        justifyContent: 'center',
        paddingLeft: 120,
        paddingRight: 120,
        position: 'absolute',
        bottom: 0
    },
    textBarContent: {
        backgroundColor: 'white',
        fontSize: 24,
        textAlign: 'center',
        maxHeight: 106,
        fontFamily: 'Volkswagen',
        transform: [
            {
                translateY: 5
            }
        ]
    },
    mask: {
        height: 106,
        width: '100%',
        overflow: 'hidden',
        display: 'flex',
        justifyContent: 'center'
    },
    bundle: {
        width: 74,
        height: 74,
        position: 'absolute',
        bottom: 20,
        left: winW / 2 - 37
    },
    bundleImg: {
        width: 74,
        height: 74,
    },
    arrow_up: {
        position: 'absolute',
        right: 20,
        top: 0,
    },
    arrow_down: {
        position: 'absolute',
        right: 20,
        bottom: 0,
    },
    arrow_img: {
        height: 62,
        width: 40,
        transform: [
            {scale: 0.5},
            {rotate: '-90deg'}
        ]
    },
    character: {
        width: 56,
        height: 56,
        position: 'absolute',
        left: 48,
        bottom: 32
    },
    transition: {
        position: 'absolute',
        width: '100%',
        height: '100%'
    }
});

const textStyles = StyleSheet.create({
    paragraph: {
        fontFamily: 'Volkswagen',
        fontSize: 22,
        color: '#097280',
        textAlign: 'center',
    },
    bold: {
        fontFamily: 'Volkswagen-Bold',
        fontSize: 22,
        color: '#097280',
        textAlign: 'center'
    }
});

module.exports = Story;