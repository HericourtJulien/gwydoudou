/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    TouchableHighlight,
    AsyncStorage
} from 'react-native';
import AppBorder from '../components/partials/AppBorder';
import Popup from '../components/partials/Popup';
import IntAnim from '../datas/InterfaceAnimations';
import Camera from '../components/interactions/Camera';
import Sounds from '../components/interactions/Sounds';
import * as Animatable from 'react-native-animatable';

let winW = Dimensions.get('window').width;
let winH = Dimensions.get('window').height;
let canUse = true;

class UnlockApp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            popup: [],
            capture: false,
            captureAppear: false,
            willUnmount: false
        };
    }

    componentDidMount() {
        AsyncStorage.removeItem('unlock_app');

        AsyncStorage.getItem('unlock_app', (error, result) => {
            if (result == null) {
                setTimeout(() => {
                    this.createPopup();
                }, 300);
                AsyncStorage.setItem('unlock_app', JSON.stringify(true));
            }
        });
    }

    shitCode(){
        this.props.navigator.push({
            id: 'Map'
        });
    }

    unmount() {
        console.log('unmount');


        setTimeout(() => {
            this.unlockApp();
        }, 1000);

        this.setState({
            willUnmount: true
        });
    }

    unlockApp(){
        console.log('unlock');
        if(!canUse){
            return;
        }

        AsyncStorage.getItem('user', (error, result) => {
            if (result == null) {
                this.props.navigator.push({
                   id: 'Inscription'
                });
            } else {
                this.props.navigator.push({
                    id: 'Map'
                });
            }
        });

        canUse = false;
    }

    createPopup() {
        let popup = [(
            <Popup playSound={(id) => this.refs.sounds.playSound(id)}>
                <Image
                    source={require('../assets/img/gifs/tuto_scan_toudou.gif')}
                    style={styles.gif}/>
                <Text style={styles.subtitle}>
                    Scanne Toudou pour débloquer{"\n"} l'application
                </Text>
            </Popup>
        )];

        this.setState({
            popup: popup
        });
    }

    render() {
        /*setTimeout(() => {
            this.props.navigator.push({
                id: 'Map'
            });
        }, 4000);*/
        let capture;

        if (this.state.capture) {
            capture = (<Image source={require('../assets/img/others/capture_actif.png')} style={styles.capture}/>);
        } else if (this.state.captureAppear) {
            capture = null;
        } else {
            capture = (<Image source={require('../assets/img/others/capture_inactif.png')} style={styles.capture}/>);
            setTimeout(() => {
                this.setState({
                    capture: false,
                    captureAppear: true
                });
            }, 1000);
        }

        return (
            <View style={styles.blockUnlock}>
                {this.state.willUnmount === false ? (
                    <Camera
                        stringToUnlock={"Toudou Voyage"}
                        action={() => this.unmount()}
                        onDetect={() => this.setState({capture: true})}
                    >
                        {/*<AppBorder/>*/}
                        {capture}

                        {/*Border*/}
                        <Image source={require('../assets/img/others/app_left_border.png')} style={styles.leftBorder}/>
                        <Image source={require('../assets/img/others/app_right_border.png')} style={styles.rightBorder}/>
                        <Image source={require('../assets/img/others/app_top_border.png')} style={styles.topBorder}/>
                        <Image source={require('../assets/img/others/app_bottom_border.png')} style={styles.bottomBorder}/>


                        <Sounds ref={'sounds'}/>
                        {this.state.popup}
                    </Camera>
                ) : null}

                {/*Cheat code*/}
                <TouchableHighlight onPress={() => this.shitCode()} style={styles.cheatCode} underlayColor="0">
                    <View/>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    blockUnlock: {
        flex: 1,
        height: 50,
        width: winW,
        marginTop: 'auto',
        justifyContent: 'center',
        alignItems: 'center'
    },
    blockUnlockButton: {
        width: 160,
        height: 50,
        position:'absolute',
        bottom:100,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f00fff'
    },
    gif: {
        width: '60%',
        height: '60%'
    },
    subtitle: {
        fontSize: 38,
        color: '#28cece',
        fontFamily: 'GoodDog',
        textAlign: 'center',
        marginTop: 32
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    },
    bottomBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        bottom: 0,
        left: 0
    },
    capture: {
        position: 'absolute',
        width: 320,
        height: 308,
        top: winH / 2 - 154,
        left: winW / 2 - 160,
    },
    cheatCode: {
        position: 'absolute',
        width: 150,
        height: 80,
        right: 0,
        bottom: 0,
    }
});

module.exports = UnlockApp;