import {Dimensions} from 'react-native';

let winW = Dimensions.get('window').width;
let winH = Dimensions.get('window').height;

export default  {
    ANIMATIONS: {
        PREVIEW: {
            START: {
                0: {
                    top: 0,
                    transform: [{
                        scale: 0
                    }],
                    zIndex: 4
                },
                0.5: {
                    top: 0,
                    transform: [{
                        scale: 0.8
                    }],
                    zIndex: 4
                },
                0.7: {
                    top: 0,
                    transform: [{
                        scale: 1.1
                    }]
                },
                1: {
                    top: 0,
                    transform: [{
                        scale: 1
                    }]
                }
            },
            END: {
                0: {
                    top: 0,
                    transform: [{
                        scale: 0.8
                    }]
                },
                0.5: {
                    top: 0,
                    transform: [{
                        scale: 0.6
                    }]
                },
                1: {
                    top: 0,
                    transform: [{
                        scale: 0
                    }]
                }
            }
        },
        MASK: {
            START: {
                0: {
                    top: 0,
                    opacity: 0
                },
                1: {
                    top: 0,
                    opacity: 0.5
                }
            },
            END: {
                0: {
                    top: 0,
                    opacity: 0.5
                },
                0.5: {
                    top: 0,
                    opacity: 0
                },
                1: {
                    top: -winH * 2,
                }
            }
        },
        CLOUD: {
            0: {
                transform: [{translateY: 0}]
            },
            0.25: {
                transform: [{translateY: 10}]
            },
            0.5: {
                transform: [{translateY: 0}]
            },
            0.75: {
                transform: [{translateY: -10}]
            },
            1: {
                transform: [{translateY: 0}]
            }
        },
        TEETH: {
            0: {
                transform: [{rotate: '0deg'}]
            },
            0.25: {
                transform: [{rotate: '10deg'}]
            },
            0.5: {
                transform: [{rotate: '0deg'}]
            },
            0.75: {
                transform: [{rotate: '-10deg'}]
            },
            1: {
                transform: [{rotate: '0deg'}]
            }
        },
        CTA: {
            0: {
                opacity: 0,
                transform: [
                    {
                        scale: 0.5
                    }, {
                        translateY: 0
                    }
                ]
            },
            0.02: {
                opacity: 1,
                transform: [
                    {
                        scale: 1.2,
                    }, {
                        translateY: 0
                    }
                ]
            },
            0.04: {
                opacity: 1,
                transform: [
                    {
                        scale: 0.9,
                    }, {
                        translateY: 0
                    }
                ]
            },
            0.05: {
                opacity: 1,
                transform: [
                    {
                        scale: 1,
                    }, {
                        translateY: 0
                    }
                ]
            },
            0.2: {
                opacity: 1,
                transform: [
                    {
                        scale: 1,
                    }, {
                        translateY: 0
                    }
                ]
            },
            0.3: {
                opacity: 1,
                transform: [
                    {
                        scale: 1,
                    }, {
                        translateY: -5
                    }
                ]
            },
            0.5: {
                opacity: 1,
                transform: [
                    {
                        scale: 1,
                    }, {
                        translateY: 5
                    }
                ]
            },
            0.7: {
                opacity: 1,
                transform: [
                    {
                        scale: 1,
                    }, {
                        translateY: -5
                    }
                ]
            },
            0.9: {
                opacity: 1,
                transform: [
                    {
                        scale: 1,
                    }, {
                        translateY: 0
                    }
                ]
            },
            0.92: {
                opacity: 1,
                transform: [
                    {
                        scale: 1.2,
                    }, {
                        translateY: 0
                    }
                ]
            },
            0.95: {
                opacity: 0,
                transform: [
                    {
                        scale: 0.5,
                    }, {
                        translateY: 0
                    }
                ]
            },
            1: {
                opacity: 0,
                transform: [
                    {
                        scale: 0.5,
                    }, {
                        translateY: 0
                    }
                ]
            }
        }
    }
}