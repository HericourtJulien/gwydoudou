export default {
    STORIES: [
        {
            ID: 1,
            NB_PAGE: 2,
            DATA: require('./stories/amazonia/Amazonia'),
            PIN: {
                ID: 1,
                POSITION: {
                    X: '30%',
                    Y: '55%'
                },
                IMG: require('./stories/amazonia/assets/img/pin.png'),
                DIMENSIONS: {
                    WIDTH: 55,
                    HEIGHT: 76
                }
            },
            TRANSITION_GIF: require('./stories/amazonia/assets/img/loading_screen.gif'),
            CARD: {
                UNLOCK: "Amazonia",
                ASSETS: [
                    {IMG: require('./stories/amazonia/assets/card/fond_5_opt.gif')},
                    {IMG: require('./stories/amazonia/assets/card/toucan.gif')},
                    {IMG: require('./stories/amazonia/assets/card/feuillage.gif')}
                ],
                STYLES: {
                    CHARACTER: {width: 250, height: 200, duration: 5000, repeat: true},
                    ANIMATION: {
                        0: {
                            bottom: 0,
                            right: 0,
                        },
                        0.2: {
                            bottom: 0,
                            right: 100
                        },
                        0.6: {
                            bottom: 0,
                            right: 100
                        },
                        1: {
                            bottom: 1000,
                            right: 1000
                        }
                    }
                },
                ACTION: {}
            },
            RESUME: {
                ID: 1,
                TEXT: "Accompagne Toudou dans son exploration de l’Amazonie pour découvrir les mystères de la jungle et admirer la beauté de la nature sauvage. Tout au long de son voyage, tu pourras venir en aide à ses nouveaux amis et ainsi faire de belles rencontres. En route pour l’aventure ! ",
                SUBTITLE: 'Toudou en',
                TITLE: 'Amazonie',
                IMG: require('./stories/amazonia/assets/img/img_preview.png')
            },
            BUNDLE: {
                TITLE: 'Amazonie',
                ELEMENTS: [
                    {
                        ID_NAME: 'trophy_1',
                        BG: require('./stories/amazonia/assets/bundle/element_0.png'),
                        IMG: require('./stories/amazonia/assets/bundle/json/feather.json'),
                        TEXT: 'La plume\nde Toucan'
                    },
                    {
                        ID_NAME: 'trophy_2',
                        BG: require('./stories/amazonia/assets/bundle/element_1.png'),
                        IMG: require('./stories/amazonia/assets/bundle/json/branch.json'),
                        TEXT: 'La branchette\nde Paresseux'
                    },
                    {
                        ID_NAME: 'trophy_3',
                        BG: require('./stories/amazonia/assets/bundle/element_2.png'),
                        IMG: require('./stories/amazonia/assets/bundle/json/teeth.json'),
                        TEXT: 'La dent\nde Croco'
                    }
                ]
            }
        },
        {
            ID: 2,
            NB_PAGE: 2,
            DATA: require('./stories/china/China'),
            PIN: {
                ID: 2,
                POSITION: {
                    X: '27%',
                    Y: '32%'
                },
                IMG: require('./stories/china/assets/img/pin.png'),
                DIMENSIONS: {
                    WIDTH: 60,
                    HEIGHT: 57
                }
            },
            CARD: {
                UNLOCK: "Canada",
                ASSETS: {}
            },
            RESUME: {
                ID: 2,
                TEXT: "Je suis le texte de la Chine !",
                SUBTITLE: 'Toudou en',
                TITLE: 'Chine',
                IMG: require('./stories/amazonia/assets/img/img_preview.png')
            },
            BUNDLE: {
                TITLE: 'Chine',
                ELEMENTS: [
                    {
                        IMG: require('./stories/china/assets/bundle/element_0.png'),
                        TEXT: 'blabla'
                    },
                    {
                        IMG: require('./stories/china/assets/bundle/element_1.png'),
                        TEXT: 'blabla'
                    },
                    {
                        IMG: require('./stories/china/assets/bundle/element_2.png'),
                        TEXT: 'blabla'
                    }
                ]
            }
        }
    ]
}