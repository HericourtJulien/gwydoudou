export default {
    PAGES: [
        {
            TEXT: 'Page 1',
            INTERACTIONS: {},
            CAN_MOVE: {
                PREV: false,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            }
        },
        {
            TEXT: 'Page 2',
            INTERACTIONS: {},
            CAN_MOVE: {
                PREV: true,
                NEXT: false
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            }
        }
    ]
}