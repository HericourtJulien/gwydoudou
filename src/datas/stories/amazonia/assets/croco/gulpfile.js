var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');

gulp.task('sass', function(){
    return gulp.src('assets/scss/*.scss')
        .pipe(sass())    // ici on utilise gulp-sass
        .pipe(gulp.dest('assets/css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('watch', ['browser-sync', 'sass'], function () {
    gulp.watch('assets/scss/*.scss', ['sass']);
    gulp.watch('*.html', browserSync.reload);
    gulp.watch('js/*.js', browserSync.reload);
});

gulp.task('browser-sync', function () {
    browserSync({
        server:{
            baseDir: ''
        }
    })
});