/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight
} from 'react-native';
import Animator from '../../../../../components/interactions/Animator';
import AppBorder from '../../../../../components/partials/AppBorder';
import IntAnim from '../../../../../datas/InterfaceAnimations';
import * as Animatable from 'react-native-animatable';

MoveAnimator = Animatable.createAnimatableComponent(Animator);

let teethMargin = 0;

export default class Page_9 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data,
            teeth: -10
        }
    }

    componentDidMount(){
        this.props.playSound(1, true);
        this.props.adjustVolume(1, 0.3);
        this.props.playSound(10, true);
    }

    componentWillUnmount(){
        this.props.stopSound(1);
        this.props.stopSound(10);
    }

    onTeethTap() {
        teethMargin -= 5;
        this.forceUpdate();
        teethMargin <= -25 ? this.props.onNextPage() : null;

        if(teethMargin <= -25){
            this.props.onNextPage();
        }
    }

    render() {
        return (
            <View>
                <Image source={require('./assets/bg.png')} style={styles.bg}>

                    <Animatable.Image
                        source={require('./assets/nuage_1.png')}
                        animation={IntAnim.ANIMATIONS.CLOUD}
                        iterationCount={'infinite'}
                        duration={5000}
                        style={styles.nuage_1}
                    />
                    <Animatable.Image
                        source={require('./assets/nuage_2.png')}
                        animation={IntAnim.ANIMATIONS.CLOUD}
                        iterationCount={'infinite'}
                        duration={4000}
                        style={styles.nuage_2}
                    />

                    <Image
                        source={require('./assets/feuille-gauche.gif')}
                        style={styles.feuille_1_move}
                    />
                    <Image
                        source={require('./assets/feuille_1.png')}
                        style={styles.feuille_1}
                    />

                    <Image
                        source={require('./assets/feuille_2.png')}
                        style={styles.feuille_2}
                    />
                    <Image
                        source={require('./assets/feuille-droite.gif')}
                        style={styles.feuille_2_move}
                    />

                    <Image
                        source={require('./assets/caillou1.gif')}
                        style={styles.caillou_1}
                    />
                    <Image
                        source={require('./assets/caillou2.gif')}
                        style={styles.caillou_2}
                    />

                    <Image
                        source={require('./assets/gueule-croco.gif')}
                        style={styles.croco_move}
                    />

                    <Image
                        source={require('./assets/dent.gif')}
                        style={[styles.croco_dent_img, {transform: [{translateY: teethMargin}]}]}
                        ref={'teeth'}
                    />
                    <Image
                        source={require('./assets/croco.png')}
                        style={styles.croco}
                    />
                    <TouchableHighlight
                        underlayColor="0"
                        onPress={() => this.onTeethTap()}
                        style={styles.croco_dent}>
                        <View style={styles.croco_dent_img}></View>
                    </TouchableHighlight>
                </Image>

                {/*Border*/}
                <Image source={require('../../../../../assets/img/others/app_left_border.png')} style={styles.leftBorder}/>
                <Image source={require('../../../../../assets/img/others/app_right_border.png')} style={styles.rightBorder}/>
                <Image source={require('../../../../../assets/img/others/app_top_border.png')} style={styles.topBorder}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bg: {
        width: '100%',
        height: '100%'
    },
    nuage_1: {
        position: 'absolute',
        top: 60,
        right: 340,
        height: 26,
        width: 82
    },
    nuage_2: {
        position: 'absolute',
        top: 90,
        right: 160,
        height: 37,
        width: 164
    },
    caillou_1: {
        position: 'absolute',
        bottom: 170,
        left: -130,
        height: 170,
        width: 500
    },
    caillou_2: {
        position: 'absolute',
        bottom: 100,
        right: 0,
        height: 170,
        width: 300
    },
    feuille_1: {
        position: 'absolute',
        top: 140,
        left: 80,
        height: 299,
        width: 187
    },
    feuille_1_move: {
        position: 'absolute',
        top: 50,
        left: -50,
        height: 400,
        width: 364
    },
    feuille_2: {
        position: 'absolute',
        top: 250,
        left: 850,
        height: 243,
        width: 162
    },
    feuille_2_move: {
        position: 'absolute',
        top: 280,
        right: 10,
        height: 240,
        width: 240
    },
    croco: {
        position: 'absolute',
        top: 500,
        left: 280,
        height: 160,
        width: 440
    },
    croco_move: {
        position: 'absolute',
        top: 80,
        left: 186,
        height: 600,
        width: 600
    },
    croco_dent: {
        position: 'absolute',
        top: 400,
        left: 650,
        height: 300,
        width: 100,
    },
    croco_dent_img: {
        position: 'absolute',
        bottom: 70,
        left: 676,
        height: 100,
        width: 50,
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    }
});

