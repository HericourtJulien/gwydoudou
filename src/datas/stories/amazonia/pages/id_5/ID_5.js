/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ScrollView,
    Dimensions
} from 'react-native';
import Animator from '../../../../../components/interactions/Animator';
import AppBorder from '../../../../../components/partials/AppBorder';
import * as Animatable from 'react-native-animatable';
import IntAnim from '../../../../../datas/InterfaceAnimations';

var winW = Dimensions.get('window').width;
var winH = Dimensions.get('window').height;

export default class Page_5 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data,
            active: true
        }
    }

    componentDidMount(){
        this.props.playSound(14, true);
        this.props.adjustVolume(14, 0.3);
        this.props.playSound(3, true);
    }

    changePage() {
        this.setState({
            active: false
        });
        this.props.onNextPage();
    }

    componentWillUnmount(){
        this.props.stopSound(14);
        this.props.stopSound(3);
    }

    render() {
        return (
            <View>
                <Image source={require('./assets/bg.png')} style={styles.bg}>

                    <Animatable.Image
                        source={require('./assets/nuage_1.png')}
                        animation={IntAnim.ANIMATIONS.CLOUD}
                        iterationCount={'infinite'}
                        duration={5000}
                        style={styles.nuage_1}
                    />

                    <Animatable.Image
                        source={require('./assets/nuage_2.png')}
                        animation={IntAnim.ANIMATIONS.CLOUD}
                        iterationCount={'infinite'}
                        duration={5000}
                        style={styles.nuage_2}
                    />

                    <Image source={require('./assets/foret_bg.png')} style={styles.foret_bg}/>
                    <Image source={require('./assets/foret_right.png')} style={styles.foret_right}/>
                    <Image source={require('./assets/foret_left.png')} style={styles.foret_left}/>

                    <Animator
                        source={require('./assets/json/feuille_2.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.feuille_2_move}
                    />

                    <Image source={require('./assets/ground.png')} style={styles.ground}/>

                    <Animator
                        source={require('./assets/json/feuille_1.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.feuille_1_move}
                    />

                    <Animator
                        source={require('./assets/json/feuille_3.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.feuille_3_move}
                    />



                    <Image source={require('./assets/feuille_1.png')} style={styles.feuille_1}/>
                    <Image source={require('./assets/feuille_2.png')} style={styles.feuille_2}/>
                    <Image source={require('./assets/feuille_3.png')} style={styles.feuille_3}/>
                    <Image source={require('./assets/statue.png')} style={styles.statue}/>

                    <View style={styles.viewBox}>
                        <ScrollView alwaysBounceVertical={false} contentOffset={{x: 0, y:900}}>
                            {this.state.active ? (
                                <TouchableHighlight
                                    onPress={() => this.changePage()}
                                    underlayColor="0"
                                    style={styles.touchableParesseux}>
                                    <View></View>
                                </TouchableHighlight>
                            ) : null}

                            <Animator
                                source={require('./assets/json/paresseux.json')}
                                duration={2500}
                                repeat={false}
                                style={styles.paresseux}
                            />
                        </ScrollView>
                    </View>

                    <Animator
                        source={require('./assets/json/liane_1.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.liane_1_move}
                    />

                    <Animator
                        source={require('./assets/json/liane_3.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.liane_3_move}
                    />

                    <Animator
                        source={require('./assets/json/caillou_1.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.caillou_1_move}
                    />

                    <Animator
                        source={require('./assets/json/caillou_2.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.caillou_2_move}
                    />


                    <Image source={require('./assets/caillou_1.png')} style={styles.caillou_1}/>
                    <Image source={require('./assets/caillou_2.png')} style={styles.caillou_2}/>


                    <Image source={require('./assets/liane_1.png')} style={styles.liane_1}/>
                    <Image source={require('./assets/liane_2.png')} style={styles.liane_2}/>
                    <Image source={require('./assets/liane_3.png')} style={styles.liane_3}/>

                    <Animator
                        source={require('./assets/json/toudou.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.toudou}
                    />

                    <Image source={require('../../../../../assets/img/others/app_top_border.png')} style={styles.topBorder}/>

                </Image>

                {/*Border*/}
                <Image source={require('../../../../../assets/img/others/app_left_border.png')} style={styles.leftBorder}/>
                <Image source={require('../../../../../assets/img/others/app_right_border.png')} style={styles.rightBorder}/>
                <Image source={require('../../../../../assets/img/others/app_top_border.png')} style={styles.topBorder}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bg: {
        width: 1024,
        height: 649
    },
    caillou_1: {
        width: 98,
        height: 65,
        position: 'absolute',
        bottom: 20,
        left: 110,
    },
    caillou_1_move: {
        width: 230,
        height: 200,
        position: 'absolute',
        top: 227,
        left: 0,
    },
    caillou_2: {
        width: 230,
        height: 275,
        position: 'absolute',
        bottom: -20,
        left: 460,
        zIndex: 5
    },
    caillou_2_move: {
        width: 300,
        height: 360,
        position: 'absolute',
        top: 180,
        left: 200,
    },
    feuille_1: {
        width: 100,
        height: 180,
        position: 'absolute',
        top: 320,
        left: 250,
    },
    feuille_1_move: {
        width: 160,
        height: 240,
        position: 'absolute',
        top: 140,
        left: 125,
    },
    feuille_2: {
        width: 112,
        height: 82,
        position: 'absolute',
        top: 440,
        right: 170,
    },
    feuille_2_move: {
        width: 150,
        height: 280,
        position: 'absolute',
        top: 140,
        left: 360,
    },
    feuille_3: {
        width: 176,
        height: 264,
        position: 'absolute',
        bottom: -20,
        right: -40,
    },
    feuille_3_move: {
        width: 220,
        height: 280,
        position: 'absolute',
        top: 200,
        left: 420,
    },
    foret_bg: {
        width: 1024,
        height: 524,
        position: 'absolute',
        bottom: 50,
        left: 0,
    },
    foret_left: {
        width: 1000,
        height: 491,
        position: 'absolute',
        top: 0,
        left: 0,
    },
    foret_right: {
        width: 1024,
        height: 648,
        position: 'absolute',
        top: 0,
        right: 0,
    },
    ground: {
        width: '100%',
        height: 174,
        position: 'absolute',
        bottom: 0,
        left: 0,
    },
    liane_1: {
        width: 182,
        height: 485,
        position: 'absolute',
        top: -20,
        left: -30,
    },
    liane_1_move: {
        width: 182,
        height: 640,
        position: 'absolute',
        top: 0,
        left: 0,
    },
    liane_2: {
        width: 59,
        height: 238,
        position: 'absolute',
        top: -10,
        left: 390,
    },
    liane_3: {
        width: 219,
        height: 241,
        position: 'absolute',
        top: 0,
        right: -10,
    },
    liane_3_move: {
        width: 219,
        height: 680,
        position: 'absolute',
        top: 0,
        left: 410,
    },
    nuage_1: {
        width: 190,
        height: 42,
        position: 'absolute',
        top: 180,
        left: 330,
    },
    nuage_2: {
        width: 101,
        height: 32,
        position: 'absolute',
        top: 270,
        left: 540,
    },
    statue: {
        width: 365,
        height: 336,
        position: 'absolute',
        top: 220,
        left: -50,
    },
    paresseux: {
        width: 400,
        height: 2300,
        // position: 'absolute',
        // top: winH
    },
    touchableParesseux: {
        width: 400,
        height: 400,
        position: 'absolute',
        zIndex: 5,
        top: 0,
        left: 0
    },
    toudou: {
        width: 200,
        height: 356,
        position: 'absolute',
        top: 110,
        transform: [{
            rotateY: '180deg'
        }, {
            translateX: -630
        }]
    },
    viewBox: {
        height: winH,
        width: winW,
        position: 'absolute',
        zIndex: 3,
        left: 180
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    }
});

