/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TouchableHighlight,
    AsyncStorage
} from 'react-native';
import Animator from '../../../../../components/interactions/Animator';
import AppBorder from '../../../../../components/partials/AppBorder';
import DraggableItem from '../../../../../components/interactions/draganddrop/DraggableItem';
import DropZone from '../../../../../components/interactions/draganddrop/DropZone';
import * as Animatable from 'react-native-animatable';

DragItemAnim = Animatable.createAnimatableComponent(DraggableItem);


let winW = Dimensions.get("window").width;
let winH = Dimensions.get("window").height;
let count = 0;

export default class Page_4 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data
        }
    }

    componentDidMount(){
        this.props.playSound(3, true);
    }

    componentWillUnmount(){
        this.props.stopSound(3);
    }

    gestureManager(gesture) {
        return this.dropzone.isOverDropZone(gesture);
    }

    gestureBundleManager(gesture) {
        return this.dropzoneBundle.isOverDropZone(gesture);
    }

    displayTrophy() {
        this.refs.mask.transitionTo({opacity: 1}, 800);
        this.refs.trophy.transitionTo({transform: [{scale: 1}]}, 1000);
        setTimeout(() => {
            this.refs.bundleIcon.transitionTo({opacity: 1}, 800);
        }, 1000);
    }

    hideTrophy() {
        this.refs.dragTrophy.transitionTo({opacity: 0}, 1000);
        AsyncStorage.setItem('trophy_1', JSON.stringify(true));
        setTimeout(() => {
            this.refs.mask.transitionTo({opacity: 0}, 800);
            this.refs.trophy.transitionTo({transform: [{scale: 0}]}, 800);
            this.props.onNextPage();
        }, 1000);
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('./assets/bg.png')} style={styles.image_bg}>
                    <Image
                        source={require('./assets/arbre.gif')}
                        style={styles.arbre}
                    />
                    <Image
                        source={require('./assets/feuille5.gif')}
                        style={styles.feuille_5_move}
                    />
                    <Image
                        source={require('./assets/feuille5.png')}
                        style={styles.feuille_5}
                    />

                    <Image source={require('./assets/bg_ground.png')} style={styles.image_bg_ground}/>
                    <Image
                        source={require('./assets/feuille-arbre.gif')}
                        style={styles.feuille_bas_arbre}
                    />
                    <Image
                        source={require('./assets/feuille-bas-arbre.png')}
                        duration={2500}
                        repeat={false}
                        style={styles.feuille_bas_arbre}
                    />
                    <Image
                        source={require('./assets/caillou1.png')}
                        style={styles.caillou_1}
                    />
                    <Image
                        source={require('./assets/caillou2.png')}
                        style={styles.caillou_2}
                    />
                    <Image
                        source={require('./assets/caillou3.png')}
                        style={styles.caillou_3}
                    />
                    <Image
                        source={require('./assets/feuille1.gif')}
                        style={styles.feuille_1_move}
                    />
                    <Image
                        source={require('./assets/feuille1.png')}
                        style={styles.feuille_1}
                    />
                    <Image
                        source={require('./assets/feuille2.png')}
                        style={styles.feuille_2}
                    />
                    <Image
                        source={require('./assets/feuille3-bis.png')}
                        style={styles.feuille_3b}
                    />
                    {/*<Image*/}
                        {/*source={require('./assets/feuille4.png')}*/}
                        {/*style={styles.feuille_4}*/}
                    {/*/>*/}
                    <Image
                        source={require('./assets/feuille3.gif')}
                        style={styles.feuille_3_move}
                    />
                    <Image
                        source={require('./assets/feuille3.png')}
                        style={styles.feuille_3}
                    />
                    <Image
                        source={require('./assets/toucan.gif')}
                        style={styles.toucan}
                    />
                    <Image
                        source={require('./assets/toudou_statique.gif')}
                        style={styles.toudou}
                    />

                    <DraggableItem
                        height={99}
                        width={82}
                        minX={0}
                        maxX={winW}
                        minY={0}
                        maxY={winH}
                        x={570}
                        y={310}
                        lockX={false}
                        lockY={false}
                        gestureHandler={(gesture) => {
                            return this.gestureManager(gesture);
                        }}
                        callback={() => {
                            this.refs.papaye.transitionTo({transform: [{scale: 0}]}, 2000);
                            this.props.onNextPage();
                        }}
                    >
                        <Animatable.Image ref={'papaye'} source={require('./assets/papaye.png')} style={styles.papaye}/>
                    </DraggableItem>

                    <DropZone
                        height={120}
                        width={200}
                        x={680}
                        y={80}
                        toX={140}
                        toY={-250}
                        lockX={false}
                        lockY={false}
                        ref={(dropzone) => {
                            this.dropzone = dropzone;
                        }}
                    />


                    <Animatable.View ref={'mask'} style={styles.mask}/>
                    <Animatable.View ref={'trophy'} style={styles.trophy}>

                        <Animatable.Image animation={bundle} iterationCount={'infinite'} duration={3000} ref={'bundleIcon'} source={require('../../../../../assets/img/story/bundle_unlock.png')} style={styles.iconBundle}/>

                        <Animator
                            source={require('../../assets/bundle/json/feather_bg.json')}
                            duration={2000}
                            repeat={false}
                            style={styles.trophyFeatherBg}
                        />
                        <Animatable.View ref={'dragTrophy'} style={{backgroundColor: 'green'}}>

                            <DraggableItem
                                height={280}
                                width={100}
                                minX={0}
                                maxX={winW}
                                minY={0}
                                maxY={winH}
                                x={455}
                                y={110}
                                lockX={false}
                                lockY={false}
                                gestureHandler={(gesture) => {
                                    return this.gestureBundleManager(gesture);
                                }}
                                callback={() => {
                                    this.hideTrophy();
                                }}
                            >
                                <Animator
                                    source={require('../../assets/bundle/json/feather.json')}
                                    duration={2000}
                                    repeat={false}
                                    style={styles.trophyFeatherImg}
                                />
                            </DraggableItem>
                        </Animatable.View>

                        <DropZone
                            height={160}
                            width={160}
                            x={430}
                            y={480}
                            toX={0}
                            toY={240}
                            lockX={false}
                            lockY={false}
                            ref={(dropzoneBundle) => {
                                this.dropzoneBundle = dropzoneBundle;
                            }}
                        />

                    </Animatable.View>
                </Image>

                {/*Cheat code*/}
                <TouchableHighlight onPress={() => this.displayTrophy()} style={styles.cheatCode} underlayColor="0">
                    <View/>
                </TouchableHighlight>


                {/*Border*/}
                <Image source={require('../../../../../assets/img/others/app_left_border.png')}
                       style={styles.leftBorder}/>
                <Image source={require('../../../../../assets/img/others/app_right_border.png')}
                       style={styles.rightBorder}/>
                <Image source={require('../../../../../assets/img/others/app_top_border.png')}
                       style={styles.topBorder}/>
            </View>
        );
    }
}

const bundle = {
    0: {
        transform: [{
            scale: 0.5
        }]
    },
    0.5: {
        transform: [{
            scale: 0.6
        }]
    },
    1: {
        transform: [{
            scale: 0.5
        }]
    }
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#dbffff',
        width: '100%',
        height: '100%'
    },
    image_bg: {
        width: '100%',
        height: '100%'
    },
    image_bg_ground: {
        width: '100%',
        height: 160,
        position: 'relative',
        top: 490
    },
    arbre: {
        width: 420,
        height: 570,
        position: 'absolute',
        top: 0,
        left: 600,
    },
    arbre_move: {
        width: 210,
        height: 285,
        position: 'absolute',
        top: -20,
        left: 360,
    },
    feuille_bas_arbre: {
        width: 246,
        height: 242,
        position: 'absolute',
        top: 300,
        left: 790,
    },
    feuille_bas_arbre_move: {
        width: 246,
        height: 242,
        position: 'absolute',
        top: 140,
        left: 390,
    },
    feuille_1: {
        width: 102,
        height: 82,
        position: 'absolute',
        top: 210,
        left: 20
    },
    feuille_1_move: {
        width: 102,
        height: 132,
        position: 'absolute',
        top: 160,
        left: 10
    },
    feuille_2: {
        width: 115,
        height: 103,
        position: 'absolute',
        top: 200,
        left: 240
    },
    feuille_3: {
        width: 156,
        height: 246,
        position: 'absolute',
        top: 440,
        left: 20
    },
    feuille_3b: {
        width: 160,
        height: 325,
        position: 'absolute',
        top: 340,
        left: 100
    },
    feuille_3_move: {
        width: 365,
        height: 370,
        position: 'absolute',
        bottom: -20,
        left: 0
    },
    feuille_4: {
        width: 117,
        height: 192,
        position: 'absolute',
        top: 460,
        left: 110
    },
    feuille_4_move: {
        width: 234,
        height: 384,
        position: 'absolute',
        top: 150,
        left: 30
    },
    feuille_5: {
        width: 236,
        height: 274,
        position: 'absolute',
        top: 280,
        left: 340
    },
    feuille_5_move: {
        width: 300,
        height: 300,
        position: 'absolute',
        top: 280,
        left: 280
    },
    toucan: {
        width: 220,
        height: 180,
        position: 'absolute',
        top: 60,
        right: 120
    },
    toudou: {
        width: 200,
        height: 356,
        position: 'absolute',
        top: 240,
        left: 400
    },
    liane_1: {
        width: 160,
        height: 400,
        position: 'absolute',
        top: -110,
        left: 190
    },
    liane_2: {
        width: 100,
        height: 600,
        position: 'absolute',
        top: -30,
        left: 250
    },
    caillou_1: {
        width: 142,
        height: 66,
        position: 'absolute',
        top: 480,
        left: 190
    },
    caillou_2: {
        width: 225,
        height: 88,
        position: 'absolute',
        top: 460,
        left: 580
    },
    caillou_3: {
        width: 163,
        height: 41,
        position: 'absolute',
        top: 518,
        left: 840
    },
    feuille_mask_1: {
        width: '100%',
        height: '100%',
    },
    papaye: {
        width: 65,
        height: 80
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    },
    trophy: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: winW,
        height: winH,
        transform: [{
            scale: 0
        }]
    },
    trophyFeatherImg: {
        width: 100,
        height: 280,
    },
    trophyFeatherBg: {
        position: 'absolute',
        width: 302,
        height: 460,
        left: (winW / 4 - 75),
        top: 10
    },
    mask: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'rgba(255,255,255, 0.7)',
        opacity: 0
    },
    iconBundle: {
        position: 'absolute',
        bottom: 130,
        left: winW / 2 - 75,
        height: 150,
        width: 150,
        opacity: 0
    },
    cheatCode: {
        position: 'absolute',
        width: 80,
        height: 80,
        right: 27,
        bottom: 20,
    }
});

