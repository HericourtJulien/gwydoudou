/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';
import Video from 'react-native-video';
import AppBorder from '../../../../../components/partials/AppBorder';

export default class Page_12 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data
        }
    }

    componentDidMount() {
        var i = 0;
        var maxNextPage = 3;

        for (i; i < maxNextPage; i++) {
            setTimeout(() => {
                this.nextPage(false);
            }, i * 4000);
        }
    }

    nextPage(home) {
        this.props.onNextPage(home);
    }

    render() {
        return (
            <View>
                <Video
                    source={require('./assets/movie.mp4')}
                    onEnd={() => this.nextPage(true)}
                    style={styles.bg}
                />

                {/*Border*/}
                <Image source={require('../../../../../assets/img/others/app_left_border.png')} style={styles.leftBorder}/>
                <Image source={require('../../../../../assets/img/others/app_right_border.png')} style={styles.rightBorder}/>
                <Image source={require('../../../../../assets/img/others/app_top_border.png')} style={styles.topBorder}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bg: {
        width: '100%',
        height: '100%'
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    }
});

