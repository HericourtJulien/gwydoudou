/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions
} from 'react-native';
import Animator from '../../../../../components/interactions/Animator';
import DragItem from '../../../../../components/interactions/draganddrop/DraggableItem';
import DropZone from '../../../../../components/interactions/draganddrop/DropZone';
import IntAnim from '../../../../../datas/InterfaceAnimations';
import * as Animatable from 'react-native-animatable';
import AppBorder from '../../../../../components/partials/AppBorder';


let width = Dimensions.get("window").width;
let height = Dimensions.get("window").height;

export default class Page_1 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data
        }
    }

    nextPage() {
        this.props.onNextPage();
    }

    gestureManager(gesture) {
        return (this.dropzone.isOverDropZone(gesture));
    }

    componentWillUnmount(){
        this.props.stopSound(3);
        this.props.stopSound(9);
    }

    componentDidMount() {
        // EXAMPLE
        console.log("mounted");

        this.props.playSound(3, true);
        this.props.playSound(9, true);

        this.props.adjustVolume(9, 0.3);

    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('./assets/bg.png')} style={styles.bg}/>

                <Animator
                    source={require('./assets/json/feuille_3.json')}
                    duration={3000}
                    repeat={false}
                    style={styles.feuille_3_move}
                />
                <Image source={require('./assets/feuille_3.png')} style={styles.feuille_3}/>


                <Image source={require('./assets/foret_3.png')} style={styles.foret_3}/>


                <Animator
                    source={require('./assets/json/feuille_4.json')}
                    duration={3000}
                    repeat={false}
                    style={styles.feuille_4_move}
                />
                <Image source={require('./assets/feuille_4.png')} style={styles.feuille_4}/>

                <Image source={require('./assets/feuille_5.png')} style={styles.feuille_5}/>
                <Animator
                    source={require('./assets/json/feuille_5.json')}
                    duration={3000}
                    repeat={false}
                    style={styles.feuille_5_move}
                />

                <Image source={require('./assets/foret_4.png')} style={styles.foret_4}/>

                <Animatable.Image
                    source={require('./assets/nuage_1.png')}
                    animation={IntAnim.ANIMATIONS.CLOUD}
                    iterationCount={'infinite'}
                    duration={5000}
                    style={styles.nuage_1}
                />
                <Animatable.Image
                    source={require('./assets/nuage_2.png')}
                    animation={IntAnim.ANIMATIONS.CLOUD}
                    iterationCount={'infinite'}
                    duration={5000}
                    style={styles.nuage_2}
                />

                <Image source={require('./assets/feuille_1b.png')} style={styles.feuille_1b}/>
                <Animator
                    source={require('./assets/json/feuille_1.json')}
                    duration={3000}
                    repeat={false}
                    style={styles.feuille_1_move}
                />
                <Image source={require('./assets/feuille_1.png')} style={styles.feuille_1}/>

                <Animator
                    source={require('./assets/json/caillou_2.json')}
                    duration={3000}
                    repeat={false}
                    style={styles.caillou_2}
                />


                <DropZone
                    width={200}
                    height={200}
                    x={650}
                    y={300}
                    toX={400}
                    toY={0}
                    lockX={false}
                    lockY={true}
                    ref={(dropzone) => {
                        this.dropzone = dropzone;
                    }}
                />

                <DragItem
                    gestureHandler={(gesture) => this.gestureManager(gesture)}
                    callback={() => this.nextPage()}
                    style={styles.toudou_drag}
                    width={500}
                    height={300}
                    minX={0}
                    toX={(width - 200)}
                    maxX={width - 200}
                    lockY={true}
                >
                    <Animator
                        source={require('./assets/json/toudou_static.json')}
                        duration={3000}
                        repeat={false}
                        style={styles.toudou}
                    />
                </DragItem>

                <Animator
                    source={require('./assets/json/caillou_3.json')}
                    duration={3000}
                    repeat={false}
                    style={styles.caillou_3}
                />

                <Animator
                    source={require('./assets/json/caillou_4.json')}
                    duration={3000}
                    repeat={false}
                    style={styles.caillou_4}
                />

                <Animator
                    source={require('./assets/json/caillou_5.json')}
                    duration={3000}
                    repeat={false}
                    style={styles.caillou_5}
                />

                <Animator
                    source={require('./assets/json/caillou_1.json')}
                    duration={3000}
                    repeat={false}
                    style={styles.caillou_1}
                />

                <Animator
                    source={require('./assets/json/nenuphar.json')}
                    duration={3000}
                    repeat={false}
                    style={styles.nenuphar}
                />


                <Animator
                    source={require('./assets/json/feuille_2.json')}
                    duration={3000}
                    repeat={false}
                    style={styles.feuille_2_move}
                />
                <Image source={require('./assets/feuille_2.png')} style={styles.feuille_2}/>


                {/*Border*/}
                <Image source={require('../../../../../assets/img/others/app_left_border.png')} style={styles.leftBorder}/>
                <Image source={require('../../../../../assets/img/others/app_right_border.png')} style={styles.rightBorder}/>
                <Image source={require('../../../../../assets/img/others/app_top_border.png')} style={styles.topBorder}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bg: {
        width: '100%',
        height: '100%'
    },
    foret_3: {
        width: 312,
        height: 270,
        position: 'absolute',
        top: 190,
        right: 0
    },
    foret_4: {
        width: 220,
        height: 238,
        position: 'absolute',
        top: 380,
        right: 0
    },
    caillou_1: {
        width: 380,
        height: 140,
        position: 'absolute',
        top: 260,
        left: -50,
        zIndex: 30
    },
    caillou_2: {
        width: 200,
        height: 100,
        position: 'absolute',
        top: 200,
        left: -30
    },
    caillou_3: {
        width: 300,
        height: 120,
        position: 'absolute',
        top: 200,
        left: 130
    },
    caillou_4: {
        width: 90,
        height: 40,
        position: 'absolute',
        top: 220,
        left: 370
    },
    caillou_5: {
        width: 322,
        height: 140,
        position: 'absolute',
        top: 270,
        left: 390
    },
    nuage_1: {
        width: 190,
        height: 42,
        position: 'absolute',
        top: 60,
        left: 350
    },
    nuage_2: {
        width: 101,
        height: 32,
        position: 'absolute',
        top: 140,
        right: 30
    },
    feuille_1: {
        width: 180,
        height: 85,
        position: 'absolute',
        top: 370,
        left: 20
    },
    feuille_1b: {
        width: 122,
        height: 154,
        position: 'absolute',
        top: 300,
        left: 30
    },
    feuille_1_move: {
        width: 340,
        height: 280,
        position: 'absolute',
        top: 100,
        left: -35
    },
    feuille_2: {
        width: 51,
        height: 119,
        position: 'absolute',
        top: 340,
        left: 570,
        transform: [
            {rotate: '25deg'}
        ]
    },
    feuille_2_move: {
        width: 110,
        height: 140,
        position: 'absolute',
        top: 100,
        left: 298,
        transform: [
            {rotate: '25deg'}
        ]
    },
    feuille_3: {
        width: 59,
        height: 49,
        position: 'absolute',
        top: 400,
        left: 690
    },
    feuille_3_move: {
        width: 100,
        height: 100,
        position: 'absolute',
        top: 180,
        left: 330
    },
    feuille_4: {
        width: 78,
        height: 88,
        position: 'absolute',
        top: 370,
        left: 860,
        transform: [
            {rotate: '15deg'}
        ]
    },
    feuille_4_move: {
        width: 80,
        height: 90,
        position: 'absolute',
        top: 185,
        left: 430
    },
    feuille_5: {
        width: 54,
        height: 102,
        position: 'absolute',
        top: 280,
        left: 970
    },
    feuille_5_move: {
        width: 110,
        height: 110,
        position: 'absolute',
        top: 165,
        left: 470,
        // transform: [
        //     {rotate: '-25deg'}
        // ]
    },
    nenuphar: {
        width: 150,
        height: 80,
        position: 'absolute',
        top: 250,
        left: 350
    },
    toudou: {
        width: 500,
        height: 300,
        position: 'absolute',
        top: 152,
        left: 36
    },
    toudou_dropzone: {
        width: 200,
        height: 200,
        position: 'absolute',
        top: 152,
        right: 36,
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    }
});
