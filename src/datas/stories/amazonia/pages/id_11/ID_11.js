/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
/**
 * Created by benja on 12/05/2017.
 */
import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Image,
    Text,
    Dimensions,
    TouchableHighlight,
    Animated,
    DeviceEventEmitter,
    WebView
} from 'react-native';
import { Accelerometer, Gyroscope } from 'react-native-sensors';
import AppBorder from '../../../../../components/partials/AppBorder';

let crocoSystem = require("../../assets/croco/index.html");
let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;
let accelero = false;

let webviewLoaded = false;

const accelerationObservable = new Accelerometer({
    updateInterval: 100, // defaults to 100ms
});

let acceleroInterval;

export default class Page_11 extends Component{

    constructor(props){
        super(props);
    }

    acceleroManager() {

        console.log("accelero manager");

        if(!accelero){
            accelero = true;
            this.startAccel();
        }else{
            this.stopAccel();
        }
    }

    startAccel(){

        console.log("start accelero");

        let correctedValue;
        let right = false;
        let left = false;

        let direction = "";
        let previousDirection;

        //margin used to avoid croco to always going left or right and help user let the croco stay in place
        let margin = 0.2;

        /**
        // WORK ONLY FOR ANDROID

        SensorManager.startAccelerometer(100); // To start the accelerometer with a minimum delay of 100ms between events.
        DeviceEventEmitter.addListener('Accelerometer', function (data) {

            //when data.x is negative, device lean to the right, else lean to the left
            correctedValue = Math.round(data.x);

            //need to stock previous datas to know if we should call a function in the webview or not, to avoid too many call or calling a function at each time
            //the sensorManager give us datas. That's a way of optimization
            previousDirection = direction;

            if(correctedValue < 0 - margin){
                right = true;
                left = false;
                direction = "right";
            }else if(correctedValue > margin){
                right = false;
                left = true;
                direction = "left";
            }else{
                right = false;
                left = false;
                direction = "";
            }

            //check if the direction has changed to call or not functions through the webview bridge
            if(direction == "left"){
                if(previousDirection != direction){
                    webview.sendToBridge(JSON.stringify("left"));
                }
            }else if(direction == "right"){
                if(previousDirection != direction){
                    webview.sendToBridge(JSON.stringify("right"));
                }
            }else{
                if(previousDirection != direction){
                    webview.sendToBridge(JSON.stringify("none"));
                }
            }

            console.log("left", left);
            console.log("right", right);

        });*/

        //FOR IOS
        acceleroInterval = setInterval(() => {
            accelerationObservable
                .map(({ x, y, z }) => y)
                //.filter(speed => speed > 0)
                .subscribe(y => {
                    //console.log(`You moved your phone with ${y}`);
                    correctedValue = y;
                });


            //need to stock previous datas to know if we should call a function in the webview or not, to avoid too many call or calling a function at each time
            //the sensorManager give us datas. That's a way of optimization
            previousDirection = direction;

            if(correctedValue < 0 - margin){
                right = false;
                left = true;
                direction = "right";
            }else if(correctedValue > margin){
                right = true;
                left = false;
                direction = "left";
            }else{
                right = false;
                left = false;
                direction = "";
            }

            //check if the direction has changed to call or not functions through the webview bridge
            if(direction === "left"){
                if(previousDirection !== direction){
                    this.refs.webview.postMessage("left");
                    console.log(direction);
                }
            }else if(direction === "right"){
                if(previousDirection !== direction){
                    this.refs.webview.postMessage("right");
                    console.log(direction);
                }
            }else{
                if(previousDirection !== direction){
                    this.refs.webview.postMessage("none");
                    console.log(direction);
                }
            }

        }, 100);

    }

    stopAccel(){
        /** WORK ONLY FOR ANDROID
            SensorManager.stopAccelerometer();
         */

        clearInterval(acceleroInterval);
        console.log("STOP");
    }

    componentDidMount(){
        this.props.playSound(9, true);
        this.props.playSound(10, true);
        this.props.adjustVolume(9, 0.3);
    }

    componentWillUnmount(){
        this.props.stopSound(9);
        this.props.stopSound(10);
    }

    onMessage(webViewData) {

        let data = webViewData.nativeEvent.data;

        if(data === "finished"){
            this.acceleroManager();
            this.props.onNextPage();
        }
    }

    render() {

        return (
            <View style={styles.container}>

                <WebView
                    /*source={crocoSystem}*/
                    source={{uri:'http://benjaminpadernoz.com/gobelins/toudou/croco'}}
                    scalesPageToFit={true}
                    style={styles.webView}
                    ref="webview"
                    onMessage={ this.onMessage.bind(this) }
                    onLoad={() => {
                        console.log("webview finished loading");
                        this.acceleroManager();
                        webviewLoaded = true;
                    }}
                />

                {/*Border*/}
                <Image source={require('../../../../../assets/img/others/app_left_border.png')} style={styles.leftBorder}/>
                <Image source={require('../../../../../assets/img/others/app_right_border.png')} style={styles.rightBorder}/>
                <Image source={require('../../../../../assets/img/others/app_top_border.png')} style={styles.topBorder}/>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        position:'absolute',
        top:0,
        left:0,
        backgroundColor:'#a6a8a6',
        overflow:'hidden',
        height:height,
        width:"100%"
    },
    webView: {
        position:"absolute",
        width:"100%",
        overflow:'hidden',
        height:height
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    }
});