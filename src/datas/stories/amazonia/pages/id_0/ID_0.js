/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions
} from 'react-native';
import AppBorder from '../../../../../components/partials/AppBorder';
import Video from 'react-native-video';

let width = Dimensions.get("window").width;
let height = Dimensions.get("window").height;

export default class Page_0 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data
        }
    }

    nextPage() {
        this.props.onNextPage();
    }

    render() {
        return (
            <View>
                <Video
                    source={require('./assets/movie.mp4')}
                    onEnd={() => this.nextPage()}
                    style={styles.bg}
                />

                {/*Border*/}
                <Image source={require('../../../../../assets/img/others/app_left_border.png')} style={styles.leftBorder}/>
                <Image source={require('../../../../../assets/img/others/app_right_border.png')} style={styles.rightBorder}/>
                <Image source={require('../../../../../assets/img/others/app_top_border.png')} style={styles.topBorder}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bg: {
        width: '100%',
        height: '100%'
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    }
});