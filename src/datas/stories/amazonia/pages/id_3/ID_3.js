/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    TouchableOpacity
} from 'react-native';
import Animator from '../../../../../components/interactions/Animator';
import AppBorder from '../../../../../components/partials/AppBorder';
import Touch from '../../../../../components/interactions/Touch';
import * as Animatable from 'react-native-animatable';

TouchAnimatable = Animatable.createAnimatableComponent(TouchableHighlight);


export default class Page_3 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data,
            disabled: [
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
                {type: false},
            ]
        };
    }

    componentDidMount(){
        this.props.playSound(3, true);
    }

    componentWillUnmount(){
        this.props.stopSound(3);
    }

    disableTouch(id) {
        this.state.disabled[id].type = true;
        this.forceUpdate();
    }

    gotPapaye() {
        //alert('gagné');
        this.props.onNextPage();
    }

    render() {
        console.log(this.state.disabled);
        return (
            <View>
                <Image
                    source={require('./assets/bg.png')}
                    style={styles.bg}>
                    <Image
                        source={require('./assets/bg_2.png')}
                        style={styles.bg}>

                        <Animatable.View ref="papaye">
                            <Touch
                                onPress={() => {
                                    this.props.playSound(5);
                                    this.gotPapaye();

                                }}
                                style={styles.papaye_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/papaye.png')}
                                    style={styles.papaye}
                                />
                            </Touch>
                        </Animatable.View>


                        <Animatable.View ref="feuille_3">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_3.stopAnimation();
                                    this.refs.feuille_3.animate(leaveLeft, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(3);
                                }}
                                disabled={this.state.disabled[3].type}
                                style={styles.feuille_3_touch}
                                underlayColor="0"
                            >
                                {/*<Animator
                                    source={require('./assets/json/feuille_3.json')}
                                    duration={3000}
                                    style={styles.feuille_3}
                                />*/}
                                <Image
                                    source={require('./assets/feuille3.gif')}
                                    style={styles.feuille_3}
                                />
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_4">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_4.stopAnimation();
                                    this.refs.feuille_4.animate(leaveLeft, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(4);
                                }}
                                disabled={this.state.disabled[4].type}
                                style={styles.feuille_4_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille4.gif')}
                                    style={styles.feuille_4}
                                />
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_7">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_7.stopAnimation();
                                    this.refs.feuille_7.animate(leaveLeft, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(7);
                                }}
                                disabled={this.state.disabled[7].type}
                                style={styles.feuille_7_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille7.gif')}
                                    style={styles.feuille_7}
                                />
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_5">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_5.stopAnimation();
                                    this.refs.feuille_5.animate(leaveLeft, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(5);
                                }}
                                disabled={this.state.disabled[5].type}
                                style={styles.feuille_5_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille5.gif')}
                                    style={styles.feuille_5}
                                />
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_1">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_1.stopAnimation();
                                    this.refs.feuille_1.animate(leaveLeft, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(1);
                                }}
                                disabled={this.state.disabled[1].type}
                                style={styles.feuille_1_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille1.gif')}
                                    style={styles.feuille_1}
                                />
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_2">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_2.stopAnimation();
                                    this.refs.feuille_2.animate(leaveLeft, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(2);
                                }}
                                disabled={this.state.disabled[2].type}
                                style={styles.feuille_2_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille_2.png')}
                                    style={styles.feuille_2}/>
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_16">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_16.stopAnimation();
                                    this.refs.feuille_16.animate(leaveRight, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(16);
                                }}
                                disabled={this.state.disabled[16].type}
                                style={styles.feuille_16_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille_16.png')}
                                    style={styles.feuille_16}/>
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_10">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_10.stopAnimation();
                                    this.refs.feuille_10.animate(leaveRight, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(10);
                                }}
                                disabled={this.state.disabled[10].type}
                                style={styles.feuille_10_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille_10.png')}
                                    style={styles.feuille_10}/>
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_14">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_14.stopAnimation();
                                    this.refs.feuille_14.animate(leaveRight, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(14);
                                }}
                                disabled={this.state.disabled[14].type}
                                style={styles.feuille_14_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille_14.png')}
                                    style={styles.feuille_14}/>
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_8">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_8.stopAnimation();
                                    this.refs.feuille_8.animate(leaveLeft, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(8);
                                }}
                                disabled={this.state.disabled[8].type}
                                style={styles.feuille_8_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille_8.png')}
                                    style={styles.feuille_8}/>
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_9">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_9.stopAnimation();
                                    this.refs.feuille_9.animate(leaveLeft, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(9);
                                }}
                                disabled={this.state.disabled[9].type}
                                style={styles.feuille_9_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille9.gif')}
                                    style={styles.feuille_9}
                                />
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_6">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_6.stopAnimation();
                                    this.refs.feuille_6.animate(leaveLeft, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(6);
                                }}
                                disabled={this.state.disabled[6].type}
                                style={styles.feuille_6_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille_6.png')}
                                    style={styles.feuille_6}/>
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_11">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_11.stopAnimation();
                                    this.refs.feuille_11.animate(leaveRight, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(11);
                                }}
                                disabled={this.state.disabled[11].type}
                                style={styles.feuille_11_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille_11.png')}
                                    style={styles.feuille_11}/>
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_13">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_13.stopAnimation();
                                    this.refs.feuille_13.animate(leaveRight, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(13);
                                }}
                                disabled={this.state.disabled[13].type}
                                style={styles.feuille_13_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille_13.png')}
                                    style={styles.feuille_13}/>
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_15">
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_15.stopAnimation();
                                    this.refs.feuille_15.animate(leaveRight, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(15);
                                }}
                                disabled={this.state.disabled[15].type}
                                style={styles.feuille_15_touch}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille15.gif')}
                                    style={styles.feuille_15}
                                />
                            </Touch>
                        </Animatable.View>

                        <Animatable.View ref="feuille_12" style={styles.feuille_12_touch}>
                            <Touch
                                onPress={() => {
                                    this.refs.feuille_12.stopAnimation();
                                    this.refs.feuille_12.animate(leaveRight, 600).then(() => {
                                        this.forceUpdate();
                                    });
                                    this.disableTouch(12);
                                }}
                                disabled={this.state.disabled[12].type}
                                underlayColor="0"
                            >
                                <Image
                                    source={require('./assets/feuille12.gif')}
                                    style={styles.feuille_12}
                                />
                            </Touch>
                        </Animatable.View>

                        <Image
                            source={require('./assets/cailloux.png')}
                            style={styles.cailloux}/>
                        <Image
                            source={require('./assets/cailloux_2.png')}
                            style={styles.cailloux_2}/>
                    </Image>
                </Image>

                {/*Border*/}
                <Image source={require('../../../../../assets/img/others/app_left_border.png')} style={styles.leftBorder}/>
                <Image source={require('../../../../../assets/img/others/app_right_border.png')} style={styles.rightBorder}/>
                <Image source={require('../../../../../assets/img/others/app_top_border.png')} style={styles.topBorder}/>
            </View>
        );
    }
}

const leaveRight = {
    0: {
        translateX: 0,
    },
    1: {
        translateX: 150,
    }
};

const leaveLeft = {
    0: {
        translateX: 0,
    },
    1: {
        translateX: -150,
    }
};

const styles = StyleSheet.create({
    bg: {
        width: '100%',
        height: '100%'
    },
    cailloux: {
        position: 'absolute',
        top: 470,
        left: -240,
        width: 382,
        height: 150
    },
    cailloux_2: {
        position: 'absolute',
        top: 550,
        left: 260,
        width: 385,
        height: 75
    },
    feuille_1_touch: {
        position: 'absolute',
        top: 340,
        left: 180,
    },
    feuille_1: {
        width: 200,
        height: 260
    },
    feuille_2_touch: {
        position: 'absolute',
        top: 400,
        left: 240,
    },
    feuille_2: {
        width: 240,
        height: 175
    },
    feuille_3_touch: {
        position: 'absolute',
        top: 270,
        left: 230,
    },
    feuille_3: {
        width: 170,
        height: 300
    },
    feuille_4_touch: {
        position: 'absolute',
        top: 200,
        left: 240,
    },
    feuille_4: {
        width: 170,
        height: 360
    },
    feuille_5_touch: {
        position: 'absolute',
        top: 250,
        left: 250,
    },
    feuille_5: {
        width: 200,
        height: 260
    },
    feuille_6_touch: {
        position: 'absolute',
        top: 410,
        left: 390,
    },
    feuille_6: {
        width: 249,
        height: 184
    },
    feuille_7_touch: {
        position: 'absolute',
        top: 140,
        left: 390,
    },
    feuille_7: {
        width: 160,
        height: 450
    },
    feuille_8_touch: {
        position: 'absolute',
        top: 140,
        left: 470,
    },
    feuille_8: {
        width: 126,
        height: 405
    },
    feuille_9_touch: {
        position: 'absolute',
        top: 130,
        left: 580,
    },
    feuille_9: {
        width: 140,
        height: 465
    },
    feuille_10_touch: {
        position: 'absolute',
        top: 280,
        left: 540,
    },
    feuille_10: {
        width: 233,
        height: 298
    },
    feuille_11_touch: {
        position: 'absolute',
        top: 400,
        left: 630,
    },
    feuille_11: {
        width: 169,
        height: 188
    },
    feuille_12_touch: {
        position: 'absolute',
        top: 330,
        left: 700,
    },
    feuille_12: {
        width: 260,
        height: 320
    },
    feuille_13_touch: {
        position: 'absolute',
        top: 130,
        left: 670,
    },
    feuille_13: {
        width: 40,
        height: 440
    },
    feuille_14_touch: {
        position: 'absolute',
        top: 280,
        left: 730,
    },
    feuille_14: {
        width: 105,
        height: 330
    },
    feuille_15_touch: {
        position: 'absolute',
        top: 180,
        left: 720,
    },
    feuille_15: {
        width: 180,
        height: 400
    },
    feuille_16_touch: {
        position: 'absolute',
        top: 300,
        left: 750,
    },
    feuille_16: {
        width: 222,
        height: 276
    },
    papaye_touch: {
        position: 'absolute',
        top: 480,
        left: 570,
    },
    papaye: {
        width: 82,
        height: 99
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    }
});

const dropzoneStyle = StyleSheet.create({
    left:{
        position:'absolute',
        top:0,
        left:0,
        height:"100%",
        width:100,
        backgroundColor:"#e33b3b"
    },
    right:{
        position:'absolute',
        right:0,
        top:0,
        height:"100%",
        width:100,
        backgroundColor:"#ffd700"
    }
});

