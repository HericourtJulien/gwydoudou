/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    Dimensions,
    Animated,
    ScrollView
} from 'react-native';
import Animator from '../../../../../components/interactions/Animator';
import AppBorder from '../../../../../components/partials/AppBorder';
import * as Animatable from 'react-native-animatable';
import Touch from '../../../../../components/interactions/Touch';
import Video from 'react-native-video';

TouchAnimatable = Animatable.createAnimatableComponent(TouchableHighlight);

let winW = Dimensions.get("window").width;
let winH = Dimensions.get("window").height;

export default class Page_2 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data,
            disabled: [
                {type: false},
                {type: false},
                {type: false},
                {type: false}
            ]
        };
    }

    componentDidMount() {
        this.props.playSound(3, true);
    }

    componentWillUnmount(){
        this.props.stopSound(3);
    }

    disableTouch(id) {
        this.state.disabled[id].type = true;
        this.forceUpdate();
    }

    render() {
        return (
            <View style={styles.container}>
                {/*<Image source={require('./assets/bg.png')} style={styles.image_bg}/>
                <Image
                    source={require('./assets/arbre.gif')}
                    style={styles.arbre}
                />


                <Image source={require('./assets/bg_ground.png')} style={styles.image_bg_ground}/>
                <Image
                    source={require('./assets/feuille5.gif')}
                    style={styles.feuille_5_move}
                />
                <Image
                    source={require('./assets/feuille5.png')}
                    style={styles.feuille_5}
                />

                <Image
                    source={require('./assets/feuille-arbre.gif')}
                    style={styles.feuille_bas_arbre_move}
                />
                <Image
                    source={require('./assets/feuille-bas-arbre.png')}
                    style={styles.feuille_bas_arbre}
                />
                <Image
                    source={require('./assets/caillou1.png')}
                    style={styles.caillou_1}
                />
                <Image
                    source={require('./assets/caillou2.png')}
                    style={styles.caillou_2}
                />
                <Image
                    source={require('./assets/caillou3.png')}
                    style={styles.caillou_3}
                />

                <Image
                    source={require('./assets/feuille1.gif')}
                    style={styles.feuille_1_move}
                />
                <Image
                    source={require('./assets/feuille1.png')}
                    style={styles.feuille_1}
                />
                <Image
                    source={require('./assets/feuille2.gif')}
                    style={styles.feuille_2_move}
                />
                <Image
                    source={require('./assets/feuille2.png')}
                    style={styles.feuille_2}
                />

                <Image
                    source={require('./assets/feuille3.gif')}
                    style={styles.feuille_3_move}
                />
                <Image
                    source={require('./assets/feuille3-bis.png')}
                    style={styles.feuille_3b}
                />
                <Image
                    source={require('./assets/feuille4.png')}
                    style={styles.feuille_4}
                />
                <Image
                    source={require('./assets/feuille3.png')}
                    style={styles.feuille_3}
                />

                <Animator
                    source={require('./assets/json/liane1.json')}
                    duration={5000}
                    repeat={false}
                    style={styles.liane_1}
                />
                <Animator
                    source={require('./assets/json/liane2.json')}
                    duration={5000}
                    repeat={false}
                    style={styles.liane_2}
                />


                <Touch
                    onPress={() => {
                        this.props.playSound(11);
                    }}
                    underlayColor={0}
                >
                    <Image
                        source={require('./assets/toucan.gif')}
                        style={styles.toucan}
                    />
                </Touch>


                <Image
                    source={require('./assets/toudou.gif')}
                    style={styles.toudou}
                />
                <View style={styles.leafContainer}>

                    <Animatable.View
                        ref="feuilleHG"
                        duration={1500}
                    >
                        <Touch
                            style={styles.fHG}
                            onPress={() => {
                                this.refs.feuilleHG.animate(leaveUpLeft, 600).then(() => {
                                    this.forceUpdate();
                                });
                                this.disableTouch(2);
                            }}
                            disabled={this.state.disabled[2].type}
                            underlayColor={0}
                        >
                            <Image
                                source={require("./assets/feuille1-hautG.png")}
                                style={styles.f3}
                            />
                        </Touch>
                    </Animatable.View>

                    <Animatable.View
                        ref="feuilleHD"
                        duration={1500}
                    >
                        <Touch
                            style={styles.fHD}
                            onPress={() => {
                                this.refs.feuilleHD.animate(leaveUpRight, 600).then(() => {
                                    this.forceUpdate();
                                });
                                this.disableTouch(3);
                            }}
                            disabled={this.state.disabled[3].type}
                            underlayColor={0}
                        >
                            <Image
                                source={require("./assets/feuille4-hautD.png")}
                                style={styles.f4}
                            />
                        </Touch>
                    </Animatable.View>

                <Animatable.View
                    ref="feuilleBG"
                    duration={1500}
                >
                    <Touch
                        style={styles.fBG}
                        onPress={() => {
                            this.refs.feuilleBG.animate(leaveBottomLeft, 600).then(() => {
                                //this.forceUpdate();
                            });
                            this.disableTouch(0);
                        }}
                        disabled={this.state.disabled[0].type}
                        underlayColor={0}
                    >
                        <Animator
                         source={require("./assets/json/feuille2-basG.json")}
                         duration={3500}
                         width={800}
                         height={1000}
                         repeat={true}
                         />
                        <Image
                            source={require("./assets/feuille2-basG.png")}
                            style={styles.f1}
                        />
                    </Touch>
                </Animatable.View>


                <Animatable.View
                    ref="feuilleBD"
                    duration={1500}
                >
                    <Touch
                        style={styles.fBD}
                        onPress={() => {
                            this.refs.feuilleBD.animate(leaveBottomRight, 600).then(() => {
                                //this.forceUpdate();
                            });
                            this.disableTouch(1);
                        }}
                        disabled={this.state.disabled[1].type}
                        underlayColor={0}
                    >
                        <Image
                            source={require("./assets/feuille3-basD.png")}
                            style={styles.f2}
                        />
                    </Touch>
                </Animatable.View>*/}
                {/*</View>*/}

                <Video
                    source={require('./assets/ID2.mp4')}
                    onEnd={() => {}}
                    style={styles.bg}
                />

                {/*Border*/}
                <Image source={require('../../../../../assets/img/others/app_left_border.png')} style={styles.leftBorder}/>
                <Image source={require('../../../../../assets/img/others/app_right_border.png')} style={styles.rightBorder}/>
                <Image source={require('../../../../../assets/img/others/app_top_border.png')} style={styles.topBorder}/>
            </View>
        );
    }

}

const leaveUpRight = {
    0: {
        left: 0,
        top: 0
    },
    1: {
        left: 600,
        top: -600
    }
};

const leaveUpLeft = {
    0: {
        left: 0,
        top: 0,
    },
    1: {
        left: -600,
        top: -600,
    }
};

const leaveBottomLeft = {
    0: {
        left: 0,
        top: 0,
    },
    1: {
        left: -600,
        top: 600,
    }
};

const leaveBottomRight = {
    0: {
        left: 0,
        top: 0,
    },
    1: {
        left: 600,
        top: 600,
    }
};

const styles = StyleSheet.create({
    bg: {
        width: '100%',
        height: '100%'
    },
    container: {
        backgroundColor: '#dbffff',
        width: winW,
        height: winH
    },
    image_bg: {
        width: winW,
        height: winH,
        left: 0,
        top: 0,
        position: 'absolute',
    },
    image_bg_ground: {
        width: '100%',
        height: 160,
        position: 'relative',
        top: 490
    },
    arbre: {
        width: 420,
        height: 570,
        position: 'absolute',
        top: 0,
        left: 600,
    },
    arbre_move: {
        width: 210,
        height: 285,
        position: 'absolute',
        top: -20,
        left: 360,
    },
    feuille_bas_arbre: {
        width: 246,
        height: 252,
        position: 'absolute',
        top: 300,
        left: 790,
    },
    feuille_bas_arbre_move: {
        width: 246,
        height: 242,
        position: 'absolute',
        top: 300,
        left: 790,
    },
    feuille_1: {
        width: 102,
        height: 82,
        position: 'absolute',
        top: 210,
        left: 20
    },
    feuille_1_move: {
        width: 102,
        height: 132,
        position: 'absolute',
        top: 160,
        left: 10
    },
    feuille_2: {
        width: 115,
        height: 103,
        position: 'absolute',
        top: 290,
        left: 200
    },
    feuille_2_move: {
        width: 130,
        height: 123,
        position: 'absolute',
        top: 250,
        left: 180
    },
    feuille_3: {
        width: 156,
        height: 246,
        position: 'absolute',
        top: 440,
        left: 20
    },
    feuille_3b: {
        width: 180,
        height: 335,
        position: 'absolute',
        top: 340,
        left: 100
    },
    feuille_3_move: {
        width: 300,
        height: 300,
        position: 'absolute',
        bottom: 150,
        left: 0
    },
    feuille_4: {
        width: 117,
        height: 192,
        position: 'absolute',
        top: 460,
        left: 110
    },
    feuille_4_move: {
        width: 234,
        height: 384,
        position: 'absolute',
        top: 150,
        left: 30
    },
    feuille_5: {
        width: 150,
        height: 170,
        position: 'absolute',
        top: 370,
        left: 305
    },
    feuille_5_move: {
        width: 180,
        height: 180,
        position: 'absolute',
        top: 370,
        left: 305
    },
    toucan: {
        width: 220,
        height: 180,
        position: 'absolute',
        top: -100,
        right: 100
    },
    toudou: {
        width: 210,
        height: 356,
        position: 'absolute',
        bottom: 150,
        left: 390
    },
    liane_1: {
        width: 160,
        height: 400,
        position: 'absolute',
        top: -110,
        left: 190
    },
    liane_2: {
        width: 100,
        height: 600,
        position: 'absolute',
        top: -130,
        left: 250
    },
    caillou_1: {
        width: 142,
        height: 66,
        position: 'absolute',
        top: 480,
        left: 190
    },
    caillou_2: {
        width: 225,
        height: 88,
        position: 'absolute',
        top: 460,
        left: 580
    },
    caillou_3: {
        width: 163,
        height: 41,
        position: 'absolute',
        top: 518,
        left: 840
    },
    feuille_mask_1: {
        width: '100%',
        height: '100%',
    },
    fBG: {
        position: "absolute",
        top: -400,
        left: -100,
        width:630, height:1000
    },
    fBD: {
        position: "absolute",
        top: -250,
        right: -150,
        width:750, height:800
    },
    /*fHG: {
        position: "absolute",
        top: -180,
        left: -150,
        width:650, height:650
    },
    fHD: {
        position: "absolute",
        top: -180,
        right: -150,
        width:600, height:800
    },*/
    f1:{
        width:630, height:1000
    },
    f2:{
        width:750, height:800
    },
    /*f3:{
        width:650, height:650
    },
    f4:{
        width:600, height:800
    },*/
    leafContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        width: winW,
        height: winH
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    }
});