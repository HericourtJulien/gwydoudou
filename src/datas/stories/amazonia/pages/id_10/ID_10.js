/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    AsyncStorage,
    Dimensions
} from 'react-native';
import Animator from '../../../../../components/interactions/Animator';
import AppBorder from '../../../../../components/partials/AppBorder';
import IntAnim from '../../../../../datas/InterfaceAnimations';
import DraggableItem from '../../../../../components/interactions/draganddrop/DraggableItem';
import DropZone from '../../../../../components/interactions/draganddrop/DropZone';
import * as Animatable from 'react-native-animatable';

var winW = Dimensions.get('window').width;
var winH = Dimensions.get('window').height;

export default class Page_10 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data
        }
    }

    componentDidMount(){
        this.props.playSound(10, true);
        this.props.playSound(1, true);
        this.props.adjustVolume(1, 0.3);
    }

    componentWillUnmount(){
        this.props.stopSound(10);
        this.props.stopSound(1);
    }

    gestureBundleManager(gesture) {
        return this.dropzoneBundle.isOverDropZone(gesture);
    }

    displayTrophy() {
        this.refs.mask.transitionTo({opacity: 1}, 800);
        this.refs.trophy.transitionTo({transform: [{scale: 1}]}, 1000);
        setTimeout(() => {
            this.refs.bundleIcon.transitionTo({opacity: 1}, 800);
        }, 1000);
    }

    hideTrophy() {
        this.refs.dragTrophy.transitionTo({opacity: 0}, 1000);
        AsyncStorage.setItem('trophy_3', JSON.stringify(true));
        setTimeout(() => {
            this.refs.mask.transitionTo({opacity: 0}, 800);
            this.refs.trophy.transitionTo({transform: [{scale: 0}]}, 800);
            this.props.onNextPage();
        }, 1000);
    }

    render() {
        return (
            <View>
                <Image source={require('./assets/bg.png')} style={styles.bg}>
                    <Animatable.Image
                        source={require('./assets/nuage_1.png')}
                        animation={IntAnim.ANIMATIONS.CLOUD}
                        iterationCount={'infinite'}
                        duration={5000}
                        style={styles.nuage_1}
                    />
                    <Animatable.Image
                        source={require('./assets/nuage_2.png')}
                        animation={IntAnim.ANIMATIONS.CLOUD}
                        iterationCount={'infinite'}
                        duration={4000}
                        style={styles.nuage_2}
                    />

                    <Image source={require('./assets/bg_second.png')} style={styles.bg_second}/>
                    <Image source={require('./assets/feuille_montagne.png')} style={styles.feuille_montagne}/>

                    <Image source={require('./assets/bg_third.png')} style={styles.bg_third}/>
                    <Image source={require('./assets/feuille_caillou_3.png')} style={styles.feuille_3}/>

                    <Animator
                        source={require('./assets/json/feuille_caillou_3.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.feuille_3_move}
                    />

                    <Animator
                        source={require('./assets/json/caillou.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.caillou_move}
                    />


                    <Animator
                        source={require('./assets/json/croco.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.croco}
                    />

                    <Image source={require('./assets/feuille_caillou_4.png')} style={styles.feuille_4}/>


                    <Animator
                        source={require('./assets/json/feuille_caillou_4.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.feuille_4_move}
                    />

                    <Image source={require('./assets/ground.png')} style={styles.ground}/>

                    <Image source={require('./assets/feuille_caillou_1.png')} style={styles.feuille_1}/>

                    <Animator
                        source={require('./assets/json/feuille_caillou_1.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.feuille_1_move}
                    />
                    <Animator
                        source={require('./assets/json/toudou.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.toudou}
                    />

                    <Image source={require('./assets/caillou_1.png')} style={styles.caillou_1}/>
                </Image>

                <Animatable.View ref={'mask'} style={styles.mask}/>
                <Animatable.View ref={'trophy'} style={styles.trophy}>

                    <Animatable.Image animation={bundle} iterationCount={'infinite'} duration={3000} ref={'bundleIcon'} source={require('../../../../../assets/img/story/bundle_unlock.png')} style={styles.iconBundle}/>

                    <Image
                        source={require('./assets/teeth_bg.gif')}
                        style={styles.trophyTeethBg}
                    />
                    <Animatable.View ref={'dragTrophy'} style={{backgroundColor: 'green'}}>

                        <DraggableItem
                            height={280}
                            width={100}
                            minX={0}
                            maxX={winW}
                            minY={0}
                            maxY={winH}
                            x={455}
                            y={130}
                            lockX={false}
                            lockY={false}
                            gestureHandler={(gesture) => {
                                return this.gestureBundleManager(gesture);
                            }}
                            callback={() => {
                                this.hideTrophy();
                            }}
                        >
                            <Animator
                                source={require('../../assets/bundle/json/teeth.json')}
                                duration={2000}
                                repeat={false}
                                style={styles.trophyTeethImg}
                            />
                        </DraggableItem>
                    </Animatable.View>

                    <DropZone
                        height={160}
                        width={160}
                        x={430}
                        y={480}
                        toX={0}
                        toY={240}
                        lockX={false}
                        lockY={false}
                        ref={(dropzoneBundle) => {
                            this.dropzoneBundle = dropzoneBundle;
                        }}
                    />

                </Animatable.View>

                {/*Cheat code*/}
                <TouchableHighlight onPress={() => this.displayTrophy()} style={styles.cheatCode} underlayColor="0">
                    <View/>
                </TouchableHighlight>

                {/*Border*/}
                <Image source={require('../../../../../assets/img/others/app_left_border.png')} style={styles.leftBorder}/>
                <Image source={require('../../../../../assets/img/others/app_right_border.png')} style={styles.rightBorder}/>
                <Image source={require('../../../../../assets/img/others/app_top_border.png')} style={styles.topBorder}/>
            </View>
        );
    }
}

const bundle = {
    0: {
        transform: [{
            scale: 0.5
        }]
    },
    0.5: {
        transform: [{
            scale: 0.6
        }]
    },
    1: {
        transform: [{
            scale: 0.5
        }]
    }
};

const styles = StyleSheet.create({
    bg: {
        width: '100%',
        height: '100%'
    },
    bg_second: {
        position: 'absolute',
        height: '100%',
        width: '100%'
    },
    bg_third: {
        position: 'absolute',
        height: '100%',
        width: '100%'
    },
    ground: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        height: 174,
        width: '100%'
    },
    feuille_1: {
        position: 'absolute',
        top: 345,
        left: -30,
        height: 205,
        width: 181
    },
    feuille_1_move: {
        position: 'absolute',
        top: 130,
        left: 0,
        height: 300,
        width: 181
    },
    feuille_3: {
        position: 'absolute',
        top: 340,
        left: 630,
        height: 142,
        width: 95
    },
    feuille_3_move: {
        position: 'absolute',
        top: 154,
        left: 300,
        height: 166,
        width: 180
    },
    feuille_4: {
        position: 'absolute',
        top: 420,
        right: 120,
        height: 139,
        width: 93
    },
    feuille_4_move: {
        position: 'absolute',
        top: 220,
        left: 390,
        height: 140,
        width: 120
    },
    feuille_montagne: {
        position: 'absolute',
        top: 385,
        left: 395,
        height: 55,
        width: 62
    },
    caillou_1: {
        position: 'absolute',
        bottom: 70,
        left: -135,
        height: 128,
        width: 328
    },
    nuage_1: {
        position: 'absolute',
        top: 170,
        left: 600,
        height: 39,
        width: 220
    },
    nuage_2: {
        position: 'absolute',
        top: 70,
        left: 720,
        height: 63,
        width: 281
    },
    caillou_move: {
        position: 'absolute',
        top: 220,
        left: 275,
        height: 90,
        width: 280
    },
    croco: {
        position: 'absolute',
        top: 160,
        left: 200,
        height: 400,
        width: 600
    },
    croco_surface: {
        position: 'absolute',
        top: 220,
        left: 275,
        height: 90,
        width: 280
    },
    toudou: {
        position: 'absolute',
        top: 140,
        left: 75,
        height: 360,
        width: 240
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    },
    trophy: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: winW,
        height: winH,
        transform: [{
            scale: 0
        }]
    },
    trophyTeethImg: {
        width: 120,
        height: 250,
    },
    trophyTeethBg: {
        position: 'absolute',
        width: 302,
        height: 460,
        left: 356,
        top: 14
    },
    mask: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'rgba(255,255,255, 0.7)',
        opacity: 0
    },
    iconBundle: {
        position: 'absolute',
        bottom: 130,
        left: winW / 2 - 75,
        height: 150,
        width: 150,
        opacity: 0
    },
    cheatCode: {
        position: 'absolute',
        width: 80,
        height: 80,
        right: 27,
        bottom: 20,
    }
});

