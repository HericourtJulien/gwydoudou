/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TouchableHighlight
} from 'react-native';
import Animator from '../../../../../components/interactions/Animator';
import AppBorder from '../../../../../components/partials/AppBorder';
import IntAnim from '../../../../../datas/InterfaceAnimations';
import * as Animatable from 'react-native-animatable';

var winW = Dimensions.get('window').width;
var winH = Dimensions.get('window').height;

let micInterval;
let blowDetected = false;

const {MicOBJC} = require('NativeModules');

function customX() {
    return Math.round(Math.random() * ((winW - 30) - 30) + 30)/3;
}

function customY() {
    return Math.round(Math.random() * ((winH - 100) - 30) + 30)/3;
}

function customRotation() {
    return Math.round(Math.random() * 360) + 'deg';
}

function customScale() {
    return (Math.random() * (1 - 0.5) + 0.5);
}

function customDelay(){
    return Math.round(Math.random() * (8000 - 1000) + 1000);
}

export default class Page_6 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data,
            flies: null
        }
    }

    componentWillMount() {
        let flies = [];
        let i = 0;
        let posX, posY, rotation;

        for (i; i < 10; i++) {
            posX = Math.round(Math.random() * winW / 2);
            posY = Math.round(Math.random() * (winH - 128) / 2);
            rotation = Math.round(Math.random() * 360) + 'deg';
            flies.push(
                <Animator
                    source={require('./assets/json/mouche.json')}
                    duration={500}
                    repeat={false}
                    style={[styles.mouche, {left: posX, top: posY, transform: [{rotate: rotation}]}]}
                />
            );
        }

        this.setState({
            flies: flies
        });
    }

    componentDidMount() {

        this.props.playSound(3, true);
        this.props.playSound(14, true);

        //setup microphone from HOME MADE native module
        MicOBJC.setup();
        micInterval = setInterval(() => {
            MicOBJC.levelTimerCallback((a) => {
                console.log(a);

                //check if blowed already detected or not
                if (!blowDetected) {
                    blowDetected = true;
                    this.moveFlies();
                }
            });
        }, 200);
    }

    componentWillUnmount(){
        this.props.stopSound(3);
        this.props.stopSound(14);
    }

    moveFlies() {
        //stop using microphone
        clearInterval(micInterval);
        blowDetected = false;

        console.log("move flies");

        //move the flies
        let i = 0;
        for (i; i < this.state.flies.length; i++) {
            this.refs[i + 1].animate({
                0: {
                    translateX: 50,
                    translateY: 50,
                    rotate: "30deg"
                },
                1: {
                    translateX: customX() * 8,
                    translateY: customY() * 8,
                    rotate: customRotation()
                }
            }, 3000);
        }

        this.props.stopSound(14);

        setTimeout(() => {
            this.props.onNextPage();
        }, 2000);
    }

    render() {
        return (
            <View>
                <Image source={require('./assets/bg.png')} style={styles.bg}>
                    <Animator
                        source={require('./assets/json/feuille_1.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.feuille_1}
                    />
                    <Image source={require('./assets/statue.png')} style={styles.statue}/>
                    <Animatable.Image
                        source={require('./assets/nuage_1.png')}
                        animation={IntAnim.ANIMATIONS.CLOUD}
                        iterationCount={'infinite'}
                        duration={5000}
                        style={styles.nuage}
                    />
                    <Image source={require('./assets/liane.png')} style={styles.liane}/>
                    <Animator
                        source={require('./assets/json/paresseux.json')}
                        duration={2500}
                        repeat={false}
                        style={styles.paresseux}
                    />

                    <Animatable.View
                        ref="1"
                        animation={moveFlie1}
                        iterationCount={"infinite"}
                        duration={5000}
                        delay={customDelay()}
                        easing={"linear"}
                    >
                        <Animator
                            source={require('./assets/json/mouche.json')}
                            duration={250}
                            repeat={false}
                            style={[styles.mouche, {
                                left: 240,
                                top: 200,
                                transform: [{rotate: customRotation()}, {scale: customScale()}]
                            }]}
                        />
                    </Animatable.View>

                    <Animatable.View
                        ref="2"
                        animation={moveFlie2}
                        iterationCount={"infinite"}
                        duration={5000}
                        delay={customDelay()}
                        easing={"linear"}
                    >
                        <Animator
                            source={require('./assets/json/mouche.json')}
                            duration={250}
                            repeat={false}
                            style={[styles.mouche, {
                                left: 260,
                                top: 230,
                                transform: [{rotate: customRotation()}, {scale: customScale()}]
                            }]}
                        />
                    </Animatable.View>

                    <Animatable.View
                        ref="3"
                        animation={moveFlie3}
                        iterationCount={"infinite"}
                        duration={5000}
                        delay={customDelay()}
                        easing={"linear"}
                    >
                        <Animator
                            source={require('./assets/json/mouche.json')}
                            duration={250}
                            repeat={false}
                            style={[styles.mouche, {
                                left: 212,
                                top: 420,
                                transform: [{rotate: customRotation()}, {scale: customScale()}]
                            }]}
                        />
                    </Animatable.View>

                    <Animatable.View
                        ref="4"
                        animation={moveFlie4}
                        iterationCount={"infinite"}
                        duration={5000}
                        delay={customDelay()}
                        easing={"linear"}
                    >
                        <Animator
                            source={require('./assets/json/mouche.json')}
                            duration={250}
                            repeat={false}
                            style={[styles.mouche, {
                                left: 288,
                                top: 130,
                                transform: [{rotate: customRotation()}, {scale: customScale()}]
                            }]}
                        />
                    </Animatable.View>

                    <Animatable.View
                        ref="5"
                        animation={moveFlie5}
                        iterationCount={"infinite"}
                        duration={5000}
                        delay={customDelay()}
                        easing={"linear"}
                    >
                        <Animator
                            source={require('./assets/json/mouche.json')}
                            duration={250}
                            repeat={false}
                            style={[styles.mouche, {
                                left: 250,
                                top: 368,
                                transform: [{rotate: customRotation()}, {scale: customScale()}]
                            }]}
                        />
                    </Animatable.View>

                    <Animatable.View
                        ref="6"
                        animation={moveFlie6}
                        iterationCount={"infinite"}
                        duration={5000}
                        delay={customDelay()}
                        easing={"linear"}
                    >
                        <Animator
                            source={require('./assets/json/mouche.json')}
                            duration={250}
                            repeat={false}
                            style={[styles.mouche, {
                                left: 268,
                                top: 165,
                                transform: [{rotate: customRotation()}, {scale: customScale()}]
                            }]}
                        />
                    </Animatable.View>

                    <Animatable.View
                        ref="7"
                        animation={moveFlie7}
                        iterationCount={"infinite"}
                        duration={5000}
                        delay={customDelay()}
                        easing={"linear"}
                    >
                        <Animator
                            source={require('./assets/json/mouche.json')}
                            duration={250}
                            repeat={false}
                            style={[styles.mouche, {
                                left: 371,
                                top: 247,
                                transform: [{rotate: customRotation()}, {scale: customScale()}]
                            }]}
                        />
                    </Animatable.View>

                    <Animatable.View
                        ref="8"
                        animation={moveFlie8}
                        iterationCount={"infinite"}
                        duration={5000}
                        delay={customDelay()}
                        easing={"linear"}
                    >
                        <Animator
                            source={require('./assets/json/mouche.json')}
                            duration={250}
                            repeat={false}
                            style={[styles.mouche, {
                                left: 200,
                                top: 400,
                                transform: [{rotate: customRotation()}, {scale: customScale()}]
                            }]}
                        />
                    </Animatable.View>

                    <Animatable.View
                        ref="9"
                        animation={moveFlie9}
                        iterationCount={"infinite"}
                        duration={5000}
                        delay={customDelay()}
                        easing={"linear"}
                    >
                        <Animator
                            source={require('./assets/json/mouche.json')}
                            duration={250}
                            repeat={false}
                            style={[styles.mouche, {
                                left: 150,
                                top: 200,
                                transform: [{rotate: customRotation()}, {scale: customScale()}]
                            }]}
                        />
                    </Animatable.View>

                    <Animatable.View
                        ref="10"
                        animation={moveFlie10}
                        iterationCount={"infinite"}
                        duration={5000}
                        delay={customDelay()}
                        easing={"linear"}
                    >
                        <Animator
                            source={require('./assets/json/mouche.json')}
                            duration={250}
                            repeat={false}
                            style={[styles.mouche, {
                                left: 200,
                                top: 250,
                                transform: [{rotate: customRotation()}, {scale: customScale()}]
                            }]}
                        />
                    </Animatable.View>


                    {/*{this.state.flies}*/}
                </Image>

                {/*Cheat code*/}
                <TouchableHighlight onPress={() => this.moveFlies()} style={styles.cheatCode} underlayColor="0">
                    <View/>
                </TouchableHighlight>

                {/*Border*/}
                <Image source={require('../../../../../assets/img/others/app_left_border.png')}
                       style={styles.leftBorder}/>
                <Image source={require('../../../../../assets/img/others/app_right_border.png')}
                       style={styles.rightBorder}/>
                <Image source={require('../../../../../assets/img/others/app_top_border.png')}
                       style={styles.topBorder}/>
            </View>
        );
    }
}

let moveFlie1 = {
    0: {
        left: 0,
        top: 0,
    },
    0.25:{
        left: 150,
        top: 120,
    },
    0.5: {
        left: 100,
        top: 100,
    },
    0.75:{
        left: 250,
        top: 300,
    },
    1: {
        left: 0,
        top: 0,
    }
};

let moveFlie2 = {
    0: {
        left: 0,
        top: 0,
    },
    0.25:{
        left: 130,
        top: 120,
    },
    0.5: {
        left: 100,
        top: 100,
    },
    0.75:{
        left: 140,
        top: 110,
    },
    1: {
        left: 0,
        top: 0,
    }
};

let moveFlie3 = {
    0: {
        left: 0,
        top: 0,
    },
    0.25:{
        left: 130,
        top: 90,
    },
    0.5: {
        left: 120,
        top: 170,
    },
    0.75:{
        left: 150,
        top: 30,
    },
    1: {
        left: 0,
        top: 0,
    }
};

let moveFlie4 = {
    0: {
        left: 0,
        top: 0,
    },
    0.25:{
        left: 110,
        top: 70,
    },
    0.5: {
        left: 150,
        top: 150,
    },
    0.75:{
        left: 130,
        top: 120,
    },
    1: {
        left: 0,
        top: 0,
    }
};


let moveFlie5 = {
    0: {
        left: 0,
        top: 0,
    },
    0.25:{
        left: 140,
        top: 50,
    },
    0.5: {
        left: 170,
        top: 170,
    },
    0.75:{
        left: 150,
        top: 150,
    },
    1: {
        left: 0,
        top: 0,
    }
};

let moveFlie6 = {
    0: {
        left: 0,
        top: 0,
    },
    0.25:{
        left: 50,
        top: 10,
    },
    0.5: {
        left: 140,
        top: 120,
    },
    0.75:{
        left: 120,
        top: 140,
    },
    1: {
        left: 0,
        top: 0,
    }
};

let moveFlie7 = {
    0: {
        left: 0,
        top: 0,
    },
    0.25:{
        left: 130,
        top: 140,
    },
    0.5: {
        left: 180,
        top: 150,
    },
    0.75:{
        left: 150,
        top: 120,
    },
    1: {
        left: 0,
        top: 0,
    }
};

let moveFlie8 = {
    0: {
        left: 0,
        top: 0,
    },
    0.25:{
        left: 150,
        top: 120,
    },
    0.5: {
        left: 110,
        top: 150,
    },
    0.75:{
        left: 170,
        top: 120,
    },
    1: {
        left: 0,
        top: 0,
    }
};

let moveFlie9 = {
    0: {
        left: 0,
        top: 0,
    },
    0.25:{
        left: 150,
        top: 10,
    },
    0.5: {
        left: 120,
        top: 170,
    },
    0.75:{
        left: 170,
        top: 180,
    },
    1: {
        left: 0,
        top: 0,
    }
};

let moveFlie10 = {
    0: {
        left: 0,
        top: 10,
    },
    0.25:{
        left: 120,
        top: 110,
    },
    0.5: {
        left: 120,
        top: 180,
    },
    0.75:{
        left: 150,
        top: 10,
    },
    1: {
        left: 100,
        top: 130,
    }
};



const styles = StyleSheet.create({
    bg: {
        width: '100%',
        height: '100%'
    },
    statue: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        height: 470,
        width: 442
    },
    feuille_1: {
        position: 'absolute',
        bottom: 0,
        left: 190,
        height: 470,
        width: 342
    },
    nuage: {
        position: 'absolute',
        top: 100,
        left: 520,
        height: 83,
        width: 370
    },
    liane: {
        position: 'absolute',
        top: -20,
        left: 20,
        height: 770,
        width: 47
    },
    paresseux: {
        position: 'absolute',
        top: -200,
        left: 100,
        height: 1600,
        width: 1300
    },
    mouche: {
        position: 'absolute',
        /*top: 200,
        left: 120,*/
        height: 80,
        width: 80
    },
    leftBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        left: 0
    },
    rightBorder: {
        width: 25,
        height: 768,
        position: 'absolute',
        zIndex: 40,
        top: 0,
        right: 0
    },
    topBorder: {
        width: '100%',
        height: 26,
        position: 'absolute',
        zIndex: 4,
        top: 0,
        left: 0
    },
    cheatCode: {
        position: 'absolute',
        width: 80,
        height: 80,
        right: 27,
        bottom: 20,
    }
});

