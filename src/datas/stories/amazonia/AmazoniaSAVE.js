export default {
    PAGES: [
        {
            ID: 0,
            TEXT: [
                "{char}none{char}"
            ],
            INTERACTIONS: [],
            CAN_MOVE: {
                PREV: false,
                NEXT: false
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 1,
            TEXT: [
                "Me voilà arrivé en Amazonie! L’entrée de la jungle est de l’autre côté de la rive !\n{b}Fais moi traverser à l’aide de ma pirogue!{b}"
            ],
            INTERACTIONS: ['drag_1'],
            CAN_MOVE: {
                PREV: true,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 2,
            TEXT: [
                "Regarde comme les feuilles sont grandes, il faudrait me frayer un chemin pour atteindre le sentier.\n{b}Aide-moi à écarter ces feuilles !{b}",
                "Oh mais qui voilà ! C’est Toucan perché sur un arbre qui a l’air contrarié.",
                "Salut Toucan, je m’appelle Toudou ! Que t’arrive-t-il ?",
                "Bonjour… je suis bien embêté, j’ai fait tomber ma papaye ! Impossible de la retrouver et j’ai faim! TRRRRRI-TRRRRI{char}toucan{char}",
                "Attends je vais t’aider !"
            ],
            INTERACTIONS: ['swipe_1'],
            CAN_MOVE: {
                PREV: true,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 3,
            TEXT: [
                "{b}Cherche la papaye dans le paysage.{b}{char}none{char}",
            ],
            INTERACTIONS: ['touch_1', 'drag_2'],
            CAN_MOVE: {
                PREV: true,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 4,
            TEXT: [
                "{b}Glisse (jette) la papaye dans le bec de Toucan.{char}none{char}",
                "Merci Toudou ! Je vais enfin pouvoir me régaler ! Mais, dis-moi, tu n’es pas d’ici ?{char}toucan{char}",
                "Non, je viens de France et j’ai promis à {name} de lui ramener un souvenir de tous les amis que je rencontre. Veux-tu devenir mon ami ?",
                "Bien sûr, voilà une plume pour ton copain {name} TRRRRRI-TRRRRI\n{b}Place la plume dans le baluchon.{b}{char}toucan{char}",
                "Oh merci Toucan, à bientôt !",
            ],
            INTERACTIONS: ['drag_3'],
            CAN_MOVE: {
                PREV: true,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 5,
            TEXT: [
                "Tiens, il semblerait que des lianes gênent le passage! {b}Essaye de les décrocher !{b}",
                "Oh bonjour Paresseux, je m’appelle Toudou. Mais tu ne dors pas ? Je croyais que les paresseux dormaient toute la journée ?",
                "Oh, je n’y arrive pas ! Les mouches font trop de bruit ! Et j’ai sommeil ! AAAAAAAAAH (bâillement)",
                "Attends je vais t’aider !"
            ],
            INTERACTIONS: ['drag_4'],
            CAN_MOVE: {
                PREV: true,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 6,
            TEXT: [
                "{b}Souffle sur l’écran pour chasser les mouches.{b}",
            ],
            INTERACTIONS: ['mike_1'],
            CAN_MOVE: {
                PREV: true,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 7,
            TEXT: [
                "Oh merci Toudou ! Je vais enfin pouvoir dormir! Mais, dis-moi, tu n’es pas d’ici ?",
                "Non, je viens de France et j’ai promis à {name} de lui ramener un souvenir de tous les amis que je rencontre. Veux-tu devenir mon ami ?",
                "Bien sûr, voilà une branchette de mon arbre préféré pour ton copain {name} AAAAAAAAHHH\n{b}Place la branchette dans le baluchon.{b}",
                "Oh merci Paresseux, à bientôt !"
            ],
            INTERACTIONS: ['drag_5'],
            CAN_MOVE: {
                PREV: true,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 8,
            TEXT: [
                "Me revoilà vers le fleuve...il y a quelque chose qui ressemble à un rocher ! {b}Touche le.{b} C’est un crocodile, il a l’air tout triste...",
                "Bonjour ! Moi c’est Toudou ! Tu m’as l’air bien triste, que t’arrive-t-il ?",
                "Bonjour… Non, je ne suis pas triste, j’ai une dent qui bouge et qui me fait très mal ! Je n’arrive pas à l’enlever tout seul… GRRRROAR",
                "Attends je vais t’aider !"
            ],
            INTERACTIONS: [],
            CAN_MOVE: {
                PREV: true,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 9,
            TEXT: [
                "{b}Tape sur la dent de Croco pour la faire tomber.{b}"
            ],
            INTERACTIONS: ['swipe_2'],
            CAN_MOVE: {
                PREV: true,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 10,
            TEXT: [
                "Oh merci Toudou ! Je ne sens plus rien! Mais dis-moi, tu n’es pas d’ici ?",
                "Non, je viens de France et j’ai promis à {name} de lui ramener un souvenir de tous les amis que je rencontre. Veux-tu devenir mon ami ?",
                "Bien sûr, tiens, prends ma petite dent de lait pour ton copain {name} GRRRROAR\n{b}Place la dent dans le baluchon.{b}",
                "Oh merci Croco ! Il est temps pour moi de rentrer !",
                "Grimpe sur mon dos, je t’emmène jusqu’à ta pirogue !"
            ],
            INTERACTIONS: ['drag_6'],
            CAN_MOVE: {
                PREV: true,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 11,
            TEXT: [
                "{b}Incline la tablette vers la droite pour faire nager Croco jusqu’au bateau.{b}",
            ],
            INTERACTIONS: ['drag_6'],
            CAN_MOVE: {
                PREV: true,
                NEXT: true
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        },
        {
            ID: 12,
            TEXT: [
                "Regarde Toucan et Paresseux sont là aussi pour te souhaiter bon voyage",
                "Au revoir Toudou ! Bonjour à {name} !",
                "Au revoir les amis , à très bientôt et encore merci !",
                "C’est ici que ce beau voyage prend fin, je rentre à la maison !"
            ],
            INTERACTIONS: [],
            CAN_MOVE: {
                PREV: true,
                NEXT: false
            },
            SOUNDS: {
                NARRATOR: '',
                AMBIANT: ''
            },
            IMG: [],
            CURRENT_TEXT: 0
        }
    ]
}