/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import {Navigator} from 'react-native-deprecated-custom-components';
import { setCustomText } from 'react-native-global-props';

var Map = require('./src/pages/Map');
var Loader = require('./src/pages/Loader');
var Settings = require('./src/pages/Settings');
var UnlockApp = require('./src/pages/UnlockApp');
var UnlockNewStory = require('./src/pages/UnlockNewStory');
var Bundle = require('./src/pages/Bundle');
var Story = require('./src/pages/story/Story');
var Inscription = require('./src/pages/Inscription');

var Test = require('./src/datas/stories/amazonia/pages/id_1/ID_1');
//var Test = require('./src/pages/CrocoTest');

var Orientation = require('react-native-orientation');


export default class gwydoudou extends Component {

    /*componentWillMount() {
        Orientation.lockToLandscape();
    }*/

    render() {
        setCustomText(customTextProps);
        return (
            <Navigator
                initialRoute={{
                    id: 'Map'
                }}
                renderScene={
                    this.navigatorRenderScene
                }
                configureScene={(route, routeStack) => Navigator.SceneConfigs.FadeAndroid}
            />
        );
    }

    navigatorRenderScene(route, navigator) {
        _navigator = navigator;
        switch (route.id) {
            case 'Map':
                return (<Map navigator={navigator} title="Map"/>);
            case 'Loader':
                return (<Loader navigator={navigator} title="Loader"/>);
            case 'Settings':
                return (<Settings navigator={navigator} title="Settings"/>);
            case 'UnlockApp':
                return (<UnlockApp navigator={navigator} title="UnlockApp"/>);
            case 'UnlockNewStory':
                return (<UnlockNewStory navigator={navigator} title="UnlockNewStory"/>);
            case 'Bundle':
                return (<Bundle navigator={navigator} title="Bundle"/>);
            case 'Inscription':
                return (<Inscription navigator={navigator} title="Inscription"/>);
            case 'Story':
                return (<Story navigator={navigator} title="Story" data={route.data}/>);
            case 'Test':
                return (<Test navigator={navigator} title="Test" />)
        }
    }
}

const customTextProps = {
    style: {
        fontFamily: 'Volkswagen-DemiBold',
        fontSize: 15
    }
};

AppRegistry.registerComponent('gwydoudou', () => gwydoudou);
