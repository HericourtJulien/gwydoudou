/**
 * Created by benja on 12/05/2017.
 */

/**
 * Animator -> use to control the crocodile along its path
 * @constructor
 */
var Animator = function(){
    var self = this;

    self.crocoTween = null;
    self.backTween = null;

    self.isLaunched = false;
    self.isPlaying = false;
    self.isReversing = false;
    self.animationDone = false;
    self.animationDuration = 20;

    self.scene = document.getElementById("mainScene");

    //gegt path data from the SVG curve
    self.fleuvePath = MorphSVGPlugin.pathDataToBezier("#fleuve");

    //set the croco position for the begining ot the page animation
    self.croco = document.getElementById("croco");
    self.croco.style.top = self.fleuvePath[0].y + "px";
    self.croco.style.transform = "rotate(25deg)";
    TweenMax.set("#croco", {xPercent:-50, yPercent:-50});//set its anchor point to be at the center of the gif

    self.tl = new TimelineMax();//Timeline for the crocodile animation
    self.sceneTl = new TimelineMax();//Timeline for the background scene

    /**********************************************************
     * buttons and listener for debug purpose
     *********************************************************/
    self.playBtn = document.getElementById("play");
    self.reverseBtn = document.getElementById("reverse");
    self.pauseBtn = document.getElementById("pause");

    self.playBtn.addEventListener("click", function () {

        if(self.isPlaying){
            self.pauseAnim();
        }else{
            if(self.isLaunched){
                self.playAnim();
            }else{
                self.launch();
            }
        }

    });

    self.reverseBtn.addEventListener("click", function () {
        self.reverseAnim();
    });

    self.pauseBtn.addEventListener("click", function () {
        self.pauseAnim();
    });
    /*********************************************************/

    //get screen size to know when to move the scene in x axis
    self.screenWidth = window.innerWidth;

};

/**
 * launch the animation
 * set the timeliness
 */
Animator.prototype.launch = function () {
    var self = this;
    console.log("launch");

    self.croco.style.top = "0px";

    self.isPlaying = true;
    self.isLaunched = true;

    var crocoPosX = 0;

    self.crocoTween = self.tl.to("#croco", self.animationDuration, {
        bezier:{
            values:self.fleuvePath,
            type: "cubic",
            autoRotate:true
        },
        ease: Power0.easeInOut,
        onUpdateScope:this,
        onUpdate:function () {
            //check if the x position of croco is windowWidt /2 or more to start moving the scene to the left
            //and let croco at the center of the screen
            crocoPosX = self.croco._gsTransform.x;

            self.moveBackground(crocoPosX);
            //document.getElementById("crocoX").innerHTML = parseInt(crocoPosX);
        },
        onComplete:function(){
            self.animationDone = true;
            sendToRN("finished");
        }
    });

    self.debugInfos();

};

/**
 * reverse the animation
 */
Animator.prototype.reverseAnim = function () {
    var self = this;
    console.log("reverse");

    self.isPlaying = true;
    self.isReversing = true;

    self.pauseAnim();
    self.tl.reverse();
    self.debugInfos();
};

/**
 * stop the animation
 */
Animator.prototype.pauseAnim = function () {
    var self = this;
    console.log("pause");

    self.isPlaying = false;

    self.tl.pause();
    self.debugInfos();
};

/**
 * play the animation from left to right
 */
Animator.prototype.playAnim = function () {
  var self = this;
  console.log("play");

  if(self.isPlaying){
      self.pauseAnim();
  }

  self.isPlaying = true;

  self.tl.play();
  self.debugInfos();
};

/**
 * display debug infos
 */
Animator.prototype.debugInfos = function () {
    var self = this;
    console.warn("debugger");

    var isPlaying = document.getElementById("isPlaying");

    isPlaying.innerText = "is playing : " + self.isPlaying;

};

/**
 * move the background to always have the croco in the middle of the screens
 * @param crocoPos number -> current position of the crocodile
 */
Animator.prototype.moveBackground = function (crocoPos) {
    var self = this;
    console.log("move background");
    //objective is to move the x scroll position to keep croco at the center

    if(crocoPos > self.screenWidth/1.5 && self.isPlaying){
        window.scrollTo((crocoPos-self.screenWidth/1.5)/2, 0);
    }else if(crocoPos < (self.scene.offsetWidth - self.screenWidth/3) && self.isReversing){
        window.scrollTo((crocoPos-self.screenWidth/1.5)/2, 0);
    }


};