//
//  MicOBJC.h
//  cleanProject
//
//  Created by mac13 on 20/06/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <React/RCTBridgeModule.h>

@interface MicOBJC : NSObject <RCTBridgeModule>{
  AVAudioRecorder *recorder;
  NSTimer *levelTimer;
  double lowPassResults;
}

- (void) setUpdateInterval:(double) interval;
-(void)setup;
-(void)levelTimerCallback;

@end
