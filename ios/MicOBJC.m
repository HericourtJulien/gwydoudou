//
//  MicOBJC.m
//  cleanProject
//
//  Created by mac13 on 20/06/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "MicOBJC.h"
#import <React/RCTBridge.h>
#import <React/RCTEventDispatcher.h>

@implementation MicOBJC

@synthesize bridge = _bridge;

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(setUpdateInterval:(double) interval) {
  NSLog(@"setUpdateInterval: %f", interval);
  double intervalInSeconds = interval / 1000;
  
 }

RCT_EXPORT_METHOD(setup){
  NSURL *url = [NSURL fileURLWithPath:@"/dev/null"];
		
  NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithFloat: 44100.0],                 AVSampleRateKey,
                            [NSNumber numberWithInt: kAudioFormatAppleLossless], AVFormatIDKey,
                            [NSNumber numberWithInt: 1],                         AVNumberOfChannelsKey,
                            [NSNumber numberWithInt: AVAudioQualityMax],         AVEncoderAudioQualityKey,
                            nil];
		
  NSError *error;
		
  recorder = [[AVAudioRecorder alloc] initWithURL:url settings:settings error:&error];
		
  if (recorder) {
    [recorder prepareToRecord];
    recorder.meteringEnabled = YES;
    [recorder record];
    //levelTimer = [NSTimer scheduledTimerWithTimeInterval: 0.03 target: self selector: @selector(levelTimerCallback:) userInfo: nil repeats: YES];
    //[levelTimer fire];
  } else
    NSLog([error description]);	

}


RCT_EXPORT_METHOD(levelTimerCallback:(RCTResponseSenderBlock)cb)//:(NSTimer *)timer {
{
  [recorder updateMeters];
  
  const double ALPHA = 0.05;
  double peakPowerForChannel = pow(10, (0.05 * [recorder peakPowerForChannel:0]));
  lowPassResults = ALPHA * peakPowerForChannel + (1.0 - ALPHA) * lowPassResults;
  //NSLog(@"intensité : %f", lowPassResults);
 
  if (lowPassResults > 0.25){
    NSLog(@"Mic blow detected");
    NSString *blowed = @"blowed";
    cb(@[[NSString stringWithString:blowed]]);
  }
}

@end
