//
//  Microphone.m
//  cleanProject
//
//  Created by itycom on 07/06/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTBridge.h>
#import <React/RCTEventDispatcher.h>

@interface RCT_EXTERN_MODULE (Microphone, NSObject)

RCT_EXTERN_METHOD(startBtn:(RCTResponseSenderBlock)startCallback)
RCT_EXTERN_METHOD(stopBtn)

@end
