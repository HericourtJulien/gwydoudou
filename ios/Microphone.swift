//
//  ViewController.swift
//  BlowMic
//
//  Created by mac13 on 29/05/2017.
//  Copyright © 2017 mac13. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import CoreAudio

@objc(Microphone)
class Microphone : NSObject{
  
    var isRecording = false

    var recorder: AVAudioRecorder!
    var levelTimer = Timer()
    var lowPassResults: Double = 0.0
    
    var isBlowDetected:Bool = false;
    var audioSession:AVAudioSession!
  
    var bridge: RCTBridgeModule!
    var callBack:RCTResponseSenderBlock?
  
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
  
    
    //This selector/function is called every time our timer (levelTime) fires
    func levelTimerCallback(){
    
      
        //we have to update meters before we can get the metering values
        recorder.updateMeters()
        callBack!([NSNull(), ["PROUT"]])
        //print to the console if we are beyond a threshold value. Here I've used -7
        if recorder.averagePower(forChannel: 0) > -7 {
            print("DB ")
            print(recorder.averagePower(forChannel: 0))
            
            if recorder.averagePower(forChannel: 0) > 4{
                //startCallback([NSNull(), ["PROUT"]])
                stopBtn(Any)
            }
        }
    }
  
    @objc func startBtn(_ startCallback:@escaping RCTResponseSenderBlock) -> Void{
        //start using microphone
        
        if(isRecording){
            return
        }
      
      
        callBack = startCallback
        isRecording = true
        print("START")
        
        
        //make an AudioSession, set it to PlayAndRecord and make it active
        audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try audioSession.setActive(true)
            print("CONFIG OK")
        } catch {
            print(error)
        }
        
        //set up the URL for the audio file
        
        let url = getDocumentsDirectory().appendingPathComponent("myRecording1.caf")
        
        // make a dictionary to hold the recording settings so we can instantiate our AVAudioRecorder
        
        let recordSettings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        //declare a variable to store the returned error if we have a problem instantiating our AVAudioRecorder
        let erreur: NSError?
        
        //Instantiate an AVAudioRecorder
        do{
            try recorder = AVAudioRecorder(url: url, settings: recordSettings)
            recorder.prepareToRecord()
            recorder.isMeteringEnabled = true
            
            //start recording
            recorder.record()
            print("RECORD")
          
          
            //instantiate a timer to be called with whatever frequency we want to grab metering values
          
          if #available(iOS 10.0, *) {
            self.levelTimer = Timer.scheduledTimer(withTimeInterval: 0.02, repeats: true, block: { (timer) in
              self.levelTimerCallback()
            })
            self.levelTimer.fire()
          } else {
            // Fallback on earlier versions
          }
          
          //self.levelTimer = Timer.scheduledTimer(timeInterval: 0.02, target: self, selector: #selector(Microphone.levelTimerCallback), userInfo: nil, repeats: true)
          //self.levelTimer.fire()
        }catch{
            erreur = error as NSError
            print(error)
        }
        
    }
  
  
    @objc func stopBtn(_ sender:Any) {
        
        if(isRecording){
            isRecording = false
            isBlowDetected = true
            recorder.stop()
            print("STOP")
        }else{
            return
        }
        
  }
  
}

